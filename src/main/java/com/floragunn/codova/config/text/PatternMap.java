/*
 * Copyright 2022 floragunn GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.floragunn.codova.config.text;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;

import com.floragunn.fluent.collections.ImmutableMap;
import com.floragunn.fluent.collections.ImmutableSet;
import com.google.common.collect.HashMultimap;

public class PatternMap<V> {
    private final ImmutableMap<String, ImmutableSet<V>> constants;
    private final ImmutableMap<Pattern, ImmutableSet<V>> patterns;
    private final Node<V> trieRoot;

    private PatternMap(ImmutableMap<String, ImmutableSet<V>> constants, ImmutableMap<Pattern, ImmutableSet<V>> patterns, Node<V> trieRoot) {
        this.constants = constants;
        this.patterns = patterns;
        this.trieRoot = trieRoot;
    }

    public ImmutableSet<V> get(String key) {
        ImmutableSet<V> result = constants.get(key);

        if (result == null) {
            result = ImmutableSet.empty();
        }

        if (patterns.isEmpty() && trieRoot.size == 0) {
            return result;
        }

        if (trieRoot.size != 0) {
            ImmutableSet.Builder<V> resultBuilder = new ImmutableSet.Builder<V>(result);

            trieRoot.get(key, 0, resultBuilder);

            for (Map.Entry<Pattern, ImmutableSet<V>> entry : patterns.entrySet()) {
                Pattern pattern = entry.getKey();

                if (pattern.matches(key)) {
                    resultBuilder.addAll(entry.getValue());
                }
            }

            return resultBuilder.build();
        } else {
            ImmutableSet.Builder<V> resultBuilder = null;

            for (Map.Entry<Pattern, ImmutableSet<V>> entry : patterns.entrySet()) {
                Pattern pattern = entry.getKey();

                if (pattern.matches(key)) {
                    if (resultBuilder == null) {
                        if (result.isEmpty()) {
                            result = entry.getValue();
                        } else {
                            resultBuilder = new ImmutableSet.Builder<V>(result).with(entry.getValue());
                        }
                    } else {
                        resultBuilder.addAll(entry.getValue());
                    }
                }
            }

            if (resultBuilder != null) {
                return resultBuilder.build();
            } else {
                return result;
            }
        }
    }

    public ImmutableSet<V> get(Collection<String> keys) {
        ImmutableSet.Builder<V> resultBuilder = new ImmutableSet.Builder<V>();

        for (String key : keys) {
            ImmutableSet<V> value = constants.get(key);

            if (value != null) {
                resultBuilder.addAll(value);
            }

            if (trieRoot.size != 0) {
                trieRoot.get(key, 0, resultBuilder);
            }

            for (Map.Entry<Pattern, ImmutableSet<V>> entry : patterns.entrySet()) {
                Pattern pattern = entry.getKey();

                if (pattern.matches(key)) {
                    resultBuilder.addAll(entry.getValue());
                }
            }
        }

        return resultBuilder.build();
    }

    public boolean isEmpty() {
        return constants.isEmpty() && patterns.isEmpty();
    }

    public static class Builder<V> {
        private HashMultimap<String, V> constants = HashMultimap.create();
        private HashMultimap<Pattern, V> patternsWithoutPrefix = HashMultimap.create();
        private HashMultimap<Pattern, V> patternsWithPrefix = HashMultimap.create();
        private Node.Builder<V> trieRoot = new Node.Builder<>("");

        public void add(Pattern pattern, V value) {
            if (pattern instanceof PatternImpl.Constant) {
                constants.put(((PatternImpl.Constant) pattern).getValue(), value);
            } else if (pattern instanceof PatternImpl.CompoundPattern) {
                for (String constant : ((PatternImpl.CompoundPattern) pattern).getConstants()) {
                    constants.put(constant, value);
                }

                for (Pattern subPattern : ((PatternImpl.CompoundPattern) pattern).getPatternObjects()) {
                    this.add(subPattern, value);
                }
            } else if (pattern instanceof PatternImpl) {
                String prefix = ((PatternImpl) pattern).getPrefix();

                if (prefix != null) {
                    trieRoot.add(prefix, 0, (PatternImpl) pattern, value);
                    patternsWithPrefix.put(pattern, value);
                } else {
                    patternsWithoutPrefix.put(pattern, value);
                }
            } else {
                patternsWithoutPrefix.put(pattern, value);
            }
        }

        public PatternMap<V> build() {
            return new PatternMap<>(ImmutableMap.map(constants.asMap(), (k) -> k, (v) -> ImmutableSet.of(v)),
                    ImmutableMap.map(patternsWithoutPrefix.asMap(), (k) -> k, (v) -> ImmutableSet.of(v)), trieRoot.build());
        }
    }

    static class Node<V> {
        private final char[] nextKeys;
        private final Node<V>[] nextNodes;
        private final ImmutableMap<PatternImpl, ImmutableSet<V>> patterns;
        private final int size;

        Node(char[] nextKeys, Node<V>[] nextNodes, ImmutableMap<PatternImpl, ImmutableSet<V>> patterns, int size) {
            this.nextKeys = nextKeys;
            this.nextNodes = nextNodes;
            this.patterns = patterns;
            this.size = size;
        }

        void get(String value, int offset, ImmutableSet.Builder<V> result) {
            if (!patterns.isEmpty()) {
                this.patterns.forEach((pattern, v) -> {
                    if (pattern.matchesSkipPrefix(value)) {
                        result.addAll(v);
                    }
                });
            }

            if (offset >= value.length()) {
                return;
            }

            char c = value.charAt(offset);

            if (nextKeys.length == 1) {
                if (nextKeys[0] == c) {
                    nextNodes[0].get(value, offset + 1, result);
                }
            } else if (nextKeys.length != 0) {
                int pos = Arrays.binarySearch(nextKeys, c);

                if (pos >= 0) {
                    nextNodes[pos].get(value, offset + 1, result);
                }
            }
        }

        static class Builder<V> {
            private TreeMap<Character, Builder<V>> nodes = new TreeMap<>();
            private HashMultimap<PatternImpl, V> patterns = HashMultimap.create();
            private int size = 0;

            Builder(String prefix) {
            }

            void add(String key, int pos, PatternImpl pattern, V value) {
                size++;
                if (key == null) {
                    patterns.put(pattern, value);
                    return;
                }

                char c = key.charAt(pos);

                Builder<V> childNode = nodes.computeIfAbsent(c, (c0) -> new Builder<>(key.substring(0, pos + 1)));

                if (pos == key.length() - 1) {
                    childNode.patterns.put(pattern, value);
                } else {
                    childNode.add(key, pos + 1, pattern, value);
                }
            }

            Node<V> build() {
                char[] nextKeys = new char[this.nodes.size()];
                @SuppressWarnings("unchecked")
                Node<V>[] nextNodes = new Node[this.nodes.size()];

                int i = 0;

                for (Map.Entry<Character, Builder<V>> entry : this.nodes.entrySet()) {
                    nextKeys[i] = entry.getKey();
                    nextNodes[i] = entry.getValue().build();
                    i++;
                }

                return new Node<V>(nextKeys, nextNodes, ImmutableMap.map(patterns.asMap(), (k) -> k, (v) -> ImmutableSet.of(v)), size);
            }
        }
    }
}
