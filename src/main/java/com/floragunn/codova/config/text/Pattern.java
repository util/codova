/*
 * Copyright 2022 floragunn GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.floragunn.codova.config.text;

import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

import com.floragunn.codova.config.text.PatternImpl.CompoundPattern;
import com.floragunn.codova.config.text.PatternImpl.Constant;
import com.floragunn.codova.config.text.PatternImpl.JavaPattern;
import com.floragunn.codova.config.text.PatternImpl.PrefixPattern;
import com.floragunn.codova.documents.DocNode;
import com.floragunn.codova.documents.Document;
import com.floragunn.codova.documents.Parser;
import com.floragunn.codova.validation.ConfigValidationException;
import com.floragunn.fluent.collections.ImmutableList;
import com.floragunn.fluent.collections.ImmutableSet;

public interface Pattern extends Document<Pattern>, Predicate<String> {

    public static Pattern create(String pattern) throws ConfigValidationException {
        if ("*".equals(pattern)) {
            return wildcard();
        } else if (pattern.startsWith("/") && pattern.endsWith("/")) {
            return new JavaPattern(pattern.substring(1, pattern.length() - 1));
        } else if (pattern.endsWith("*") && pattern.indexOf('*') == pattern.length() - 1 && !pattern.contains("?")) {
            return new PrefixPattern(pattern.substring(0, pattern.length() - 1));
        } else if (pattern.contains("?") || pattern.contains("*")) {
            return new SimplePatternImpl(pattern);
        } else {
            return new Constant(pattern);
        }
    }

    public static Pattern create(String pattern1, String pattern2, String... more) throws ConfigValidationException {
        if (pattern2 == null && (more == null || more.length == 0)) {
            return create(pattern1);
        }

        if (pattern1 == null && (more == null || more.length == 0)) {
            return create(pattern2);
        }

        return CompoundPattern.create(ImmutableList.of(pattern1, pattern2).with(more));
    }

    public static Pattern create(List<String> patterns) throws ConfigValidationException {
        if (patterns.size() == 0) {
            return PatternImpl.BLANK;
        } else if (patterns.size() == 1) {
            return create(patterns.get(0));
        } else {
            return CompoundPattern.create(patterns);
        }
    }

    public static Pattern createWithoutExclusions(Collection<String> patterns) throws ConfigValidationException {
        if (patterns.size() == 0) {
            return PatternImpl.BLANK;
        } else if (patterns.size() == 1) {
            return create(patterns.iterator().next());
        } else {
            return CompoundPattern.createWithoutExclusions(patterns);
        }
    }
    
    public static Pattern createUnchecked(String pattern) {
        try {
            return create(pattern);
        } catch (ConfigValidationException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public static Pattern createUnchecked(String pattern1, String pattern2, String... more) {
        try {
            return create(pattern1, pattern2, more);
        } catch (ConfigValidationException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public static Pattern createUnchecked(List<String> patterns) {
        try {
            return create(patterns);
        } catch (ConfigValidationException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public static Pattern createPrefixPattern(String prefix) {
        return new PatternImpl.PrefixPattern(prefix);
    }
    
    public static Pattern createPrefixPattern(String prefix1, String... more) {        
        return createPrefixPattern(ImmutableSet.of(prefix1, more));
    }
    
    public static Pattern createPrefixPattern(Collection<String> prefixes) {
        return new PatternImpl.TrieCompoundPattern(ImmutableSet.of(prefixes).map(PatternImpl.PrefixPattern::new));
    }
    
    public static Pattern join(Collection<Pattern> patterns) {
        if (patterns.size() == 0) {
            return PatternImpl.BLANK;
        } else if (patterns.size() == 1) {
            return patterns.iterator().next();
        } else {
            return CompoundPattern.join(patterns);
        }
    }

    public static Pattern parse(DocNode docNode, Parser.Context context) throws ConfigValidationException {
        if (docNode.isList()) {
            return create(docNode.toListOfStrings());
        } else {
            return create(docNode.toString());
        }
    }

    public static boolean isConstant(String pattern) {
        if ("*".equals(pattern)) {
            return false;
        } else if (pattern.startsWith("/") && pattern.endsWith("/")) {
            return false;
        } else if (pattern.contains("?") || pattern.contains("*")) {
            return false;
        } else {
            return true;
        }
    }

    public static Pattern wildcard() {
        return PatternImpl.WILDCARD;
    }
    
    public static Pattern blank() {
        return PatternImpl.BLANK;
    }

    boolean matches(String string);

    boolean matches(Iterable<String> string);

    ImmutableSet<String> getMatching(ImmutableSet<String> strings);

    Iterable<String> iterateMatching(Iterable<String> strings);

    <E> Iterable<E> iterateMatching(Iterable<E> elements, Function<E, String> toStringFunction);

    ImmutableSet<String> getConstants();

    ImmutableSet<String> getPatterns();

    <E> ImmutableSet<E> getMatching(ImmutableSet<E> set, Function<E, String> stringMappingFunction);

    Pattern excluding(Pattern exludingPattern);

    boolean isWildcard();
    
    boolean isBlank();

}
