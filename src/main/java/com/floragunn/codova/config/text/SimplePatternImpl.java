/*
 * Contains code from Apache Commons IO licensed under the Apache 2 license:
 * 
 *  - https://github.com/apache/commons-io/blob/b6a22186d8246d973c6dc9b7fd2aaa84f34cccbf/src/main/java/org/apache/commons/io/FilenameUtils.java
 *  
 * For Apache Commons IO:
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * 
 * 
 * Modifications:
 *  
 * Copyright 2022 floragunn GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.floragunn.codova.config.text;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;

import com.floragunn.fluent.collections.ImmutableList;
import com.floragunn.fluent.collections.ImmutableSet;

public class SimplePatternImpl extends PatternImpl {
    private final String source;
    private final String[] tokens;
    private final String prefix;

    SimplePatternImpl(String pattern) {
        this.source = pattern;
        this.tokens = splitOnTokens(pattern);
        this.prefix = !(this.tokens[0].equals("*") || this.tokens[0].equals("?")) ? this.tokens[0] : null;
    }

    @Override
    public boolean matches(String string) {
        return matches(string, 0, 0);
    }

    @Override
    boolean matchesSkipPrefix(String string) {
        if (prefix != null) {
            return matches(string, prefix.length(), 1);
        } else {
            return matches(string);
        }
    }

    private boolean matches(String string, int startTextIdx, int startWcsIdx) {
        if (string == null) {
            return false;
        }

        final String[] wcs = this.tokens;
        boolean anyChars = false;
        int textIdx = startTextIdx;
        int wcsIdx = startWcsIdx;
        final Deque<int[]> backtrack = new ArrayDeque<>(wcs.length);

        // loop around a backtrack stack, to handle complex * matching
        do {
            if (!backtrack.isEmpty()) {
                final int[] array = backtrack.pop();
                wcsIdx = array[0];
                textIdx = array[1];
                anyChars = true;
            }

            // loop whilst tokens and text left to process
            while (wcsIdx < wcs.length) {

                if (wcs[wcsIdx].equals("?")) {
                    // ? so move to next text char
                    textIdx++;
                    if (textIdx > string.length()) {
                        break;
                    }
                    anyChars = false;

                } else if (wcs[wcsIdx].equals("*")) {
                    // set any chars status
                    anyChars = true;
                    if (wcsIdx == wcs.length - 1) {
                        textIdx = string.length();
                    }

                } else {
                    // matching text token
                    if (anyChars) {
                        // any chars then try to locate text token
                        textIdx = string.indexOf(wcs[wcsIdx], textIdx);
                        if (textIdx == -1) {
                            // token not found
                            break;
                        }
                        final int repeat = string.indexOf(wcs[wcsIdx], textIdx + 1);
                        if (repeat >= 0) {
                            backtrack.push(new int[] { wcsIdx, repeat });
                        }
                    } else if (!string.regionMatches(false, textIdx, wcs[wcsIdx], 0, wcs[wcsIdx].length())) {
                        // matching from current position
                        // couldn't match token
                        break;
                    }

                    // matched text token, move text index to end of matched token
                    textIdx += wcs[wcsIdx].length();
                    anyChars = false;
                }

                wcsIdx++;
            }

            // full match
            if (wcsIdx == wcs.length && textIdx == string.length()) {
                return true;
            }

        } while (!backtrack.isEmpty());

        return false;
    }

    @Override
    public int hashCode() {
        return source.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof SimplePatternImpl)) {
            return false;
        }

        return ((SimplePatternImpl) obj).source.equals(this.source);
    }

    @Override
    public String toString() {
        return source;
    }

    @Override
    public ImmutableSet<String> getConstants() {
        return ImmutableSet.empty();
    }

    @Override
    public ImmutableSet<String> getPatterns() {
        return ImmutableSet.of(source);
    }

    @Override
    public Object toBasicObject() {
        return ImmutableList.of(source);
    }

    private static String[] splitOnTokens(String text) {
        char[] array = text.toCharArray();
        ArrayList<String> list = new ArrayList<>();
        StringBuilder buffer = new StringBuilder();
        char prevChar = 0;

        for (final char ch : array) {
            if (ch == '?' || ch == '*') {
                if (buffer.length() != 0) {
                    list.add(buffer.toString());
                    buffer.setLength(0);
                }
                if (ch == '?') {
                    list.add("?");
                } else if (prevChar != '*') {// ch == '*' here; check if previous char was '*'
                    list.add("*");
                }
            } else {
                buffer.append(ch);
            }
            prevChar = ch;
        }
        if (buffer.length() != 0) {
            list.add(buffer.toString());
        }

        return list.toArray(new String[list.size()]);
    }

    @Override
    String getPrefix() {
        return prefix;
    }

}
