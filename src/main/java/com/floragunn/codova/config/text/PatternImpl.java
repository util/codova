/*
 * Copyright 2022 floragunn GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.floragunn.codova.config.text;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.regex.PatternSyntaxException;

import com.floragunn.codova.validation.ConfigValidationException;
import com.floragunn.codova.validation.ValidationErrors;
import com.floragunn.codova.validation.errors.InvalidAttributeValue;
import com.floragunn.codova.validation.errors.ValidationError;
import com.floragunn.fluent.collections.ImmutableList;
import com.floragunn.fluent.collections.ImmutableSet;

public abstract class PatternImpl implements Pattern {
    abstract String getPrefix();

    abstract boolean matchesSkipPrefix(String string);

    @Override
    public ImmutableSet<String> getMatching(ImmutableSet<String> strings) {
        return strings.matching((s) -> matches(s));
    }

    @Override
    public <E> ImmutableSet<E> getMatching(ImmutableSet<E> set, Function<E, String> stringMappingFunction) {
        return set.matching((e) -> matches(stringMappingFunction.apply(e)));
    }

    @Override
    public boolean matches(Iterable<String> strings) {
        for (String string : strings) {
            if (matches(string)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean test(String string) {
        return matches(string);
    }

    @Override
    public Iterable<String> iterateMatching(Iterable<String> strings) {
        return new Iterable<String>() {

            @Override
            public Iterator<String> iterator() {
                Iterator<String> delegate = strings.iterator();

                return new Iterator<String>() {

                    private String next;

                    @Override
                    public boolean hasNext() {
                        if (next == null) {
                            init();
                        }

                        return next != null;
                    }

                    @Override
                    public String next() {
                        if (next == null) {
                            init();
                        }
                        
                        String result = next;
                        next = null;
                        return result;
                    }

                    private void init() {
                        while (delegate.hasNext()) {
                            String candidate = delegate.next();

                            if (matches(candidate)) {
                                next = candidate;
                                break;
                            }
                        }
                    }
                };
            }

        };
    }

    @Override
    public <E> Iterable<E> iterateMatching(Iterable<E> elements, Function<E, String> toStringFunction) {
        return new Iterable<E>() {

            @Override
            public Iterator<E> iterator() {
                Iterator<E> delegate = elements.iterator();

                return new Iterator<E>() {

                    private E next;

                    @Override
                    public boolean hasNext() {
                        if (next == null) {
                            init();
                        }

                        return next != null;
                    }

                    @Override
                    public E next() {
                        if (next == null) {
                            init();
                        }
                        
                        E result = next;
                        next = null;
                        return result;
                    }

                    private void init() {
                        while (delegate.hasNext()) {
                            E candidate = delegate.next();

                            if (matches(toStringFunction.apply(candidate))) {
                                next = candidate;
                                break;
                            }
                        }
                    }
                };
            }

        };
    }
    
    @Override
    public boolean isWildcard() {
        return false;
    }

    @Override
    public boolean isBlank() {
        return false;
    }

    @Override
    public Pattern excluding(Pattern exludingPattern) {
        if (exludingPattern == BLANK) {
            return this;
        }

        return new ExcludingPattern(exludingPattern, this);
    }

    static class Constant extends PatternImpl {
        private final String value;

        Constant(String value) {
            this.value = value;
        }

        @Override
        public boolean matches(String string) {
            return value.equals(string);
        }

        @Override
        public Iterable<String> iterateMatching(Iterable<String> strings) {
            if (strings instanceof ImmutableSet) {
                ImmutableSet<String> set = (ImmutableSet<String>) strings;

                if (set.size() == 0) {
                    return set;
                } else if (set.size() == 1) {
                    String value = set.only();

                    if (matches(value)) {
                        return set;
                    } else {
                        return ImmutableSet.empty();
                    }
                }
            }
            return super.iterateMatching(strings);
        }

        public <E> Iterable<E> iterateMatching(Iterable<E> elements, Function<E, String> toStringFunction) {
            if (elements instanceof ImmutableSet) {
                ImmutableSet<E> set = (ImmutableSet<E>) elements;

                if (set.size() == 0) {
                    return set;
                } else if (set.size() == 1) {
                    E value = set.only();

                    if (matches(toStringFunction.apply(value))) {
                        return set;
                    } else {
                        return ImmutableSet.empty();
                    }
                }
            }
            return super.iterateMatching(elements, toStringFunction);
        }
        
        @Override
        public ImmutableSet<String> getConstants() {
            return ImmutableSet.of(value);
        }

        @Override
        public ImmutableSet<String> getPatterns() {
            return ImmutableSet.empty();
        }

        String getValue() {
            return value;
        }

        @Override
        public Object toBasicObject() {
            return value;
        }

        @Override
        public String toString() {
            return value;
        }

        @Override
        String getPrefix() {
            return value;
        }

        @Override
        boolean matchesSkipPrefix(String string) {
            return true;
        }

    }

    static class PrefixPattern extends PatternImpl {
        private final String prefix;
        private final String source;

        PrefixPattern(String prefix) {
            this.prefix = prefix;
            this.source = prefix + "*";
        }

        @Override
        public boolean matches(String string) {
            return string.startsWith(this.prefix);
        }

        @Override
        public int hashCode() {
            return prefix.hashCode();
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof PrefixPattern)) {
                return false;
            }

            return ((PrefixPattern) obj).prefix.equals(this.prefix);
        }

        @Override
        public String toString() {
            return source;
        }

        @Override
        public ImmutableSet<String> getConstants() {
            return ImmutableSet.empty();
        }

        @Override
        public ImmutableSet<String> getPatterns() {
            return ImmutableSet.of(source);
        }

        @Override
        public Object toBasicObject() {
            return ImmutableList.of(source);
        }

        @Override
        String getPrefix() {
            return prefix;
        }

        @Override
        boolean matchesSkipPrefix(String string) {
            return true;
        }
    }

    static class JavaPattern extends PatternImpl {
        private final java.util.regex.Pattern javaPattern;
        private final String patternString;

        JavaPattern(String pattern) throws ConfigValidationException {
            try {
                this.javaPattern = java.util.regex.Pattern.compile(pattern);
                this.patternString = pattern;
            } catch (PatternSyntaxException e) {
                throw new ConfigValidationException(new InvalidAttributeValue(null, pattern, "A regular expression pattern"));
            }
        }

        @Override
        public boolean matches(String string) {
            return javaPattern.matcher(string).matches();
        }

        @Override
        public int hashCode() {
            return patternString.hashCode();
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof JavaPattern)) {
                return false;
            }

            return ((JavaPattern) obj).patternString.equals(this.patternString);
        }

        @Override
        public String toString() {
            return patternString;
        }

        @Override
        public ImmutableSet<String> getConstants() {
            return ImmutableSet.empty();
        }

        @Override
        public ImmutableSet<String> getPatterns() {
            return ImmutableSet.of(patternString);
        }

        @Override
        public Object toBasicObject() {
            return ImmutableList.of(patternString);
        }

        @Override
        String getPrefix() {
            return null;
        }

        @Override
        boolean matchesSkipPrefix(String string) {
            return matches(string);
        }
    }

    static class CompoundPattern extends PatternImpl {
        private final String asString;
        private final ImmutableList<String> source;
        private final ImmutableList<Pattern> patterns;
        private final ImmutableSet<String> constants;

        static Pattern create(List<String> patterns) throws ConfigValidationException {
            // For negated patterns, we follow this logic:
            // Negated patterns apply for the patterns that precede them, but not for the patterns that succeed them.
            // Example: a*,-abc*,abcd 
            // In this example, the negation -abc* applies to a*, but not to abcd. Thus, this pattern matches ab, abcd, but not abc or abcde
            
            int start = 0;
            int patternCount = patterns.size();
            ValidationErrors validationErrors = new ValidationErrors();
            List<Pattern> result = new ArrayList<>(patternCount);

            for (int i = 0; i < patternCount; i++) {
                String patternString = patterns.get(i);

                if (patternString.startsWith("-")) {
                    if (start == i) {
                        validationErrors.add(new ValidationError(null, "Exclusions must come after normal patterns"));
                        continue;
                    }
                    try {
                        // All negations starting from here apply for the preceding patterns
                        List<String> negations = collectNegationsStartingAt(patterns, i);
                        List<String> precedingPatterns = patterns.subList(start, i);
                        Pattern base = createWithoutExclusions(precedingPatterns);
                        Pattern exclusion = Pattern.create(negations);
                        result.add(new ExcludingPattern(exclusion, base));
                        
                        if (negations.size() + i >= patternCount) {
                            // We are at the end
                            start = patterns.size();
                            break;
                        } else {
                            int next = findNextNonNegation(patterns, i + 1);
                            if (next == -1) {
                                // We are at the end
                                start = patterns.size();
                                break;
                            } else {
                                i = next - 1;
                                start = next;
                            }
                        }
                        
                    } catch (ConfigValidationException e) {
                        validationErrors.add(null, e);
                    }
                }
            }
                       
            if (start < patterns.size()) {
                try {
                    result.add(createWithoutExclusions(patterns.subList(start, patterns.size())));
                } catch (ConfigValidationException e) {
                    validationErrors.add(null, e);
                }
            }

            return Pattern.join(result);
        }

        static Pattern createWithoutExclusions(Collection<String> patterns) throws ConfigValidationException {
            ValidationErrors validationErrors = new ValidationErrors();

            ImmutableSet.Builder<PatternImpl> patternsWithPrefix = new ImmutableSet.Builder<>();
            ImmutableSet.Builder<Pattern> patternsWithoutPrefix = new ImmutableSet.Builder<>();
            ImmutableSet.Builder<String> constantSet = new ImmutableSet.Builder<>();

            int i = 0;

            for (String patternString : patterns) {
                try {
                    Pattern pattern = Pattern.create(patternString);

                    if (pattern == WILDCARD) {
                        return pattern;
                    } else if (pattern instanceof Constant) {
                        constantSet.with(patternString);
                    } else if (pattern instanceof PatternImpl) {
                        PatternImpl patternImpl = (PatternImpl) pattern;

                        String prefix = patternImpl.getPrefix();

                        if (prefix != null) {
                            patternsWithPrefix.with(patternImpl);
                        } else {
                            patternsWithoutPrefix.with(patternImpl);
                        }
                    } else {
                        patternsWithoutPrefix.with(pattern);
                    }

                    i++;
                } catch (ConfigValidationException e) {
                    validationErrors.add(String.valueOf(i), e);
                }
            }

            validationErrors.throwExceptionForPresentErrors();

            int totalCount = patternsWithoutPrefix.size() + patternsWithPrefix.size() + constantSet.size();

            if (totalCount == 0) {
                return BLANK;
            } else if (totalCount == 1) {
                if (constantSet.size() == 1) {
                    return new Constant(constantSet.any());
                } else if (patternsWithoutPrefix.size() == 1) {
                    return patternsWithoutPrefix.any();
                } else if (patternsWithPrefix.size() == 1) {
                    return patternsWithPrefix.any();
                }
            }

            ImmutableList<Pattern> compiledPatterns;

            if (patternsWithPrefix.size() == 0) {
                compiledPatterns = patternsWithoutPrefix.build().toList();
            } else {
                TrieCompoundPattern trieCompoundPattern = new TrieCompoundPattern(patternsWithPrefix.build());
                compiledPatterns = ImmutableList.<Pattern>of(trieCompoundPattern).with(patternsWithoutPrefix.build().toList());
            }

            return new CompoundPattern(compiledPatterns, constantSet.build(), patterns.toString(), ImmutableList.of(patterns));
        }

        static Pattern join(Collection<Pattern> patterns) {
            for (Pattern pattern : patterns) {
                if (pattern == WILDCARD) {
                    return pattern;
                }
            }

            ImmutableSet.Builder<PatternImpl> patternsWithPrefix = new ImmutableSet.Builder<>();
            ImmutableSet.Builder<Pattern> patternsWithoutPrefix = new ImmutableSet.Builder<>();
            ImmutableSet.Builder<String> constantSet = new ImmutableSet.Builder<>();

            collect(patterns, patternsWithPrefix, patternsWithoutPrefix, constantSet);

            int totalCount = patternsWithoutPrefix.size() + patternsWithPrefix.size() + constantSet.size();

            if (totalCount == 0) {
                return BLANK;
            } else if (totalCount == 1) {
                if (constantSet.size() == 1) {
                    return new Constant(constantSet.any());
                } else if (patternsWithoutPrefix.size() == 1) {
                    return patternsWithoutPrefix.any();
                } else if (patternsWithPrefix.size() == 1) {
                    return patternsWithPrefix.any();
                }
            }

            ImmutableList<Pattern> compiledPatterns;

            if (patternsWithPrefix.size() == 0) {
                compiledPatterns = patternsWithoutPrefix.build().toList();
            } else {
                TrieCompoundPattern trieCompoundPattern = new TrieCompoundPattern(patternsWithPrefix.build());

                if (constantSet.size() == 0 && patternsWithoutPrefix.size() == 0) {
                    // We only have the patterns defined in trieCompoundPattern. We can just return this.
                    return trieCompoundPattern;
                }

                compiledPatterns = ImmutableList.<Pattern>of(trieCompoundPattern).with(patternsWithoutPrefix.build().toList());
            }

            return new CompoundPattern(compiledPatterns, constantSet.build(), patterns.toString(), ImmutableList.empty());
        }

        private static void collect(Collection<? extends Pattern> patterns, ImmutableSet.Builder<PatternImpl> patternsWithPrefix,
                ImmutableSet.Builder<Pattern> patternsWithoutPrefix, ImmutableSet.Builder<String> constantSet) {
            for (Pattern pattern : patterns) {

                if (pattern instanceof Constant) {
                    constantSet.with(((Constant) pattern).getValue());
                } else if (pattern instanceof CompoundPattern) {
                    CompoundPattern compoundPattern = (CompoundPattern) pattern;
                    constantSet.with(compoundPattern.constants);
                    collect(compoundPattern.patterns, patternsWithPrefix, patternsWithoutPrefix, constantSet);
                } else if (pattern instanceof TrieCompoundPattern) {
                    TrieCompoundPattern trieCompoundPattern = (TrieCompoundPattern) pattern;
                    collect(trieCompoundPattern.patterns, patternsWithPrefix, patternsWithoutPrefix, constantSet);
                } else if (pattern instanceof PatternImpl) {
                    PatternImpl patternImpl = (PatternImpl) pattern;
                    String prefix = patternImpl.getPrefix();

                    if (prefix != null) {
                        patternsWithPrefix.with(patternImpl);
                    } else {
                        patternsWithoutPrefix.with(patternImpl);
                    }
                } else {
                    patternsWithoutPrefix.with(pattern);
                }

            }
        }
        
        private static List<String> collectNegationsStartingAt(List<String> patterns, int start) {
            List<String> result = new ArrayList<>();
            
            for (int i = start; i < patterns.size(); i++) {
                String pattern = patterns.get(i);
                
                if (pattern.startsWith("-")) {
                    result.add(pattern.substring(1));
                }
            }
            
            return result;
        }
        
        private static int findNextNonNegation(List<String> patterns, int start) {
            for (int i = start; i < patterns.size(); i++) {
                String pattern = patterns.get(i);

                if (!pattern.startsWith("-")) {
                    return i;
                }
            }
            
            return -1;
        }

        CompoundPattern(ImmutableList<Pattern> patterns, ImmutableSet<String> constants, String asString, ImmutableList<String> source) {
            this.constants = constants;
            this.patterns = patterns;
            this.asString = asString;
            this.source = source;
        }

        @Override
        public boolean matches(String string) {
            if (constants.contains(string)) {
                return true;
            }

            for (Pattern pattern : this.patterns) {
                if (pattern.matches(string)) {
                    return true;
                }
            }

            return false;
        }

        @Override
        public int hashCode() {
            return patterns.hashCode() + constants.hashCode();
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof CompoundPattern)) {
                return false;
            }

            return ((CompoundPattern) obj).patterns.equals(this.patterns) && ((CompoundPattern) obj).constants.equals(this.constants);
        }

        @Override
        public String toString() {
            return asString;
        }

        @Override
        public ImmutableSet<String> getConstants() {
            return constants;
        }

        @Override
        public ImmutableSet<String> getPatterns() {
            return patterns.stream().flatMap(p -> p.getPatterns().stream()).collect(ImmutableSet.collector());
        }

        ImmutableList<Pattern> getPatternObjects() {
            return patterns;
        }

        @Override
        public Object toBasicObject() {
            return source;
        }

        @Override
        String getPrefix() {
            return null;
        }

        @Override
        boolean matchesSkipPrefix(String string) {
            return matches(string);
        }
    }

    static class TrieCompoundPattern extends PatternImpl {
        private final String asString;
        private final ImmutableSet<PatternImpl> patterns;

        private final Node root;

        TrieCompoundPattern(ImmutableSet<PatternImpl> patterns) {
            this.patterns = patterns;
            this.asString = patterns.toString();
            this.root = build(patterns);
        }

        @Override
        public boolean matches(String string) {
            return root.matches(string, 0);
        }

        @Override
        public int hashCode() {
            return patterns.hashCode();
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof TrieCompoundPattern)) {
                return false;
            }

            return ((TrieCompoundPattern) obj).patterns.equals(this.patterns);
        }

        @Override
        public String toString() {
            return asString;
        }

        @Override
        public ImmutableSet<String> getConstants() {
            return ImmutableSet.empty();
        }

        @Override
        public ImmutableSet<String> getPatterns() {
            return patterns.mapFlat(p -> p.getPatterns());
        }

        @Override
        public Object toBasicObject() {
            return patterns;
        }

        @Override
        String getPrefix() {
            return null;
        }

        @Override
        boolean matchesSkipPrefix(String string) {
            return matches(string);
        }

        private static Node build(Collection<PatternImpl> patterns) {
            Node.Builder root = new Node.Builder("");

            for (PatternImpl pattern : patterns) {
                root.add(pattern.getPrefix(), 0, pattern);
            }

            return root.build();
        }

        static class Node {
            private final char[] nextKeys;
            private final Node[] nextNodes;
            private final PatternImpl pattern;

            Node(char[] nextKeys, Node[] nextNodes, PatternImpl pattern) {
                this.nextKeys = nextKeys;
                this.nextNodes = nextNodes;
                this.pattern = pattern;
            }

            boolean matches(String value, int offset) {
                if (pattern != null && pattern.matchesSkipPrefix(value)) {
                    return true;
                }

                if (offset >= value.length()) {
                    return false;
                }

                char c = value.charAt(offset);

                if (nextKeys.length == 1) {
                    if (nextKeys[0] == c) {
                        return nextNodes[0].matches(value, offset + 1);
                    } else {
                        return false;
                    }
                }

                int pos = Arrays.binarySearch(nextKeys, c);

                if (pos >= 0) {
                    return nextNodes[pos].matches(value, offset + 1);
                } else {
                    return false;
                }
            }

            static class Builder {
                private TreeMap<Character, Builder> nodes = new TreeMap<>();
                private Set<PatternImpl> patterns = new HashSet<>();
                private final String prefix;

                Builder(String prefix) {
                    this.prefix = prefix;
                }

                void add(String value, int pos, PatternImpl pattern) {
                    if (value == null) {
                        patterns.add(pattern);
                        return;
                    }

                    char c = value.charAt(pos);

                    Builder childNode = nodes.computeIfAbsent(c, (c0) -> new Builder(value.substring(0, pos + 1)));

                    if (pos == value.length() - 1) {
                        childNode.patterns.add(pattern);
                    } else {
                        childNode.add(value, pos + 1, pattern);
                    }
                }

                Node build() {
                    char[] nextKeys = new char[this.nodes.size()];
                    Node[] nextNodes = new Node[this.nodes.size()];

                    int i = 0;

                    for (Map.Entry<Character, Builder> entry : this.nodes.entrySet()) {
                        nextKeys[i] = entry.getKey();
                        nextNodes[i] = entry.getValue().build();
                        i++;
                    }

                    return new Node(nextKeys, nextNodes, EqualPrefixCompoundPattern.join(patterns, prefix));
                }
            }
        }

    }

    static class EqualPrefixCompoundPattern extends PatternImpl {

        private final String asString;
        private final ImmutableList<String> source;
        private final ImmutableSet<PatternImpl> patterns;
        private final String sharedPrefix;

        static PatternImpl join(Collection<PatternImpl> patterns, String sharedPrefix) {

            for (PatternImpl pattern : patterns) {
                if (!sharedPrefix.equals(pattern.getPrefix())) {
                    throw new IllegalArgumentException("Pattern " + pattern + " does not have the prefix " + sharedPrefix);
                }
            }

            if (patterns.size() == 0) {
                return PatternImpl.BLANK;
            } else if (patterns.size() == 1) {
                return patterns.iterator().next();
            } else {
                return new EqualPrefixCompoundPattern(ImmutableSet.of(patterns), patterns.toString(), ImmutableList.empty(), sharedPrefix);
            }
        }

        EqualPrefixCompoundPattern(ImmutableSet<PatternImpl> patterns, String asString, ImmutableList<String> source, String sharedPrefix) {
            this.patterns = patterns;
            this.asString = asString;
            this.source = source;
            this.sharedPrefix = sharedPrefix;
        }

        @Override
        public boolean matches(String string) {
            for (Pattern pattern : this.patterns) {
                if (pattern.matches(string)) {
                    return true;
                }
            }

            return false;
        }

        @Override
        public int hashCode() {
            return patterns.hashCode();
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof EqualPrefixCompoundPattern)) {
                return false;
            }

            return ((EqualPrefixCompoundPattern) obj).patterns.equals(this.patterns);
        }

        @Override
        public String toString() {
            return asString;
        }

        @Override
        public ImmutableSet<String> getConstants() {
            return ImmutableSet.empty();
        }

        @Override
        public ImmutableSet<String> getPatterns() {
            return patterns.mapFlat(p -> p.getPatterns());
        }

        @Override
        public Object toBasicObject() {
            return source;
        }

        @Override
        String getPrefix() {
            return sharedPrefix;
        }

        @Override
        boolean matchesSkipPrefix(String string) {
            for (PatternImpl pattern : this.patterns) {
                if (pattern.matchesSkipPrefix(string)) {
                    return true;
                }
            }

            return false;
        }
    }

    static class ExcludingPattern extends PatternImpl {
        private final Pattern exclusions;
        private final Pattern base;
        private final String prefix;
        private String asString;

        ExcludingPattern(Pattern exclusions, Pattern base) {
            this.exclusions = exclusions;
            this.base = base;
            this.prefix = base instanceof PatternImpl ? ((PatternImpl) base).getPrefix() : null;
        }

        @Override
        public boolean matches(String string) {
            return !exclusions.matches(string) && base.matches(string);
        }

        @Override
        public ImmutableSet<String> getConstants() {
            return ImmutableSet.empty();
        }

        @Override
        public ImmutableSet<String> getPatterns() {
            return ImmutableSet.empty();
        }

        @Override
        public Object toBasicObject() {
            return base.toBasicObject();
        }

        @Override
        String getPrefix() {
            return prefix;
        }

        @Override
        boolean matchesSkipPrefix(String string) {
            return matches(string);
        }

        @Override
        public String toString() {
            String result = this.asString;
            
            if (result != null) {
                return result;
            }
            
            result = buildToString();            
            this.asString = result;        
            return result;
        }

        private String buildToString() {
            StringBuilder result = new StringBuilder(base.toString());

            for (String pattern : exclusions.getPatterns()) {
                result.append(", ");
                result.append("-").append(pattern);
            }
            
            for (String pattern : exclusions.getConstants()) {
                result.append(", ");
                result.append("-").append(pattern);
            }

            return result.toString();
        }
    }

    static Pattern WILDCARD = new Pattern() {

        @Override
        public boolean matches(String string) {
            return true;
        }

        @Override
        public Iterable<String> iterateMatching(Iterable<String> strings) {
            return strings;
        }

        @Override
        public <E> Iterable<E> iterateMatching(Iterable<E> elements, Function<E, String> toStringFunction) {
            return elements;
        }


        @Override
        public ImmutableSet<String> getMatching(ImmutableSet<String> strings) {
            return strings;
        }

        @Override
        public boolean matches(Iterable<String> string) {
            if (string.iterator().hasNext()) {
                return true;
            } else {
                return false;
            }
        }
        
        @Override
        public String toString() {
            return "*";
        }

        @Override
        public ImmutableSet<String> getConstants() {
            return ImmutableSet.empty();
        }

        @Override
        public ImmutableSet<String> getPatterns() {
            return ImmutableSet.of("*");
        }

        @Override
        public Object toBasicObject() {
            return ImmutableList.of("*");
        }

        @Override
        public <E> ImmutableSet<E> getMatching(ImmutableSet<E> set, Function<E, String> stringMappingFunction) {
            return set;
        }

        @Override
        public boolean test(String t) {
            return true;
        }

        @Override
        public Pattern excluding(Pattern exludingPattern) {
            return new ExcludingPattern(exludingPattern, this);
        }

        @Override
        public boolean isWildcard() {
            return true;
        }

        @Override
        public boolean isBlank() {
            return false;
        }
    };

    static PatternImpl BLANK = new PatternImpl() {

        @Override
        public boolean matches(String string) {
            return false;
        }

        @Override
        public Iterable<String> iterateMatching(Iterable<String> strings) {
            return ImmutableSet.empty();
        }

        @Override
        public ImmutableSet<String> getMatching(ImmutableSet<String> strings) {
            return ImmutableSet.empty();
        }

        @Override
        public boolean matches(Iterable<String> string) {
            return false;
        }

        @Override
        public String toString() {
            return "-/-";
        }

        @Override
        public ImmutableSet<String> getConstants() {
            return ImmutableSet.empty();
        }

        @Override
        public ImmutableSet<String> getPatterns() {
            return ImmutableSet.empty();
        }

        @Override
        public Object toBasicObject() {
            return ImmutableList.empty();
        }

        @Override
        public <E> ImmutableSet<E> getMatching(ImmutableSet<E> set, Function<E, String> stringMappingFunction) {
            return ImmutableSet.empty();
        }

        @Override
        public boolean test(String t) {
            return false;
        }

        @Override
        public Pattern excluding(Pattern exludingPattern) {
            return this;
        }

        @Override
        public boolean isWildcard() {
            return false;
        }

        @Override
        public boolean isBlank() {
            return true;
        }

        @Override
        String getPrefix() {
            return null;
        }

        @Override
        boolean matchesSkipPrefix(String string) {
            return false;
        }
    };

}
