/*
 * Copyright 2021 floragunn GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.floragunn.codova.documents;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.floragunn.codova.validation.ConfigValidationException;
import com.floragunn.codova.validation.ValidatingFunction;
import com.floragunn.codova.validation.ValidationErrors;
import com.floragunn.codova.validation.errors.InvalidAttributeValue;
import com.floragunn.codova.validation.errors.ValidationError;
import com.floragunn.fluent.collections.ImmutableList;
import com.floragunn.fluent.collections.ImmutableMap;
import com.floragunn.fluent.collections.ImmutableSet;
import com.floragunn.fluent.collections.OrderedImmutableMap;
import com.floragunn.fluent.collections.views.MapView;
import com.google.common.collect.Maps;
import com.jayway.jsonpath.JsonPath;

public abstract class DocNode implements Map<String, Object>, Document<Object> {
    private static final Logger log = LoggerFactory.getLogger(DocNode.class);

    public static final DocNode EMPTY = new PlainJavaObjectAdapter(Collections.EMPTY_MAP);
    public static final DocNode NULL = new PlainJavaObjectAdapter(null);

    public static DocNodeParserBuilder parse(Format format) {
        return new DocNodeParserBuilder(format);
    }

    public static DocNodeParserBuilder parse(ContentType contentType) {
        return new DocNodeParserBuilder(contentType.getFormat());
    }

    public static DocNode parse(UnparsedDocument<?> unparsedDoc) throws DocumentParseException {
        return wrap(unparsedDoc.parse());
    }

    public static DocNode of(String key, Object value) {
        int separator = key.indexOf('.');

        if (separator == -1) {
            return new PlainJavaObjectAdapter(Collections.singletonMap(key, value));
        } else {
            return of(key.substring(0, separator), DocNode.of(key.substring(separator + 1), value));
        }
    }

    public static DocNode of(String k1, Object v1, String k2, Object v2) {
        int s1 = k1.indexOf('.');
        int s2 = k2.indexOf('.');

        if (s1 != -1 && s2 != -1) {
            String k1b = k1.substring(0, s1);
            String k2b = k2.substring(0, s2);

            if (k1b.equals(k2b)) {
                return new PlainJavaObjectAdapter(OrderedImmutableMap.of(k1b, DocNode.of(k1.substring(s1 + 1), v1, k2.substring(s2 + 1), v2)));
            } else {
                return new PlainJavaObjectAdapter(
                        OrderedImmutableMap.of(k1b, DocNode.of(k1.substring(s1 + 1), v1), k2b, DocNode.of(k2.substring(s2 + 1), v2)));
            }

        } else if (s1 != -1) {
            return new PlainJavaObjectAdapter(OrderedImmutableMap.of(k1.substring(0, s1), DocNode.of(k1.substring(s1 + 1), v1), k2, v2));
        } else if (s2 != -1) {
            return new PlainJavaObjectAdapter(OrderedImmutableMap.of(k1, v1, k2.substring(0, s2), DocNode.of(k2.substring(s2 + 1), v2)));
        } else {
            // s1 == -1 && s2 == -1
            return new PlainJavaObjectAdapter(OrderedImmutableMap.of(k1, v1, k2, v2));
        }
    }

    public static DocNode of(String k1, Object v1, String k2, Object v2, String k3, Object v3) {
        int s1 = k1.indexOf('.');
        int s2 = k2.indexOf('.');
        int s3 = k3.indexOf('.');

        if (s1 != -1) {
            if (s2 != -1) {
                if (s3 != -1) {
                    String k1b = k1.substring(0, s1);
                    String k2b = k2.substring(0, s2);
                    String k3b = k3.substring(0, s3);

                    if (k1b.equals(k2b)) {
                        if (k2b.equals(k3b)) {
                            return new PlainJavaObjectAdapter(OrderedImmutableMap.of(k1b,
                                    DocNode.of(k1.substring(s1 + 1), v1, k2.substring(s2 + 1), v2, k3.substring(s3 + 1), v3)));
                        } else {
                            return new PlainJavaObjectAdapter(OrderedImmutableMap.of(k1b,
                                    DocNode.of(k1.substring(s1 + 1), v1, k2.substring(s2 + 1), v2), k3b, DocNode.of(k3.substring(s3 + 1), v3)));
                        }
                    } else {
                        if (k2b.equals(k3b)) {
                            return new PlainJavaObjectAdapter(OrderedImmutableMap.of(k1b, DocNode.of(k1.substring(s1 + 1), v1), k2b,
                                    DocNode.of(k2.substring(s2 + 1), v2, k3.substring(s3 + 1), v3)));
                        } else if (k1b.equals(k3b)) {
                            return new PlainJavaObjectAdapter(OrderedImmutableMap.of(k1b,
                                    DocNode.of(k1.substring(s1 + 1), v1, k3.substring(s3 + 1), v3), k2b, DocNode.of(k2.substring(s2 + 1), v2)));
                        } else {
                            return new PlainJavaObjectAdapter(OrderedImmutableMap.of(k1b, DocNode.of(k1.substring(s1 + 1), v1), k2b,
                                    DocNode.of(k2.substring(s2 + 1), v2), k3b, DocNode.of(k3.substring(s3 + 1), v3)));
                        }
                    }
                } else {
                    String k1b = k1.substring(0, s1);
                    String k2b = k2.substring(0, s2);

                    if (k1b.equals(k2b)) {
                        return new PlainJavaObjectAdapter(
                                OrderedImmutableMap.of(k1b, DocNode.of(k1.substring(s1 + 1), v1, k2.substring(s2 + 1), v2), k3, v3));
                    } else {
                        return new PlainJavaObjectAdapter(
                                OrderedImmutableMap.of(k1b, DocNode.of(k1.substring(s1 + 1), v1), k2b, DocNode.of(k2.substring(s2 + 1), v2), k3, v3));
                    }
                }
            } else {
                // s2 == -1
                if (s3 != -1) {
                    String k1b = k1.substring(0, s1);
                    String k3b = k3.substring(0, s3);

                    if (k1b.equals(k3b)) {
                        return new PlainJavaObjectAdapter(
                                OrderedImmutableMap.of(k1b, DocNode.of(k1.substring(s1 + 1), v1, k3.substring(s3 + 1), v3), k2, v2));
                    } else {
                        return new PlainJavaObjectAdapter(
                                OrderedImmutableMap.of(k1b, DocNode.of(k1.substring(s1 + 1), v1), k2, v2, k3b, DocNode.of(k3.substring(s3 + 1), v3)));
                    }
                } else {
                    // s2 == -1 && s3 == -1

                    return new PlainJavaObjectAdapter(
                            OrderedImmutableMap.of(k1.substring(0, s1), DocNode.of(k1.substring(s1 + 1), v1), k2, v2, k3, v3));
                }
            }
        } else {
            // s1 == -1
            if (s2 != -1) {
                if (s3 != -1) {
                    String k2b = k2.substring(0, s2);
                    String k3b = k3.substring(0, s3);

                    if (k2b.equals(k3b)) {
                        return new PlainJavaObjectAdapter(
                                OrderedImmutableMap.of(k1, v1, k2b, DocNode.of(k2.substring(s2 + 1), v2, k3.substring(s3 + 1), v3)));
                    } else {
                        return new PlainJavaObjectAdapter(
                                OrderedImmutableMap.of(k1, v1, k2b, DocNode.of(k2.substring(s2 + 1), v2), k3b, DocNode.of(k3.substring(s3 + 1), v3)));
                    }
                } else {
                    // s3 == -1
                    return new PlainJavaObjectAdapter(
                            OrderedImmutableMap.of(k1, v1, k2.substring(0, s2), DocNode.of(k2.substring(s2 + 1), v2), k3, v3));
                }
            } else {
                // s2 == -1
                if (s3 != -1) {
                    return new PlainJavaObjectAdapter(
                            OrderedImmutableMap.of(k1, v1, k2, v2, k3.substring(0, s3), DocNode.of(k3.substring(s3 + 1), v3)));
                } else {
                    // s1 == -1 && s2 == -1 && s3 == -1
                    return new PlainJavaObjectAdapter(OrderedImmutableMap.of(k1, v1, k2, v2, k3, v3));
                }
            }
        }
    }

    public static DocNode of(String k1, Object v1, String k2, Object v2, String k3, Object v3, String k4, Object v4) {
        int s1 = k1.indexOf('.');
        int s2 = k2.indexOf('.');
        int s3 = k3.indexOf('.');
        int s4 = k4.indexOf('.');

        if (s1 != -1) {
            if (s2 != -1) {
                if (s3 != -1) {
                    if (s4 != -1) {
                        String k1b = k1.substring(0, s1);
                        String k2b = k2.substring(0, s2);
                        String k3b = k3.substring(0, s3);
                        String k4b = k4.substring(0, s4);

                        if (k1b.equals(k2b)) {
                            if (k2b.equals(k3b)) {
                                if (k3b.equals(k4b)) {
                                    return new PlainJavaObjectAdapter(OrderedImmutableMap.of(//
                                            k1b, DocNode.of(k1.substring(s1 + 1), v1, k2.substring(s2 + 1), v2, k3.substring(s3 + 1), v3,
                                                    k4.substring(s4 + 1), v4)));
                                } else {
                                    return new PlainJavaObjectAdapter(OrderedImmutableMap.of(//
                                            k1b, DocNode.of(k1.substring(s1 + 1), v1, k2.substring(s2 + 1), v2, k3.substring(s3 + 1), v3), //
                                            k4b, DocNode.of(k4.substring(s4 + 1), v4)));
                                }
                            } else {
                                if (k3b.equals(k4b)) {
                                    // k1b == k2b != k3b == k4b
                                    return new PlainJavaObjectAdapter(OrderedImmutableMap.of(//
                                            k1b, DocNode.of(k1.substring(s1 + 1), v1, k2.substring(s2 + 1), v2), //
                                            k3b, DocNode.of(k3.substring(s3 + 1), v3, k4.substring(s4 + 1), v4)));
                                } else {
                                    if (k1b.equals(k4b)) {
                                        // k1b == k2b == k4b != k3b
                                        return new PlainJavaObjectAdapter(OrderedImmutableMap.of(//
                                                k1b, DocNode.of(k1.substring(s1 + 1), v1, k2.substring(s2 + 1), v2, k4.substring(s4 + 1), v4), //
                                                k3b, DocNode.of(k3.substring(s3 + 1), v3)));
                                    } else {
                                        // k1b == k2b != k3b != k4b
                                        return new PlainJavaObjectAdapter(OrderedImmutableMap.of(//
                                                k1b, DocNode.of(k1.substring(s1 + 1), v1, k2.substring(s2 + 1), v2), //
                                                k3b, DocNode.of(k3.substring(s3 + 1), v3), //
                                                k4b, DocNode.of(k4.substring(s4 + 1), v4)));
                                    }

                                }
                            }
                        } else {
                            if (k2b.equals(k3b)) {
                                if (k3b.equals(k4b)) {
                                    // k1b != k2b == k3b == k4b
                                    return new PlainJavaObjectAdapter(OrderedImmutableMap.of(//
                                            k1b, DocNode.of(k1.substring(s1 + 1), v1), //
                                            k2b, DocNode.of(k2.substring(s2 + 1), v2, k3.substring(s3 + 1), v3, k4.substring(s4 + 1), v4)));
                                } else {
                                    if (k1b.equals(k4b)) {
                                        // k1b == k4b != k2b == k3b
                                        return new PlainJavaObjectAdapter(OrderedImmutableMap.of(//
                                                k1b, DocNode.of(k1.substring(s1 + 1), v1, k4.substring(s4 + 1), v4), //
                                                k2b, DocNode.of(k2.substring(s2 + 1), v2, k3.substring(s3 + 1), v3)));
                                    } else {
                                        // k1b != k2b == k3b != k4b
                                        return new PlainJavaObjectAdapter(OrderedImmutableMap.of(//
                                                k1b, DocNode.of(k1.substring(s1 + 1), v1), //
                                                k2b, DocNode.of(k2.substring(s2 + 1), v2, k3.substring(s3 + 1), v3), //
                                                k4b, DocNode.of(k4.substring(s4 + 1), v4)));
                                    }
                                }
                            } else {
                                if (k3b.equals(k4b)) {
                                    if (k1b.equals(k3b)) {
                                        // k1b == k3b == k4b != k2b
                                        return new PlainJavaObjectAdapter(OrderedImmutableMap.of(//
                                                k1b, DocNode.of(k1.substring(s1 + 1), v1, k3.substring(s3 + 1), v3, k4.substring(s4 + 1), v4), //
                                                k2b, DocNode.of(k2.substring(s2 + 1), v2)));
                                    } else {
                                        // k1b != k2b != k3b == k4b
                                        return new PlainJavaObjectAdapter(OrderedImmutableMap.of(//
                                                k1b, DocNode.of(k1.substring(s1 + 1), v1), //
                                                k2b, DocNode.of(k2.substring(s2 + 1), v2), //
                                                k3b, DocNode.of(k3.substring(s3 + 1), v3, k4.substring(s4 + 1), v4)));
                                    }
                                } else {
                                    if (k1b.equals(k3b)) {
                                        if (k2b.equals(k4b)) {
                                            // k1b == k3b != k2b == k4b
                                            return new PlainJavaObjectAdapter(OrderedImmutableMap.of(//
                                                    k1b, DocNode.of(k1.substring(s1 + 1), v1, k3.substring(s3 + 1), v3), //
                                                    k2b, DocNode.of(k2.substring(s2 + 1), v2, k4.substring(s4 + 1), v4)));
                                        } else {
                                            // k1b == k3b != k2b != k4b                                            
                                            return new PlainJavaObjectAdapter(OrderedImmutableMap.of(//
                                                    k1b, DocNode.of(k1.substring(s1 + 1), v1, k3.substring(s3 + 1), v3), //
                                                    k2b, DocNode.of(k2.substring(s2 + 1), v2), //
                                                    k4b, DocNode.of(k4.substring(s4 + 1), v4)));
                                        }
                                    } else {
                                        if (k2b.equals(k4b)) {
                                            // k1b != k3b != k2b == k4b
                                            return new PlainJavaObjectAdapter(OrderedImmutableMap.of(//
                                                    k1b, DocNode.of(k1.substring(s1 + 1), v1), //
                                                    k2b, DocNode.of(k2.substring(s2 + 1), v2, k4.substring(s4 + 1), v4), //
                                                    k3b, DocNode.of(k3.substring(s3 + 1), v3)));

                                        } else {
                                            if (k1b.equals(k4b)) {
                                                // k1b == k4b != k2b != k3b                                                 
                                                return new PlainJavaObjectAdapter(OrderedImmutableMap.of(//
                                                        k1b, DocNode.of(k1.substring(s1 + 1), v1, k4.substring(s4 + 1), v4), //
                                                        k2b, DocNode.of(k2.substring(s2 + 1), v2), //
                                                        k3b, DocNode.of(k3.substring(s3 + 1), v3)));
                                            } else {
                                                // k1b != k2b != k3b != k4b
                                                return new PlainJavaObjectAdapter(OrderedImmutableMap.of(//
                                                        k1b, DocNode.of(k1.substring(s1 + 1), v1), //
                                                        k2b, DocNode.of(k2.substring(s2 + 1), v2), //
                                                        k3b, DocNode.of(k3.substring(s3 + 1), v3), //
                                                        k4b, DocNode.of(k4.substring(s4 + 1), v4)));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        // s4 == -1

                        String k1b = k1.substring(0, s1);
                        String k2b = k2.substring(0, s2);
                        String k3b = k3.substring(0, s3);

                        if (k1b.equals(k2b)) {
                            if (k2b.equals(k3b)) {
                                return new PlainJavaObjectAdapter(OrderedImmutableMap.of(k1b,
                                        DocNode.of(k1.substring(s1 + 1), v1, k2.substring(s2 + 1), v2, k3.substring(s3 + 1), v3), k4, v4));
                            } else {
                                // k1b == k2b != k3b
                                return new PlainJavaObjectAdapter(
                                        OrderedImmutableMap.of(k1b, DocNode.of(k1.substring(s1 + 1), v1, k2.substring(s2 + 1), v2), k3b,
                                                DocNode.of(k3.substring(s3 + 1), v3), k4, v4));
                            }
                        } else {
                            if (k2b.equals(k3b)) {
                                // k1b != k2b == k3b 
                                return new PlainJavaObjectAdapter(OrderedImmutableMap.of(//
                                        k1b, DocNode.of(k1.substring(s1 + 1), v1), //
                                        k2b, DocNode.of(k2.substring(s2 + 1), v2, k3.substring(s3 + 1), v3), //
                                        k4, v4));
                            } else if (k1b.equals(k3b)) {
                                // k1b == k3b != k2b 
                                return new PlainJavaObjectAdapter(OrderedImmutableMap.of(//
                                        k1b, DocNode.of(k1.substring(s1 + 1), v1, k3.substring(s3 + 1), v3), //
                                        k2b, DocNode.of(k2.substring(s2 + 1), v2), //
                                        k4, v4));

                            } else {
                                // k1b != k2b != k3b 
                                return new PlainJavaObjectAdapter(OrderedImmutableMap.of(//
                                        k1b, DocNode.of(k1.substring(s1 + 1), v1), //
                                        k2b, DocNode.of(k2.substring(s2 + 1), v2), //
                                        k3b, DocNode.of(k3.substring(s3 + 1), v3), //
                                        k4, v4));
                            }
                        }
                    }
                } else {
                    // s3 == -1

                    if (s4 != -1) {
                        String k1b = k1.substring(0, s1);
                        String k2b = k2.substring(0, s2);
                        String k4b = k4.substring(0, s4);

                        if (k1b.equals(k2b)) {
                            if (k2b.equals(k4b)) {
                                // k1b == k2b == k4b
                                return new PlainJavaObjectAdapter(OrderedImmutableMap.of(k1b,
                                        DocNode.of(k1.substring(s1 + 1), v1, k2.substring(s2 + 1), v2, k4.substring(s4 + 1), v4), k3, v3));
                            } else {
                                // k1b == k2b != k4b
                                return new PlainJavaObjectAdapter(
                                        OrderedImmutableMap.of(k1b, DocNode.of(k1.substring(s1 + 1), v1, k2.substring(s2 + 1), v2), k3, v3, k4b,
                                                DocNode.of(k4.substring(s4 + 1), v4)));
                            }
                        } else {
                            // k1b != k2b

                            if (k2b.equals(k4b)) {
                                // k1b != k2b == k4b
                                return new PlainJavaObjectAdapter(OrderedImmutableMap.of(//
                                        k1b, DocNode.of(k1.substring(s1 + 1), v1), //
                                        k2b, DocNode.of(k2.substring(s2 + 1), v2, k4.substring(s4 + 1), v4), //
                                        k3, v3));
                            } else {
                                if (k1b.equals(k4b)) {
                                    // k1b == k4b != k2b
                                    return new PlainJavaObjectAdapter(OrderedImmutableMap.of(//
                                            k1b, DocNode.of(k1.substring(s1 + 1), v1, k4.substring(s4 + 1), v4), //
                                            k2b, DocNode.of(k2.substring(s2 + 1), v2), //
                                            k3, v3));
                                } else {
                                    // k1b != k2b != k4b
                                    return new PlainJavaObjectAdapter(OrderedImmutableMap.of(//
                                            k1b, DocNode.of(k1.substring(s1 + 1), v1), //
                                            k2b, DocNode.of(k2.substring(s2 + 1), v2), //
                                            k3, v3, //
                                            k4b, DocNode.of(k4.substring(s4 + 1), v4)));
                                }
                            }
                        }

                    } else {
                        // s3 == -1 && s4 == -1

                        String k1b = k1.substring(0, s1);
                        String k2b = k2.substring(0, s2);

                        if (k1b.equals(k2b)) {
                            // k1b == k2b
                            return new PlainJavaObjectAdapter(
                                    OrderedImmutableMap.of(k1b, DocNode.of(k1.substring(s1 + 1), v1, k2.substring(s2 + 1), v2), k3, v3, k4, v4));
                        } else {
                            // k1b != k2b
                            return new PlainJavaObjectAdapter(OrderedImmutableMap.of(k1b, DocNode.of(k1.substring(s1 + 1), v1), k2b,
                                    DocNode.of(k2.substring(s2 + 1), v2), k3, v3, k4, v4));
                        }
                    }
                }
            } else {
                // s2 == -1
                if (s3 != -1) {
                    if (s4 != -1) {
                        String k1b = k1.substring(0, s1);
                        String k3b = k3.substring(0, s3);
                        String k4b = k4.substring(0, s4);

                        if (k1b.equals(k3b)) {
                            if (k3b.equals(k4b)) {
                                // k1b == k3b == k4b
                                return new PlainJavaObjectAdapter(OrderedImmutableMap.of(//
                                        k1b, DocNode.of(k1.substring(s1 + 1), v1, k3.substring(s3 + 1), v3, k4.substring(s4 + 1), v4), //
                                        k2, v2));
                            } else {
                                // k1b == k3b != k4b
                                return new PlainJavaObjectAdapter(OrderedImmutableMap.of(//
                                        k1b, DocNode.of(k1.substring(s1 + 1), v1, k3.substring(s3 + 1), v3), //
                                        k2, v2, //
                                        k4b, DocNode.of(k4.substring(s4 + 1), v4)));
                            }
                        } else {
                            if (k3b.equals(k4b)) {
                                // k1b != k3b == k4b
                                return new PlainJavaObjectAdapter(OrderedImmutableMap.of(//
                                        k1b, DocNode.of(k1.substring(s1 + 1), v1), //
                                        k2, v2, //
                                        k3b, DocNode.of(k3.substring(s3 + 1), v3, k4.substring(s4 + 1), v4)));
                            } else {
                                if (k1b.equals(k4b)) {
                                    // k1b == k4b != k3b
                                    return new PlainJavaObjectAdapter(OrderedImmutableMap.of(//
                                            k1b, DocNode.of(k1.substring(s1 + 1), v1, k4.substring(s4 + 1), v4), //
                                            k2, v2, //
                                            k3b, DocNode.of(k3.substring(s3 + 1), v3)));
                                } else {
                                    // k1b != k3b != k4b
                                    return new PlainJavaObjectAdapter(OrderedImmutableMap.of(//
                                            k1b, DocNode.of(k1.substring(s1 + 1), v1), //
                                            k2, v2, //
                                            k3b, DocNode.of(k3.substring(s3 + 1), v3), //
                                            k4b, DocNode.of(k4.substring(s4 + 1), v4)));
                                }

                            }
                        }
                    } else {
                        String k1b = k1.substring(0, s1);
                        String k3b = k3.substring(0, s3);

                        if (k1b.equals(k3b)) {
                            return new PlainJavaObjectAdapter(OrderedImmutableMap.of(//
                                    k1b, DocNode.of(k1.substring(s1 + 1), v1, k3.substring(s3 + 1), v3), //
                                    k2, v2, // 
                                    k4, v4));
                        } else {
                            return new PlainJavaObjectAdapter(OrderedImmutableMap.of(//
                                    k1b, DocNode.of(k1.substring(s1 + 1), v1), //
                                    k2, v2, //
                                    k3b, DocNode.of(k3.substring(s3 + 1), v3), //
                                    k4, v4));
                        }
                    }
                } else {
                    if (s4 != -1) {
                        String k1b = k1.substring(0, s1);
                        String k4b = k4.substring(0, s4);

                        if (k1b.equals(k4b)) {
                            return new PlainJavaObjectAdapter(OrderedImmutableMap.of(//
                                    k1b, DocNode.of(k1.substring(s1 + 1), v1, k4.substring(s4 + 1), v4), //
                                    k2, v2, //
                                    k3, v3));
                        } else {
                            return new PlainJavaObjectAdapter(OrderedImmutableMap.of(//
                                    k1b, DocNode.of(k1.substring(s1 + 1), v1), //
                                    k2, v2, //
                                    k3, v3, //
                                    k4b, DocNode.of(k4.substring(s4 + 1), v4)));
                        }
                    } else {
                        // s2 == -1 && s3 == -1 && s4 == -1
                        return new PlainJavaObjectAdapter(OrderedImmutableMap.of(//
                                k1.substring(0, s1), DocNode.of(k1.substring(s1 + 1), v1), //
                                k2, v2, k3, v3, k4, v4));
                    }
                }
            }
        } else {
            // s1 == -1
            if (s2 != -1) {
                if (s3 != -1) {
                    if (s4 != -1) {
                        String k2b = k2.substring(0, s2);
                        String k3b = k3.substring(0, s3);
                        String k4b = k4.substring(0, s4);

                        if (k2b.equals(k3b)) {
                            if (k3b.equals(k4b)) {
                                // k2b == k3b == k4b
                                return new PlainJavaObjectAdapter(OrderedImmutableMap.of(k1, v1, k2b,
                                        DocNode.of(k2.substring(s2 + 1), v2, k3.substring(s3 + 1), v3, k4.substring(s4 + 1), v4)));
                            } else {
                                // k2b == k3b != k4b
                                return new PlainJavaObjectAdapter(OrderedImmutableMap.of(k1, v1, k2b,
                                        DocNode.of(k2.substring(s2 + 1), v2, k3.substring(s3 + 1), v3), k4b, DocNode.of(k4.substring(s4 + 1), v4)));
                            }
                        } else {
                            if (k3b.equals(k4b)) {
                                // k2b != k3b == k4b
                                return new PlainJavaObjectAdapter(OrderedImmutableMap.of(k1, v1, k2b, DocNode.of(k2.substring(s2 + 1), v2), k3b,
                                        DocNode.of(k3.substring(s3 + 1), v3, k4.substring(s4 + 1), v4)));
                            } else {
                                if (k2b.equals(k4b)) {
                                    // k2b == k4b != k3b                             
                                    return new PlainJavaObjectAdapter(OrderedImmutableMap.of(//
                                            k1, v1, //
                                            k2b, DocNode.of(k2.substring(s2 + 1), v2, k4.substring(s4 + 1), v4), //
                                            k3b, DocNode.of(k3.substring(s3 + 1), v3)));
                                } else {
                                    // k2b != k3b != k4b                                
                                    return new PlainJavaObjectAdapter(OrderedImmutableMap.of(//
                                            k1, v1, //
                                            k2b, DocNode.of(k2.substring(s2 + 1), v2), //
                                            k3b, DocNode.of(k3.substring(s3 + 1), v3), //
                                            k4b, DocNode.of(k4.substring(s4 + 1), v4)));
                                }
                            }
                        }
                    } else {
                        String k2b = k2.substring(0, s2);
                        String k3b = k3.substring(0, s3);

                        if (k2b.equals(k3b)) {
                            // k2b == k3b 
                            return new PlainJavaObjectAdapter(OrderedImmutableMap.of(//
                                    k1, v1, //
                                    k2b, DocNode.of(k2.substring(s2 + 1), v2, k3.substring(s3 + 1), v3), //
                                    k4, v4));
                        } else {
                            // k2b != k3b
                            return new PlainJavaObjectAdapter(OrderedImmutableMap.of(//
                                    k1, v1, //
                                    k2b, DocNode.of(k2.substring(s2 + 1), v2), //
                                    k3b, DocNode.of(k3.substring(s3 + 1), v3), //
                                    k4, v4));
                        }
                    }
                } else {
                    // s3 == -1

                    if (s4 != -1) {
                        String k2b = k2.substring(0, s2);
                        String k4b = k4.substring(0, s4);

                        if (k2b.equals(k4b)) {
                            // k2b == k4b 
                            return new PlainJavaObjectAdapter(OrderedImmutableMap.of(//
                                    k1, v1, //
                                    k2b, DocNode.of(k2.substring(s2 + 1), v2, k4.substring(s4 + 1), v4), //
                                    k3, v3));
                        } else {
                            // k2b != k4b
                            return new PlainJavaObjectAdapter(OrderedImmutableMap.of(//
                                    k1, v1, //
                                    k2b, DocNode.of(k2.substring(s2 + 1), v2), //
                                    k3, v3, //
                                    k4b, DocNode.of(k4.substring(s4 + 1), v4)));
                        }
                    } else {
                        return new PlainJavaObjectAdapter(OrderedImmutableMap.of(//
                                k1, v1, //
                                k2.substring(0, s2), DocNode.of(k2.substring(s2 + 1), v2), //
                                k3, v3, //
                                k4, v4));
                    }
                }
            } else {
                // s2 == -1
                if (s3 != -1) {
                    if (s4 != -1) {
                        String k3b = k3.substring(0, s3);
                        String k4b = k4.substring(0, s4);

                        if (k3b.equals(k4b)) {
                            // k3b == k4b 
                            return new PlainJavaObjectAdapter(
                                    OrderedImmutableMap.of(k1, v1, k2, v2, k3b, DocNode.of(k3.substring(s3 + 1), v3, k4.substring(s4 + 1), v4)));
                        } else {
                            // k3b != k4b
                            return new PlainJavaObjectAdapter(OrderedImmutableMap.of(k1, v1, k2, v2, k3b, DocNode.of(k3.substring(s3 + 1), v3), k4b,
                                    DocNode.of(k4.substring(s4 + 1), v4)));
                        }
                    } else {
                        return new PlainJavaObjectAdapter(
                                OrderedImmutableMap.of(k1, v1, k2, v2, k3.substring(0, s3), DocNode.of(k3.substring(s3 + 1), v3), k4, v4));
                    }
                } else {
                    // s1 == -1 && s2 == -1 && s3 == -1
                    if (s4 != -1) {
                        return new PlainJavaObjectAdapter(
                                OrderedImmutableMap.of(k1, v1, k2, v2, k3, v3, k4.substring(0, s4), DocNode.of(k4.substring(s4 + 1), v4)));
                    } else {
                        return new PlainJavaObjectAdapter(OrderedImmutableMap.of(k1, v1, k2, v2, k3, v3, k4, v4));
                    }
                }
            }
        }
    }

    public static DocNode of(String k1, Object v1, String k2, Object v2, String k3, Object v3, String k4, Object v4, String k5, Object v5,
            Object... more) {
        OrderedImmutableMap.Builder<String, Object> builder = new OrderedImmutableMap.Builder<>(5 + (more != null ? more.length : 0));

        add(builder, k1, v1);
        add(builder, k2, v2);
        add(builder, k3, v3);
        add(builder, k4, v4);
        add(builder, k5, v5);

        if (more != null) {
            for (int i = 0; i < more.length; i += 2) {
                add(builder, String.valueOf(more[i]), more[i + 1]);
            }
        }

        return new PlainJavaObjectAdapter(build(builder));
    }

    public static DocNode wrap(Object object) {
        if (object instanceof DocNode) {
            return (DocNode) object;
        } else {
            return new PlainJavaObjectAdapter(object);
        }
    }

    public static DocNode array(Object... array) {
        return new PlainJavaObjectAdapter(ImmutableList.ofArray(array));
    }

    private static void add(OrderedImmutableMap.Builder<String, Object> builder, String key, Object value) {
        int s = key.indexOf('.');

        if (s == -1) {
            builder.put(key, value);
        } else {
            String base = key.substring(0, s);
            Object container = builder.get(base);

            if (container == null) {
                OrderedImmutableMap.Builder<String, Object> subBuilder = new OrderedImmutableMap.Builder<String, Object>();
                builder.put(base, subBuilder);
                add(subBuilder, key.substring(s + 1), value);
            } else if (container instanceof OrderedImmutableMap.Builder) {
                @SuppressWarnings("unchecked")
                OrderedImmutableMap.Builder<String, Object> subBuilder = (OrderedImmutableMap.Builder<String, Object>) container;
                add(subBuilder, key.substring(s + 1), value);
            } else {
                throw new IllegalStateException("Key " + base + " does already exist");
            }
        }
    }

    private static OrderedImmutableMap<String, Object> build(OrderedImmutableMap.Builder<String, Object> builder) {
        return builder.build((value) -> {
            if (value instanceof OrderedImmutableMap.Builder) {
                @SuppressWarnings("unchecked")
                OrderedImmutableMap.Builder<String, Object> subBuilder = (OrderedImmutableMap.Builder<String, Object>) value;
                return build(subBuilder);
            } else {
                return value;
            }
        });
    }

    private static class PlainJavaObjectAdapter extends DocNode {
        private final Object object;

        public PlainJavaObjectAdapter(Object object) {
            this(object, null);
        }

        public PlainJavaObjectAdapter(Object object, String key) {
            if (object instanceof PlainJavaObjectAdapter) {
                log.warn("Got {}; this should be not wrapped. Unwrapping", object, new Exception());
                object = ((PlainJavaObjectAdapter) object).object;
            }

            this.object = object;
            this.key = key;
        }

        @Override
        public Object get(String attribute) {
            if (attribute == null) {
                return this.object;
            } else if (this.object instanceof Map) {
                return toBaseType(((Map<?, ?>) this.object).get(attribute));
            } else {
                return null;
            }
        }

        @Override
        public DocNode getAsNode(String attribute) {
            return DocNode.wrap(get(attribute));
        }

        @Override
        public ImmutableList<DocNode> getAsListOfNodes(String attribute) {
            Object object = null;

            if (attribute == null) {
                object = this.object;
            } else if (this.object instanceof Map) {
                object = ((Map<?, ?>) this.object).get(attribute);
            }

            if (object == null) {
                return ImmutableList.empty();
            }

            if (object instanceof PlainJavaObjectAdapter) {
                object = ((PlainJavaObjectAdapter) object).object;
            }

            if (object instanceof Collection) {
                return toListOfDocumentNode((Collection<?>) object);
            } else {
                return ImmutableList.of(wrap(object));
            }
        }

        @Override
        public <R> ImmutableList<R> getList(String attribute, ValidatingFunction<String, R> conversionFunction, Object expected)
                throws ConfigValidationException {
            Object object;

            if (attribute == null) {
                object = this.object;
            }
            if (this.object instanceof Map) {
                object = ((Map<?, ?>) this.object).get(attribute);
            } else {
                return null;
            }

            if (object == null) {
                return null;
            }

            if (object instanceof PlainJavaObjectAdapter) {
                object = ((PlainJavaObjectAdapter) object).object;
            }

            if (!(object instanceof Collection)) {
                throw new ConfigValidationException(new InvalidAttributeValue(attribute, object, "A list of values"));
            }

            Collection<?> collection = (Collection<?>) object;

            ImmutableList.Builder<R> result = new ImmutableList.Builder<>(collection.size());
            ValidationErrors validationErrors = new ValidationErrors();

            int index = 0;

            for (Object subObject : collection) {
                try {
                    if (subObject == null) {
                        result.add((R) null);
                    } else {
                        result.add(conversionFunction.apply(subObject.toString()));
                    }

                } catch (Exception e) {
                    validationErrors.add(new InvalidAttributeValue(attribute + "." + index, subObject, expected).cause(e));
                }
                index++;
            }

            validationErrors.throwExceptionForPresentErrors();

            return result.build();
        }

        private static Object toBaseType(Object object) {
            if (object == null) {
                return null;
            } else if (object instanceof Number || object instanceof String || object instanceof Boolean) {
                return object;
            } else if (object instanceof Collection) {
                return toListOfBaseType((Collection<?>) object);
            } else if (object instanceof DocNode) {
                return object;
            } else if (object instanceof Map) {
                return (Map<?, ?>) object;
            } else {
                throw new RuntimeException("Unexpected type: " + object);
            }
        }

        private static ImmutableList<Object> toListOfBaseType(Collection<?> object) {
            ImmutableList.Builder<Object> list = new ImmutableList.Builder<>(object.size());

            for (Object subNode : object) {
                list.add(toBaseType(subNode));
            }

            return list.build();
        }

        private static ImmutableList<DocNode> toListOfDocumentNode(Collection<?> object) {
            ImmutableList.Builder<DocNode> list = new ImmutableList.Builder<>(object.size());

            for (Object subNode : object) {
                list.add(DocNode.wrap(subNode));
            }

            return list.build();
        }

        @Override
        public ImmutableMap<String, Object> toMap() {
            if (this.object == null) {
                return null;
            } else if (this.object instanceof Map) {
                return OrderedImmutableMap.of(MapView.stringKeysUnsafe((Map<?, ?>) this.object));
            } else {
                return OrderedImmutableMap.of("_", this.object);
            }
        }

        @Override
        public boolean isMap() {
            return object instanceof Map;
        }

        @Override
        public boolean isList(String attribute) {
            if (attribute != null && this.object instanceof Map) {
                Object object = ((Map<?, ?>) this.object).get(attribute);

                if (object instanceof PlainJavaObjectAdapter) {
                    object = ((PlainJavaObjectAdapter) object).object;
                }

                return object instanceof Collection;
            } else if (attribute == null) {
                return this.object instanceof Collection;
            } else {
                return false;
            }
        }

        @Override
        public boolean isList() {
            return this.object instanceof Collection;
        }

        @Override
        public int size() {
            if (object instanceof Map) {
                return ((Map<?, ?>) object).size();
            } else if (object instanceof Collection) {
                return ((Collection<?>) object).size();
            } else if (object != null) {
                return 1;
            } else {
                return 0;
            }
        }

        @Override
        public boolean isEmpty() {
            return size() == 0;
        }

        @Override
        public boolean containsKey(Object key) {
            if (object instanceof Map) {
                return ((Map<?, ?>) object).containsKey(key);
            } else {
                return false;
            }
        }

        @Override
        public Set<String> keySet() {
            if (object instanceof Map) {
                return ensureSetOfStrings(((Map<?, ?>) object).keySet());
            } else {
                return ImmutableSet.empty();
            }
        }

        @Override
        public Collection<Object> values() {
            if (object instanceof Map) {
                return ImmutableList.of(((Map<?, ?>) object).values());
            } else if (object instanceof Collection) {
                return ImmutableList.of(((Collection<?>) object));
            } else if (object != null) {
                return ImmutableList.of(object);
            } else {
                return ImmutableList.empty();
            }
        }

        @Override
        public Set<Entry<String, Object>> entrySet() {
            if (object instanceof Map) {
                Map<?, ?> map = (Map<?, ?>) object;
                Set<Entry<String, Object>> result = new LinkedHashSet<>(map.size());

                for (Map.Entry<?, ?> entry : map.entrySet()) {
                    result.add(Maps.immutableEntry(String.valueOf(entry.getKey()), entry.getValue()));
                }

                return result;
            } else {
                return ImmutableSet.empty();
            }
        }

        @SuppressWarnings({ "unchecked", "rawtypes" })
        private static Set<String> ensureSetOfStrings(Set<?> set) {
            if (containsOnlyStrings(set)) {
                return (Set<String>) (Set) set;
            }

            Set<String> result = new LinkedHashSet<String>(set.size());

            for (Object object : set) {
                if (object != null) {
                    result.add(object.toString());
                } else {
                    result.add(null);
                }
            }

            return result;
        }

        private static boolean containsOnlyStrings(Set<?> set) {
            for (Object object : set) {
                if (!(object instanceof String)) {
                    return false;
                }
            }

            return true;
        }

        @Override
        public ImmutableList<Object> toList() {
            if (object instanceof Collection) {
                return toListOfBaseType((Collection<?>) object);
            } else if (object != null) {
                return ImmutableList.of(toBaseType(object));
            } else {
                return ImmutableList.empty();
            }
        }

        @Override
        public int hashCode() {
            if (object != null) {
                return object.hashCode();
            } else {
                return 0;
            }
        }

        @Override
        public DocNode splitDottedAttributeNamesToTree() throws UnexpectedDocumentStructureException {
            return DocNode.wrap(splitDottedAttributeNamesToTree(this, null));
        }

        @SuppressWarnings("unchecked")
        private static Object splitDottedAttributeNamesToTree(Object object, Map<Object, Object> mapBuilder)
                throws UnexpectedDocumentStructureException {
            if (object instanceof DocNode) {
                object = ((DocNode) object).toBasicObject();
            }

            if (object instanceof Collection) {
                List<Object> listBuilder = new ArrayList<>(((Collection<?>) object).size());

                for (Object e : ((Collection<?>) object)) {
                    listBuilder.add(splitDottedAttributeNamesToTree(e, null));
                }

                return listBuilder;
            }

            if (!(object instanceof Map)) {
                return object;
            }

            Map<?, ?> map = (Map<?, ?>) object;
            if (map.isEmpty()) {
                return object;
            }

            if (mapBuilder == null) {
                mapBuilder = new LinkedHashMap<Object, Object>(map.size());
            }

            for (Map.Entry<?, ?> entry : map.entrySet()) {
                String key = String.valueOf(entry.getKey());
                Object value = entry.getValue();
                int dot = key.indexOf('.');

                if (dot == -1) {
                    Map<Object, Object> subMapBuilder = null;
                    Object existing = mapBuilder.get(key);

                    if (existing instanceof Map) {
                        subMapBuilder = (Map<Object, Object>) existing;
                    } else if (existing instanceof Map) {
                        subMapBuilder = new LinkedHashMap<Object, Object>((Map<Object, Object>) existing);
                        mapBuilder.put(key, subMapBuilder);
                    }

                    mapBuilder.put(key, splitDottedAttributeNamesToTree(value, subMapBuilder));
                } else {
                    String[] parts = key.split("\\.");
                    Map<Object, Object> currentBuilder = mapBuilder;

                    for (int i = 0; i < parts.length - 1; i++) {
                        Object existing = currentBuilder.get(parts[i]);

                        if (existing == null) {
                            Map<Object, Object> subBuilder = new LinkedHashMap<Object, Object>();
                            currentBuilder.put(parts[i], subBuilder);
                            currentBuilder = subBuilder;
                        } else if (existing instanceof Map) {
                            Map<Object, Object> subMapBuilder = (Map<Object, Object>) existing;
                            currentBuilder.put(parts[i], subMapBuilder);
                            currentBuilder = subMapBuilder;
                        } else {
                            throw new UnexpectedDocumentStructureException(
                                    "Dotted attributes are not well-formed: Part " + parts[i] + " of " + key + " does not fit into document tree.");
                        }
                    }

                    Map<Object, Object> subMapBuilder = null;
                    Object existing = currentBuilder.get(key);

                    if (existing instanceof Map) {
                        subMapBuilder = (Map<Object, Object>) existing;
                    }

                    currentBuilder.put(parts[parts.length - 1], splitDottedAttributeNamesToTree(value, subMapBuilder));
                }
            }

            return mapBuilder;
        }

    }

    protected String key;

    public abstract Object get(String attribute);

    public abstract ImmutableList<DocNode> getAsListOfNodes(String attribute);

    public abstract DocNode getAsNode(String attribute);

    public abstract ImmutableMap<String, Object> toMap();

    public abstract boolean isMap();

    public abstract boolean isList();

    public abstract boolean isList(String attribute);

    public boolean isScalar() {
        return !isMap() && !isList() && !isNull();
    }

    public abstract ImmutableList<Object> toList();

    public abstract DocNode splitDottedAttributeNamesToTree() throws UnexpectedDocumentStructureException;

    public Object toBasicObject() {
        return get(null);
    }

    public Map<String, DocNode> toMapOfNodes() {
        return MapView.mapValues(toMap(), (o) -> DocNode.wrap(o));
    }

    public ImmutableList<DocNode> toListOfNodes() {
        return getAsListOfNodes(null);
    }

    public boolean isNull() {
        return toBasicObject() == null;
    }

    public boolean isString() {
        return toBasicObject() instanceof String;
    }

    public boolean isNumber() {
        return toBasicObject() instanceof Number;
    }

    public boolean hasAny(String... keys) {
        for (String key : keys) {
            if (containsKey(key)) {
                return true;
            }
        }

        return false;
    }

    public DocNode with(Document<?> other) {
        DocNode otherDocNode;

        if (other instanceof DocNode) {
            otherDocNode = (DocNode) other;
        } else {
            otherDocNode = other.toDocNode();
        }

        if (otherDocNode.isEmpty()) {
            return this;
        }

        if (!otherDocNode.isMap() || !this.isMap()) {
            return otherDocNode;
        }

        return new PlainJavaObjectAdapter(this.toMap().with(otherDocNode.toMap()));
    }

    public DocNode with(String key, Object value) {
        return new PlainJavaObjectAdapter(this.toMap().with(key, value));
    }

    public DocNode without(String... attrs) {
        Set<String> attrsSet = new HashSet<>(Arrays.asList(attrs));

        LinkedHashMap<String, Object> newMap = new LinkedHashMap<>(size());

        for (Map.Entry<String, Object> entry : entrySet()) {
            if (!attrsSet.contains(entry.getKey())) {
                newMap.put(entry.getKey(), entry.getValue());
            }
        }

        return wrap(newMap);
    }

    public Object get(String attribute, String... attributePath) {
        DocNode docNode = getAsNode(attribute);

        for (int i = 0; i < attributePath.length - 1 && docNode != null; i++) {
            docNode = docNode.getAsNode(attributePath[i]);
        }

        if (docNode != null) {
            return docNode.get(attributePath[attributePath.length - 1]);
        } else {
            return null;
        }
    }

    public DocNode getAsNode(String attribute, String... moreAttributes) {
        DocNode docNode = getAsNode(attribute);

        for (int i = 0; i < moreAttributes.length && docNode != null && !docNode.isNull(); i++) {
            docNode = docNode.getAsNode(moreAttributes[i]);
        }

        return docNode;
    }

    public ImmutableList<String> toListOfStrings() {
        List<Object> list = toList();

        ImmutableList.Builder<String> result = new ImmutableList.Builder<>(list.size());

        for (Object e : list) {
            result.add(e != null ? e.toString() : null);
        }

        return result.build();
    }

    public String getAsString(String attribute) {
        Object object = this.get(attribute);

        if (object != null) {
            return object.toString();
        } else {
            return null;
        }
    }

    public Number getNumber(String attribute) throws ConfigValidationException {
        Object object = this.get(attribute);

        if (object == null) {
            return null;
        } else if (object instanceof Number) {
            return (Number) object;
        } else {
            throw new ConfigValidationException(new InvalidAttributeValue(attribute, object, "A number value"));
        }
    }

    public BigDecimal getBigDecimal(String attribute) throws ConfigValidationException {
        Object object = this.get(attribute);

        if (object == null) {
            return null;
        } else if (object instanceof BigDecimal) {
            return (BigDecimal) object;
        } else if (object instanceof Double || object instanceof Float) {
            return new BigDecimal(((Number) object).doubleValue());
        } else if (object instanceof Long || object instanceof Integer || object instanceof Short) {
            return new BigDecimal(((Number) object).longValue());
        } else {
            try {
                return new BigDecimal(object.toString());
            } catch (NumberFormatException e) {
                throw new ConfigValidationException(new InvalidAttributeValue(attribute, object, "A number value").cause(e));
            }
        }
    }

    public Boolean getBoolean(String attribute) throws ConfigValidationException {
        Object object = this.get(attribute);

        if (object == null) {
            return null;
        } else if (object instanceof Boolean) {
            return (Boolean) object;
        } else {
            throw new ConfigValidationException(new InvalidAttributeValue(attribute, object, "Must be true or false"));
        }
    }

    public <R> R get(String attribute, ValidatingFunction<String, R> conversionFunction, Object expected) throws ConfigValidationException {

        String value = getAsString(attribute);

        if (value == null) {
            return null;
        } else {
            try {
                return conversionFunction.apply(value);
            } catch (ConfigValidationException e) {
                throw new ConfigValidationException(e.getValidationErrors());
            } catch (Exception e) {
                throw new ConfigValidationException(new InvalidAttributeValue(attribute, value, expected).cause(e));
            }
        }
    }

    public <R> R getFromNode(String attribute, ValidatingFunction<DocNode, R> conversionFunction, Object expected) throws ConfigValidationException {

        DocNode value = getAsNode(attribute);

        if (value == null) {
            return null;
        } else {
            try {
                return conversionFunction.apply(value);
            } catch (ConfigValidationException e) {
                throw new ConfigValidationException(e.getValidationErrors());
            } catch (Exception e) {
                throw new ConfigValidationException(new InvalidAttributeValue(attribute, value, expected).cause(e));
            }
        }
    }

    public <R> ImmutableList<R> getList(String attribute, ValidatingFunction<String, R> conversionFunction, Object expected)
            throws ConfigValidationException {

        List<DocNode> nodeList = getAsListOfNodes(attribute);
        ImmutableList.Builder<R> result = new ImmutableList.Builder<>(nodeList.size());
        ValidationErrors validationErrors = new ValidationErrors();

        int index = 0;

        for (DocNode node : nodeList) {
            try {
                result.add(node.get(null, conversionFunction, expected));
            } catch (ConfigValidationException e) {
                validationErrors.add(String.valueOf(index), e);
            }
            index++;
        }

        validationErrors.throwExceptionForPresentErrors();

        return result.build();
    }

    public <R> ImmutableList<R> getListFromNodes(String attribute, ValidatingFunction<DocNode, R> conversionFunction, Object expected)
            throws ConfigValidationException {

        List<DocNode> nodeList = getAsListOfNodes(attribute);
        ImmutableList.Builder<R> result = new ImmutableList.Builder<>(nodeList.size());
        ValidationErrors validationErrors = new ValidationErrors();

        int index = 0;

        for (DocNode node : nodeList) {
            try {
                result.add(node.getFromNode(null, conversionFunction, expected));
            } catch (ConfigValidationException e) {
                validationErrors.add(String.valueOf(index), e);
            }
            index++;
        }

        validationErrors.throwExceptionForPresentErrors();

        return result.build();
    }

    public <R> ImmutableList<R> getAsList(String attribute, ValidatingFunction<String, R> conversionFunction, Object expected)
            throws ConfigValidationException {
        if (!hasNonNull(attribute)) {
            return ImmutableList.empty();
        } else if (isList(attribute)) {
            return getList(attribute, conversionFunction, expected);
        } else {
            return ImmutableList.of(get(attribute, conversionFunction, expected));
        }
    }

    public <R> ImmutableList<R> getAsListFromNodes(String attribute, ValidatingFunction<DocNode, R> conversionFunction)
            throws ConfigValidationException {
        return getAsListFromNodes(attribute, conversionFunction, null);
    }

    public <R> ImmutableList<R> getAsListFromNodes(String attribute, ValidatingFunction<DocNode, R> conversionFunction, Object expected)
            throws ConfigValidationException {
        if (!hasNonNull(attribute)) {
            return ImmutableList.empty();
        } else if (isList(attribute)) {
            return getListFromNodes(attribute, conversionFunction, expected);
        } else {
            return ImmutableList.of(getFromNode(attribute, conversionFunction, expected));
        }
    }

    public ImmutableList<String> getListOfStrings(String attribute) throws ConfigValidationException {
        return getList(attribute, (s) -> s, "List of strings");
    }

    public ImmutableList<String> getAsListOfStrings(String attribute) {
        try {
            return getAsList(attribute, (s) -> s, "List of strings");
        } catch (ConfigValidationException e) {
            // should not happen
            throw new RuntimeException(e);
        }
    }

    public boolean hasNonNull(String attribute) {
        return get(attribute) != null;
    }

    @Override
    public boolean containsValue(Object value) {
        if (!isMap()) {
            return false;
        }

        return values().contains(value);
    }

    @Override
    public Object get(Object key) {
        if (key instanceof String) {
            return get((String) key);
        } else {
            return null;
        }
    }

    public String getKey() {
        return this.key;
    }

    public List<?> findByJsonPath(String jsonPath) {
        return JsonPath.using(BasicJsonPathDefaultConfiguration.listDefaultConfiguration()).parse(this).read(jsonPath);
    }

    public ImmutableList<DocNode> findNodesByJsonPath(String jsonPath) {
        Object object = JsonPath.using(BasicJsonPathDefaultConfiguration.listDefaultConfiguration()).parse(this).read(jsonPath);

        if (object instanceof List) {
            ImmutableList.Builder<DocNode> result = new ImmutableList.Builder<>(((List<?>) object).size());

            for (Object subObject : (List<?>) object) {
                result.add(DocNode.wrap(subObject));
            }

            return result.build();
        } else if (object != null) {
            return ImmutableList.of(DocNode.wrap(object));
        } else {
            return ImmutableList.empty();
        }
    }

    public <T> T findSingleValueByJsonPath(String jsonPath, Class<T> type) {
        return JsonPath.using(BasicJsonPathDefaultConfiguration.defaultConfiguration()).parse(this).read(jsonPath, type);
    }

    public DocNode findSingleNodeByJsonPath(String jsonPath) {
        Object object = JsonPath.using(BasicJsonPathDefaultConfiguration.defaultConfiguration()).parse(this).read(jsonPath);

        if (object != null) {
            return DocNode.wrap(object);
        } else {
            return null;
        }
    }

    @Override
    public Object put(String key, Object value) {
        throw new UnsupportedOperationException("DocumentNode instances cannot be modified");
    }

    @Override
    public Object remove(Object key) {
        throw new UnsupportedOperationException("DocumentNode instances cannot be modified");
    }

    @Override
    public void putAll(Map<? extends String, ? extends Object> m) {
        throw new UnsupportedOperationException("DocumentNode instances cannot be modified");
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException("DocumentNode instances cannot be modified");
    }

    public Map<String, Object> toNormalizedMap() {
        return toMap();
    }

    public Number toNumber() throws ConfigValidationException {
        return getNumber(null);
    }

    @Override
    public String toString(Format format) {
        Object value = toBasicObject();

        if (value instanceof Map && format.equals(Format.JSON)) {
            return DocWriter.json().writeAsString(this.toNormalizedMap());
        } else {
            return DocWriter.format(format).writeAsString(value);
        }
    }

    @Override
    public String toJsonString() {
        return toString(Format.JSON);
    }

    @Override
    public String toYamlString() {
        return toString(Format.YAML);
    }

    @Override
    public String toString() {
        Object value = toBasicObject();

        if (value instanceof String) {
            return (String) value;
        } else if (value instanceof Number || value instanceof Boolean || value instanceof Character) {
            return value.toString();
        } else {
            return toString(60);
        }
    }

    protected String toString(int maxChars) {
        Object value = toBasicObject();

        if (value instanceof Map) {
            Map<?, ?> map = (Map<?, ?>) value;
            StringBuilder result = new StringBuilder("{");
            int count = 0;

            for (Map.Entry<?, ?> entry : map.entrySet()) {
                if (count != 0) {
                    result.append(", ");
                }

                if (result.length() >= maxChars) {
                    result.append(map.size() - count).append(" more ...");
                    break;
                }

                result.append(DocWriter.json().writeAsString(entry.getKey()));
                result.append(": ");
                result.append(DocNode.wrap(entry.getValue()).toString(maxChars / 2));
                count++;
            }

            result.append("}");
            return result.toString();
        } else if (value instanceof Collection) {
            Collection<?> collection = (Collection<?>) value;
            StringBuilder result = new StringBuilder("[");
            int count = 0;

            for (Object element : collection) {
                if (count != 0) {
                    result.append(", ");
                }

                if (result.length() >= maxChars) {
                    result.append(collection.size() - count).append(" more ...");
                    break;
                }

                result.append(DocNode.wrap(element).toString(maxChars / 2));
                count++;
            }

            result.append("]");
            return result.toString();
        } else {
            return DocWriter.json().writeAsString(value);
        }
    }

    public static class DocNodeParserBuilder {
        private final Format format;
        private final JsonFactory jsonFactory;

        private DocNodeParserBuilder(Format format) {
            this.format = format;
            this.jsonFactory = format.getJsonFactory();
        }

        public DocNode from(Reader in) throws DocumentParseException, IOException {
            try (JsonParser parser = jsonFactory.createParser(in)) {
                return wrap(new DocReader(format, parser).read());
            }
        }

        public DocNode from(String string) throws DocumentParseException {
            if (string == null || string.length() == 0) {
                throw new DocumentParseException(new ValidationError(null, "The document is empty").expected(format.getName() + " document"));
            }

            try (JsonParser parser = jsonFactory.createParser(string)) {
                return wrap(new DocReader(format, parser).read());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        public DocNode from(byte[] bytes) throws DocumentParseException {
            if (bytes == null || bytes.length == 0) {
                throw new DocumentParseException(new ValidationError(null, "The document is empty").expected(format.getName() + " document"));
            }

            try {
                return from(new ByteArrayInputStream(bytes));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        public DocNode from(InputStream in) throws DocumentParseException, IOException {
            try (JsonParser parser = jsonFactory.createParser(in)) {
                return wrap(new DocReader(format, parser).read());
            }
        }

        public DocNode from(File file) throws DocumentParseException, FileNotFoundException, IOException {
            return from(new FileInputStream(file));
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof DocNode)) {
            return false;
        }

        DocNode other = (DocNode) obj;

        Object thisObject = this.toBasicObject();
        Object otherObject = other.toBasicObject();

        return Objects.equals(thisObject, otherObject);
    }

    public DocNode flatten() {
        if (isFlat()) {
            return this;
        }
        
        return new DocNodeFlattener(this).flatten();
    }

    public boolean isFlat() {
        if (!isMap()) {
            return true;
        }

        for (Map.Entry<String, Object> entry : this.entrySet()) {
            Object value = entry.getValue();

            if (value instanceof DocNode) {
                DocNode docNode = (DocNode) value;

                if (docNode.isMap() || docNode.isList()) {
                    return false;
                }
            } else if (value instanceof Map) {
                return false;
            } else if (value instanceof Collection) {
                return false;
            }
        }

        return true;
    }
}
