/*
 * Copyright 2021-2022 floragunn GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.floragunn.codova.documents;

import java.util.Optional;

import com.floragunn.codova.config.templates.PipeExpression.PipeFunction;
import com.floragunn.codova.validation.ConfigValidationException;
import com.floragunn.codova.validation.ValidationResult;
import com.floragunn.codova.validation.VariableResolvers;
import com.floragunn.fluent.collections.ImmutableMap;

@FunctionalInterface
public interface Parser<T, C extends Parser.Context> {
    T parse(DocNode docNode, C context) throws ConfigValidationException;

    @FunctionalInterface
    interface ReturningValidationResult<T, C extends Parser.Context> {
        ValidationResult<T> parse(DocNode docNode, C context);
    }
    
    @FunctionalInterface
    interface WithDefault<T, C extends Parser.Context> {
        T parse(DocNode docNode, C context, Optional<T> defaultValue) throws ConfigValidationException;
    }
    
    
    interface Context {
        VariableResolvers variableResolvers();

        /**
         * If true, non-essential validation rules are not enforced. Non-essential validation rules are min/max values for numeric types and similar checks. 
         * However, any complete parsing failures should be still reported as validation errors. Default: false, i.e., full validation is performed.
         */
        default boolean isLenientValidationRequested() {
            return false;
        }

        /**
         * If true, external resources can be created by the parsing process. External resources need additional resource management, like explicit closing.
         * Examples for external resources are network connections.
         */
        default boolean isExternalResourceCreationEnabled() {
            return false;
        }

        default ImmutableMap<String, PipeFunction> pipeFunctions() {
            return PipeFunction.all();
        }
        
        static Parser.Context.Basic get() {
            return DEFAULT;
        }
        
        static Parser.Context.Basic lenient() {
            return new Basic(null, true, false, ImmutableMap.empty());
        }
        
        static Parser.Context.Basic withExternalResources() {
            return new Basic(null, false, true, ImmutableMap.empty());
        }
        
        static Parser.Context.Basic variableResolvers(VariableResolvers variableResolvers) {
            return new Basic(variableResolvers, false, false, ImmutableMap.empty());
        }
        
        static final Parser.Context.Basic DEFAULT = new Basic(null, false, false, null);
        
        static class Basic implements Parser.Context {

            private final VariableResolvers variableResolvers;
            private final boolean lenientValidationRequested;
            private final boolean externalResourceCreationEnabled;
            private final ImmutableMap<String, PipeFunction> pipeFunctionsMap;

            Basic(VariableResolvers variableResolvers, boolean lenientValidationRequested, boolean externalResourceCreationEnabled,
                ImmutableMap<String, PipeFunction> pipeFunctionsMap) {
                this.variableResolvers = variableResolvers;
                this.lenientValidationRequested = lenientValidationRequested;
                this.externalResourceCreationEnabled = externalResourceCreationEnabled;
                this.pipeFunctionsMap = pipeFunctionsMap == null ? PipeFunction.all() : pipeFunctionsMap;
            }

            @Override
            public VariableResolvers variableResolvers() {
                return variableResolvers;
            }

            public Basic variableResolvers(VariableResolvers variableResolvers) {
                return new Basic(variableResolvers, this.lenientValidationRequested, this.externalResourceCreationEnabled, this.pipeFunctionsMap);
            }

            @Override
            public boolean isLenientValidationRequested() {
                return lenientValidationRequested;
            }

            public Basic lenient() {
                return new Basic(this.variableResolvers, true, this.externalResourceCreationEnabled, this.pipeFunctionsMap);
            }

            @Override
            public boolean isExternalResourceCreationEnabled() {
                return externalResourceCreationEnabled;
            }

            public Basic withExternalResources() {
                return new Basic(this.variableResolvers, this.lenientValidationRequested, true, this.pipeFunctionsMap);
            }

            public Basic withPipeFunctions(ImmutableMap<String, PipeFunction> pipeFunctionsMap) {
                return new Basic(this.variableResolvers, this.lenientValidationRequested, this.externalResourceCreationEnabled, pipeFunctionsMap);
            }

            @Override
            public ImmutableMap<String, PipeFunction> pipeFunctions() {
                return this.pipeFunctionsMap;
            }
        }

    }
}
