/*
 * Copyright 2021-2022 floragunn GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.floragunn.codova.documents;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Deque;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.floragunn.codova.validation.errors.ValidationError;

/**
 * A lightweight, reflection-less way of parsing JSON. Parses JSON to these basic Java types:
 * 
 * - List
 * - Map
 * - String
 * - Boolean
 * - Number
 */
public class DocReader {

    public static DocReaderBuilder format(Format format) {
        return new DocReaderBuilder(format, format.getJsonFactory());
    }

    public static DocReaderBuilder json() {
        return format(Format.JSON);
    }

    public static DocReaderBuilder yaml() {
        return format(Format.YAML);
    }

    public static DocReaderBuilder smile() {
        return format(Format.SMILE);
    }

    private final JsonParser parser;
    private final Format format;
    private final int handleDottedAttributeNamesAsPathsFromDepth;
    private final Object emptyDocumentFallback;

    private Deque<Object> nodeStack = new ArrayDeque<>();
    private Object currentNode;
    private Object topNode;
    private String currentAttributeName = null;

    public DocReader(Format format, JsonParser parser, int handleDottedAttributeNamesAsPathsFromDepth, Object emptyDocumentFallback) {
        this.format = format;
        this.parser = parser;
        this.handleDottedAttributeNamesAsPathsFromDepth = handleDottedAttributeNamesAsPathsFromDepth;
        this.emptyDocumentFallback = emptyDocumentFallback;
    }

    public DocReader(Format format, JsonParser parser) {
        this.format = format;
        this.parser = parser;
        this.handleDottedAttributeNamesAsPathsFromDepth = -1;
        this.emptyDocumentFallback = null;
    }

    public Object read() throws IOException, DocumentParseException {

        int tokenCount = 0;

        try {
            for (JsonToken token = parser.currentToken() != null ? parser.currentToken() : parser.nextToken(); token != null; token = parser
                    .nextToken()) {

                tokenCount++;
                int depth = nodeStack.size();

                switch (token) {

                case START_OBJECT:
                    if (currentNode != null) {
                        nodeStack.add(currentNode);
                    }

                    currentNode = addNode(new LinkedHashMap<String, Object>(), depth);
                    break;

                case START_ARRAY:
                    if (currentNode != null) {
                        nodeStack.add(currentNode);
                    }

                    currentNode = addNode(new ArrayList<Object>(), depth);
                    break;

                case END_OBJECT:
                case END_ARRAY:
                    if (nodeStack.isEmpty()) {
                        currentNode = null;
                    } else {
                        currentNode = nodeStack.removeLast();
                    }
                    break;

                case FIELD_NAME:
                    currentAttributeName = parser.currentName();
                    break;

                case VALUE_TRUE:
                    addNode(Boolean.TRUE, depth);
                    break;

                case VALUE_FALSE:
                    addNode(Boolean.FALSE, depth);
                    break;

                case VALUE_NULL:
                    addNode(null, depth);
                    break;

                case VALUE_NUMBER_FLOAT:
                case VALUE_NUMBER_INT:
                    addNode(parser.getNumberValue(), depth);
                    break;

                case VALUE_STRING:
                    addNode(parser.getText(), depth);
                    break;

                case VALUE_EMBEDDED_OBJECT:
                    addNode(parser.getEmbeddedObject(), depth);
                    break;

                default:
                    throw new IllegalStateException("Unexpected token: " + token);

                }

                if (nodeStack.isEmpty() && currentNode == null) {
                    break;
                }
            }

            parser.clearCurrentToken();

            if (tokenCount == 0) {
                if (emptyDocumentFallback == null) {
                    throw new DocumentParseException(new ValidationError(null, "The document is empty").expected(format.getName() + " document"));
                } else {
                    return emptyDocumentFallback;
                }
            }

            return topNode;
        } catch (JsonProcessingException e) {
            throw new DocumentParseException(e, format);
        }
    }

    private Object addNode(Object newNode, int depth) throws JsonProcessingException {

        if (topNode == null) {
            topNode = newNode;
        }

        if (currentNode instanceof Collection) {
            @SuppressWarnings("unchecked")
            Collection<Object> collection = (Collection<Object>) currentNode;

            collection.add(newNode);
        } else if (currentNode instanceof Map) {
            if (currentAttributeName == null) {
                throw new JsonParseException(parser, "Missing attribute name");
            }

            @SuppressWarnings("unchecked")
            Map<String, Object> map = (Map<String, Object>) currentNode;

            if (handleDottedAttributeNamesAsPathsFromDepth != -1 && handleDottedAttributeNamesAsPathsFromDepth <= depth
                    && currentAttributeName.indexOf('.') != -1) {
                String[] attributePath = currentAttributeName.split("\\.");

                Map<String, Object> container = getContainer(currentAttributeName, map, attributePath);

                if (container.containsKey(attributePath[attributePath.length - 1])) {
                    throw new JsonParseException(parser,
                            "Badly nested nodes: The sub-key " + attributePath[attributePath.length - 1] + " of " + currentAttributeName
                                    + " is already defined as a non-map object: " + container.get(attributePath[attributePath.length - 1]));
                }

                container.put(attributePath[attributePath.length - 1], newNode);
            } else {
                if (map.containsKey(currentAttributeName)) {
                    throw new JsonParseException(parser, "The attribute '" + currentAttributeName + "' is defined more than once");
                }

                map.put(currentAttributeName, newNode);
            }

        } else if (currentNode != null) {
            throw new JsonParseException(parser, "Node in wrong context");
        }

        currentAttributeName = null;

        return newNode;
    }

    @SuppressWarnings("unchecked")
    private Map<String, Object> getContainer(String originalAttributeName, Map<String, Object> node, String[] attributePath)
            throws JsonParseException {
        for (int i = 0; i < attributePath.length - 1; i++) {
            Object childNode = node.get(attributePath[i]);

            if (childNode == null) {
                Map<String, Object> newChildNode = new LinkedHashMap<>();
                node.put(attributePath[i], newChildNode);
                node = newChildNode;
            } else if (childNode instanceof Map) {
                node = (Map<String, Object>) childNode;
            } else {
                throw new JsonParseException(parser, "Badly nested nodes: The sub-key " + attributePath[i] + " of " + originalAttributeName
                        + " is already defined as a non-map object: " + childNode);
            }
        }

        return node;
    }

    public static class DocReaderBuilder {
        private JsonFactory jsonFactory;
        private Format format;
        private int handleDottedAttributeNamesAsPathsFromDepth = -1;
        private Object emptyDocumentFallback;

        DocReaderBuilder(Format format, JsonFactory jsonFactory) {
            this.format = format;
            this.jsonFactory = jsonFactory;
        }

        public Object read(Reader in) throws DocumentParseException, IOException {
            try (JsonParser parser = jsonFactory.createParser(in)) {
                return new DocReader(format, parser, handleDottedAttributeNamesAsPathsFromDepth, emptyDocumentFallback).read();
            }
        }

        public Object read(String string) throws DocumentParseException {
            try (JsonParser parser = jsonFactory.createParser(string)) {
                return new DocReader(format, parser, handleDottedAttributeNamesAsPathsFromDepth, emptyDocumentFallback).read();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        public Object read(byte[] bytes) throws DocumentParseException {
            try {
                return read(new ByteArrayInputStream(bytes));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        public Object read(File file) throws DocumentParseException, FileNotFoundException, IOException {
            return read(new FileInputStream(file));
        }

        public Object read(InputStream in) throws DocumentParseException, IOException {
            try (JsonParser parser = jsonFactory.createParser(in)) {
                return new DocReader(format, parser, handleDottedAttributeNamesAsPathsFromDepth, emptyDocumentFallback).read();
            }
        }

        public Map<String, Object> readObject(Reader in) throws DocumentParseException, IOException, UnexpectedDocumentStructureException {
            return toJsonObject(read(in));
        }

        public Map<String, Object> readObject(InputStream in) throws DocumentParseException, IOException, UnexpectedDocumentStructureException {
            return toJsonObject(read(in));
        }

        public Map<String, Object> readObject(String string) throws DocumentParseException, UnexpectedDocumentStructureException {
            return toJsonObject(read(string));
        }

        public Map<String, Object> readObject(byte[] bytes) throws DocumentParseException, UnexpectedDocumentStructureException {
            return toJsonObject(read(bytes));
        }

        public Map<String, Object> readObject(File file)
                throws UnexpectedDocumentStructureException, DocumentParseException, FileNotFoundException, IOException {
            return toJsonObject(read(new FileInputStream(file)));
        }

        private static Map<String, Object> toJsonObject(Object parsedDocument) throws UnexpectedDocumentStructureException {
            if (parsedDocument instanceof Map) {
                @SuppressWarnings("unchecked")
                Map<String, Object> result = (Map<String, Object>) parsedDocument;

                return result;
            } else {
                throw new UnexpectedDocumentStructureException(
                        "Expected a JSON object. Got: " + (parsedDocument instanceof List ? "Array" : String.valueOf(parsedDocument)));
            }
        }

        public DocReaderBuilder splitAttributesAtDots() {
            this.handleDottedAttributeNamesAsPathsFromDepth = 0;
            return this;
        }

        public DocReaderBuilder splitAttributesAtDotsStartingAtDepth(int handleDottedAttributeNamesAsPathsFromDepth) {
            this.handleDottedAttributeNamesAsPathsFromDepth = handleDottedAttributeNamesAsPathsFromDepth;
            return this;
        }

        public DocReaderBuilder fallbackForEmptyDocuments(Object emptyDocumentFallback) {
            this.emptyDocumentFallback = emptyDocumentFallback;
            return this;
        }
    }

}
