/*
 * Copyright 2021 floragunn GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.floragunn.codova.documents;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;

public abstract class UnparsedDocument<T> implements Document<T> {

    public static UnparsedDocument<?> from(String source, Format format) {
        return new StringDoc(source, format);
    }
    
    public static UnparsedDocument<?> from(String source, ContentType contentType) {
        return new StringDoc(source, contentType);
    }

    public static UnparsedDocument<?> from(byte[] source, ContentType contentType) {
        return new BytesDoc(source, contentType);
    }

    public static UnparsedDocument<?> from(byte[] source, Format format, Charset charset) {
        return new BytesDoc(source, format, charset);
    }

    public static UnparsedDocument<?> from(byte[] source, Format format) {
        return new BytesDoc(source, format, null);
    }

    public static UnparsedDocument<?> fromJson(String json) {
        return new StringDoc(json, Format.JSON);
    }

    protected final Format format;
    protected final ContentType contentType;

    private UnparsedDocument(Format format, ContentType contentType) {
        this.format = format;
        this.contentType = contentType;
    }
    
    private UnparsedDocument(Format format) {
        this.format = format;
        this.contentType = format.getContentType();
    }
    
    private UnparsedDocument(ContentType contentType) {
        this.format = contentType.getFormat();
        this.contentType = contentType;
    }

    public abstract Map<String, Object> parseAsMap() throws DocumentParseException, UnexpectedDocumentStructureException;

    public abstract DocNode parseAsDocNode() throws DocumentParseException;

    public abstract Object parse() throws DocumentParseException;

    public abstract String getSourceAsString();

    public abstract JsonParser createParser() throws JsonParseException, IOException;

    public Format getFormat() {
        return format;
    }
    
    public ContentType getContentType() {
        return contentType;
    }
    
    public String getMediaType() {
        return contentType.getMediaType();
    }

    @Override
    public Object toBasicObject() {
        return this;
    }

    public static class StringDoc extends UnparsedDocument<Object> {
        private final String source;

        public StringDoc(String source, Format format) {
            super(format);
            this.source = source;
        }
        
        public StringDoc(String source, ContentType format) {
            super(format);
            this.source = source;
        }

        public Map<String, Object> parseAsMap() throws DocumentParseException, UnexpectedDocumentStructureException {
            return DocReader.format(format).readObject(source);
        }

        public DocNode parseAsDocNode() throws DocumentParseException {
            return DocNode.parse(format).from(source);
        }

        public Object parse() throws DocumentParseException {
            return DocReader.format(format).read(source);
        }

        public String getSource() {
            return source;
        }

        public Format getFormat() {
            return format;
        }

        @Override
        public String toString() {
            return format.getMediaType() + ":\n" + source;
        }

        @Override
        public String getSourceAsString() {
            return source;
        }

        @Override
        public JsonParser createParser() throws JsonParseException, IOException {
            return format.getJsonFactory().createParser(source);
        }

        @Override
        public String toString(Format format) {
            if (format.equals(this.format)) {
                return source;
            } else {
                return super.toString(format);                
            }
        }
    }

    public static class BytesDoc extends UnparsedDocument<Object> {
        private final byte[] source;
        private String sourceAsString;
        private final Charset charset;

        BytesDoc(byte[] source, ContentType contentType) {
            super(contentType);
            this.source = source;
            this.charset = contentType.getCharset();
        }
        
        BytesDoc(byte[] source, Format format, Charset charset) {
            super(format);
            this.source = source;
            this.charset = charset;
        }

        public Map<String, Object> parseAsMap() throws DocumentParseException, UnexpectedDocumentStructureException {
            return DocReader.format(format).readObject(source);
        }

        public DocNode parseAsDocNode() throws DocumentParseException {
            return DocNode.parse(format).from(source);
        }

        public Object parse() throws DocumentParseException {
            return DocReader.format(format).read(source);
        }

        public byte[] getSource() {
            return source;
        }

        public Format getFormat() {
            return format;
        }

        @Override
        public String toString() {
            if (format.isBinary()) {
                return format.getMediaType() + ": " + source.length + " bytes";
            } else {
                return format.getMediaType() + ":\n" + getSourceAsString();
            }
        }

        @Override
        public String getSourceAsString() {
            if (sourceAsString == null) {
                sourceAsString = createSourceString();
            }

            return sourceAsString;
        }

        @Override
        public JsonParser createParser() throws JsonParseException, IOException {
            return format.getJsonFactory().createParser(source);
        }

        private String createSourceString() {
            if (format.isBinary()) {
                throw new IllegalStateException("Cannot encode " + format + " as string");
            }

            if (charset != null) {
                return new String(source, charset);
            } else if (checkBom(0xff, 0xfe, 0x0, 0x0)) {
                return new String(source, 4, source.length - 4, Charset.forName("UTF-32LE"));
            } else if (checkBom(0x0, 0x0, 0xff, 0xfe)) {
                return new String(source, 4, source.length - 4, Charset.forName("UTF-32BE"));
            } else if (checkBom(0xef, 0xbb, 0xbf)) {
                return new String(source, 3, source.length - 3, StandardCharsets.UTF_8);
            } else if (checkBom(0xfe, 0xff)) {
                return new String(source, 2, source.length - 2, StandardCharsets.UTF_16BE);
            } else if (checkBom(0xff, 0xfe)) {
                return new String(source, 2, source.length - 2, StandardCharsets.UTF_16LE);
            } else {
                return new String(source, format.getDefaultCharset());
            }
        }

        private boolean checkBom(int b1, int b2) {
            return source.length >= 2 && source[0] == b1 && source[1] == b2;
        }

        private boolean checkBom(int b1, int b2, int b3) {
            return source.length >= 3 && source[0] == b1 && source[1] == b2 && source[2] == b3;
        }

        private boolean checkBom(int b1, int b2, int b3, int b4) {
            return source.length >= 4 && source[0] == b1 && source[1] == b2 && source[2] == b3 && source[3] == b4;
        }

        @Override
        public byte[] toBytes(Format format) {
            if (format.equals(this.format)) {
                return source;
            } else {
                return super.toBytes(format);                
            }
        }

    }

}
