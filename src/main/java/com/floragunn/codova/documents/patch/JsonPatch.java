/*
 * Copyright 2022 floragunn GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.floragunn.codova.documents.patch;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

import com.floragunn.codova.documents.DocNode;
import com.floragunn.codova.documents.DocUpdateException;
import com.floragunn.codova.documents.Document;
import com.floragunn.codova.documents.pointer.JsonPointer;
import com.floragunn.codova.documents.pointer.PointerEvaluationException;
import com.floragunn.codova.validation.ConfigValidationException;
import com.floragunn.codova.validation.ValidatingDocNode;
import com.floragunn.codova.validation.ValidationErrors;
import com.floragunn.codova.validation.errors.InvalidAttributeValue;
import com.floragunn.fluent.collections.ImmutableList;
import com.floragunn.fluent.collections.OrderedImmutableMap;

public class JsonPatch implements DocPatch {
    public static final String MEDIA_TYPE = "application/json-patch+json";

    private ImmutableList<Operation> operations;

    public JsonPatch(Operation... operations) {
        this.operations = ImmutableList.ofArray(operations);
    }

    JsonPatch(ImmutableList<Operation> operations) {
        this.operations = operations;
    }

    JsonPatch(DocNode source) throws ConfigValidationException {
        if (source.isList()) {
            ValidationErrors validationErrors = new ValidationErrors();

            int i = 0;

            ArrayList<Operation> operations = new ArrayList<>();

            for (DocNode element : source.toListOfNodes()) {
                try {
                    operations.add(Operation.parse(element));
                } catch (ConfigValidationException e) {
                    validationErrors.add(String.valueOf(i), e);
                }
                i++;
            }
            validationErrors.throwExceptionForPresentErrors();
            this.operations = ImmutableList.of(operations);
        } else {
            throw new ConfigValidationException(new InvalidAttributeValue(null, source, "An array of operations"));
        }
    }

    @Override
    public Object toBasicObject() {
        return operations;
    }

    @Override
    public DocNode apply(DocNode targetDocument) throws DocUpdateException {
        Object document = createMutableCopy(targetDocument.toBasicObject());
        int i = 0;

        for (Operation operation : this.operations) {
            try {
                if (operation instanceof Operation.Replace && ((Operation.Replace) operation).pointer.isRoot()) {
                    document = createMutableCopy(((Operation.Replace) operation).value);
                } else if (!operation.apply(document)) {
                    return targetDocument;
                }
            } catch (PointerEvaluationException e) {
                throw new DocUpdateException("Error while applying operation " + i + ": " + e.getMessage(), e);
            }

            i++;
        }

        return DocNode.wrap(document);
    }

    @Override
    public String getMediaType() {
        return MEDIA_TYPE;
    }

    private static Object createMutableCopy(Object object) {
        if (object instanceof Map) {
            LinkedHashMap<String, Object> result = new LinkedHashMap<>(((Map<?, ?>) object).size());

            for (Map.Entry<?, ?> entry : ((Map<?, ?>) object).entrySet()) {
                result.put(String.valueOf(entry.getKey()), createMutableCopy(entry.getValue()));
            }

            return result;
        } else if (object instanceof Collection) {
            ArrayList<Object> result = new ArrayList<>(((Collection<?>) object).size());

            for (Object element : ((Collection<?>) object)) {
                result.add(createMutableCopy(element));
            }

            return result;
        } else {
            return object;
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((operations == null) ? 0 : operations.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof JsonPatch)) {
            return false;
        }
        JsonPatch other = (JsonPatch) obj;
        if (operations == null) {
            if (other.operations != null) {
                return false;
            }
        } else if (!operations.equals(other.operations)) {
            return false;
        }
        return true;
    }

    public boolean isEmpty() {
        return operations.isEmpty();
    }

    public static JsonPatch fromDiff(DocNode source, DocNode target) {
        if (source == null) {
            source = DocNode.NULL;
        }
        if (target == null) {
            target = DocNode.NULL;
        }

        if (source.isNull() && target.isNull()) {
            return new JsonPatch();
        }

        if (source.isNull()) {
            return new JsonPatch(new Operation.Replace(JsonPointer.ROOT, target.toBasicObject()));
        }

        if (target.isNull()) {
            return new JsonPatch(new Operation.Replace(JsonPointer.ROOT, null));
        }

        if (source.isMap() && target.isMap()) {
            ImmutableList.Builder<Operation> result = new ImmutableList.Builder<Operation>();
            diff(JsonPointer.ROOT, source.toMap(), target.toMap(), result);
            return new JsonPatch(result.build());
        } else if (source.isList() && target.isList()) {
            // TODO improve algorithm
            return new JsonPatch(new Operation.Replace(JsonPointer.ROOT, target.toBasicObject()));
        } else {
            return new JsonPatch(new Operation.Replace(JsonPointer.ROOT, target.toBasicObject()));
        }
    }

    private static void diff(JsonPointer pointer, Map<?, ?> source, Map<?, ?> target, ImmutableList.Builder<Operation> result) {
        for (Map.Entry<?, ?> sourceEntry : source.entrySet()) {
            Object key = sourceEntry.getKey();
            Object sourceValue = sourceEntry.getValue();
            Object targetValue = target.get(key);

            if (sourceValue instanceof DocNode) {
                sourceValue = ((DocNode) sourceValue).toBasicObject();
            }

            if (targetValue instanceof DocNode) {
                targetValue = ((DocNode) targetValue).toBasicObject();
            }

            if (Objects.equals(sourceValue, targetValue)) {
                continue;
            }

            if (targetValue == null) {
                result.add(new Operation.Remove(pointer.with(key.toString())));
            } else if (isScalar(targetValue)) {
                result.add(new Operation.Replace(pointer.with(key.toString()), targetValue));
            } else if (isScalar(sourceValue)) {
                result.add(new Operation.Replace(pointer.with(key.toString()), targetValue));
            } else if (sourceValue instanceof Map && targetValue instanceof Map) {
                if (isAnyElementEqual((Map<?, ?>) sourceValue, (Map<?, ?>) targetValue)) {
                    diff(pointer.with(key.toString()), (Map<?, ?>) sourceValue, (Map<?, ?>) targetValue, result);
                } else {
                    result.add(new Operation.Replace(pointer.with(key.toString()), targetValue));
                }
            } else if (sourceValue instanceof Collection && targetValue instanceof Collection) {
                // TODO improve algorithm
                result.add(new Operation.Replace(pointer.with(key.toString()), targetValue));
            } else {
                result.add(new Operation.Replace(pointer.with(key.toString()), targetValue));
            }
        }

        for (Object key : target.keySet()) {
            if (source.containsKey(key)) {
                continue;
            }

            Object targetValue = target.get(key);
            result.add(new Operation.Add(pointer.with(key.toString()), targetValue));
        }
    }

    private static boolean isScalar(Object o) {
        return !(o instanceof Map || o instanceof Collection);
    }

    private static boolean isAnyElementEqual(Map<?, ?> source, Map<?, ?> target) {
        for (Map.Entry<?, ?> sourceEntry : source.entrySet()) {
            Object key = sourceEntry.getKey();
            Object sourceValue = sourceEntry.getValue();
            Object targetValue = target.get(key);

            if (sourceValue instanceof DocNode) {
                sourceValue = ((DocNode) sourceValue).toBasicObject();
            }

            if (targetValue instanceof DocNode) {
                targetValue = ((DocNode) targetValue).toBasicObject();
            }

            if (sourceValue instanceof Map && targetValue instanceof Map) {
                if (isAnyElementEqual((Map<?, ?>) sourceValue, (Map<?, ?>) targetValue)) {
                    return true;
                }
            } else if (Objects.equals(sourceValue, targetValue)) {
                return true;
            }
        }

        return false;
    }

    public static interface Operation {

        boolean apply(Object document) throws PointerEvaluationException;

        static Operation parse(DocNode docNode) throws ConfigValidationException {
            ValidationErrors validationErrors = new ValidationErrors();
            ValidatingDocNode vNode = new ValidatingDocNode(docNode, validationErrors);

            String operation = vNode.get("op").required().asString();
            validationErrors.throwExceptionForPresentErrors();

            switch (operation.toLowerCase()) {
            case "add":
                return new Add(vNode);
            case "remove":
                return new Remove(vNode);
            case "replace":
                return new Replace(vNode);
            case "move":
                return new Move(vNode);
            case "copy":
                return new Copy(vNode);
            case "test":
                return new Test(vNode);
            default:
                throw new ConfigValidationException(new InvalidAttributeValue("op", operation, "add|remove|replace|move|test"));
            }
        }

        public static class Add implements Operation, Document<Add> {
            private final JsonPointer pointer;
            private final Object value;

            Add(ValidatingDocNode vNode) throws ConfigValidationException {
                this.pointer = vNode.get("path").required().byString(JsonPointer::parse);
                this.value = vNode.get("value").required().asAnything();
                vNode.throwExceptionForPresentErrors();
            }

            Add(JsonPointer pointer, Object value) {
                this.pointer = pointer;
                this.value = value;
            }

            @Override
            public Object toBasicObject() {
                return OrderedImmutableMap.of("op", "add", "path", pointer.toString(), "value", value);
            }

            @Override
            public boolean apply(Object document) throws PointerEvaluationException {
                this.pointer.addAtPointedValue(document, value);
                return true;
            }
        }

        public static class Remove implements Operation, Document<Remove> {
            private final JsonPointer pointer;

            Remove(ValidatingDocNode vNode) throws ConfigValidationException {
                this.pointer = vNode.get("path").required().byString(JsonPointer::parse);
                vNode.throwExceptionForPresentErrors();
            }

            Remove(JsonPointer pointer) {
                this.pointer = pointer;
            }

            @Override
            public Object toBasicObject() {
                return OrderedImmutableMap.of("op", "remove", "path", pointer.toString());
            }

            @Override
            public boolean apply(Object document) throws PointerEvaluationException {
                this.pointer.removePointedValue(document);
                return true;
            }
        }

        public static class Replace implements Operation, Document<Replace> {
            private final JsonPointer pointer;
            private final Object value;

            Replace(ValidatingDocNode vNode) throws ConfigValidationException {
                this.pointer = vNode.get("path").required().byString(JsonPointer::parse);
                this.value = vNode.get("value").required().asAnything();
                vNode.throwExceptionForPresentErrors();
            }

            Replace(JsonPointer pointer, Object value) {
                this.pointer = pointer;
                this.value = value;
            }

            @Override
            public Object toBasicObject() {
                return OrderedImmutableMap.of("op", "replace", "path", pointer.toString(), "value", value);
            }

            @Override
            public boolean apply(Object document) throws PointerEvaluationException {
                this.pointer.setPointedValue(document, value);
                return true;
            }
        }

        public static class Move implements Operation, Document<Move> {
            private final JsonPointer from;
            private final JsonPointer to;

            Move(ValidatingDocNode vNode) throws ConfigValidationException {
                this.from = vNode.get("from").required().byString(JsonPointer::parse);
                this.to = vNode.get("path").required().byString(JsonPointer::parse);
                vNode.throwExceptionForPresentErrors();
            }

            @Override
            public Object toBasicObject() {
                return OrderedImmutableMap.of("op", "move", "from", from.toString(), "path", to.toString());
            }

            @Override
            public boolean apply(Object document) throws PointerEvaluationException {
                Object value = this.from.removePointedValue(document);
                this.to.addAtPointedValue(document, value);
                return true;
            }
        }

        public static class Copy implements Operation, Document<Copy> {
            private final JsonPointer from;
            private final JsonPointer to;

            Copy(ValidatingDocNode vNode) throws ConfigValidationException {
                this.from = vNode.get("from").required().byString(JsonPointer::parse);
                this.to = vNode.get("path").required().byString(JsonPointer::parse);
                vNode.throwExceptionForPresentErrors();
            }

            @Override
            public Object toBasicObject() {
                return OrderedImmutableMap.of("op", "copy", "from", from.toString(), "path", to.toString());
            }

            @Override
            public boolean apply(Object document) throws PointerEvaluationException {
                Object value = this.from.getPointedValue(document);
                this.to.addAtPointedValue(document, createMutableCopy(value));
                return true;
            }
        }

        public static class Test implements Operation, Document<Test> {
            private final JsonPointer pointer;
            private final Object value;

            Test(ValidatingDocNode vNode) throws ConfigValidationException {
                this.pointer = vNode.get("path").required().byString(JsonPointer::parse);
                this.value = vNode.get("value").required().asAnything();
                vNode.throwExceptionForPresentErrors();
            }

            @Override
            public Object toBasicObject() {
                return OrderedImmutableMap.of("op", "test", "path", pointer.toString(), "value", value);
            }

            @Override
            public boolean apply(Object document) throws PointerEvaluationException {
                Object currentValue = this.pointer.getPointedValue(document);

                if (Objects.equals(this.value, currentValue)) {
                    return true;
                } else {
                    return false;
                }
            }
        }
    }
}
