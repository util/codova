/*
 * Copyright 2022 floragunn GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.floragunn.codova.documents.pointer;

import java.util.List;
import java.util.Map;

import com.floragunn.codova.validation.ConfigValidationException;
import com.floragunn.codova.validation.errors.ValidationError;
import com.floragunn.fluent.collections.ImmutableList;

public class JsonPointer implements DocPointer {
    public static final JsonPointer ROOT = new JsonPointer(ImmutableList.empty(), "");

    private final ImmutableList<ReferenceToken> tokens;
    private final String source;
    private JsonPointer parent;

    public static JsonPointer parse(String jsonPointer) throws ConfigValidationException {
        if (jsonPointer.length() == 0) {
            return ROOT;
        }

        if (jsonPointer.charAt(0) != '/') {
            throw new ConfigValidationException(new ValidationError(null, "Invalid character at first position"));
        }

        String[] tokenStrings = jsonPointer.substring(1).split("/");
        ImmutableList.Builder<ReferenceToken> tokens = new ImmutableList.Builder<>(tokenStrings.length);

        for (int i = 0; i < tokenStrings.length; i++) {
            tokens.add(new ReferenceToken(tokenStrings[i]));
        }

        return new JsonPointer(tokens.build(), jsonPointer);
    }

    JsonPointer(ImmutableList<ReferenceToken> tokens, String source) {
        this.tokens = tokens;
        this.source = source;

        if (tokens.size() == 1) {
            this.parent = ROOT;
        }
    }

    @Override
    public Object getPointedValue(Object objectTreeNode) throws PointerEvaluationException {
        int tokenSize = this.tokens.size();

        if (tokenSize == 0) {
            return objectTreeNode;
        }

        Object currentNode = objectTreeNode;

        for (int i = 0; i < tokenSize; i++) {
            ReferenceToken token = this.tokens.get(i);

            if (currentNode instanceof List) {
                if (token.index == ReferenceToken.NON_NUMERIC) {
                    throw new PointerEvaluationException(getAncestor(i + 1) + " is not a valid array index");
                } else if (token.index == ReferenceToken.LAST) {
                    throw new PointerEvaluationException(getAncestor(i + 1) + " cannot be used for reading a value from an index");
                } else {
                    List<?> list = (List<?>) currentNode;
                    if (list.size() > token.index) {
                        currentNode = list.get(token.index);
                    } else {
                        throw new PointerEvaluationException(
                                token.index + " out of bounds for array of size " + list.size() + " at " + getAncestor(i));
                    }
                }
            } else if (currentNode instanceof Map) {
                currentNode = ((Map<?, ?>) currentNode).get(token.content);
            } else if (currentNode == null) {
                throw new PointerEvaluationException(getAncestor(i) + " is null. Expected array or object.");
            } else {
                throw new PointerEvaluationException(getAncestor(i) + " points to a scalar value. Expected array or object.");
            }
        }

        return currentNode;
    }

    @Override
    public void setPointedValue(Object objectTreeNode, Object newValue) throws PointerEvaluationException {
        Object containerNode = getParent().getPointedValue(objectTreeNode);
        ReferenceToken token = this.tokens.get(this.tokens.size() - 1);

        if (containerNode instanceof List) {
            @SuppressWarnings("unchecked")
            List<Object> list = (List<Object>) containerNode;

            if (token.index == ReferenceToken.NON_NUMERIC) {
                throw new PointerEvaluationException(token.content + " is not a valid array index");
            } else if (token.index == ReferenceToken.LAST) {
                list.add(newValue);
            } else if (token.index < list.size()) {
                list.set(token.index, newValue);
            }
        } else if (containerNode instanceof Map) {
            @SuppressWarnings("unchecked")
            Map<String, Object> map = (Map<String, Object>) containerNode;
            map.put(token.content, newValue);
        } else if (containerNode == null) {
            throw new PointerEvaluationException(getParent() + " is null. Expected array or object.");
        } else {
            throw new PointerEvaluationException(getParent() + " points to a scalar value. Expected array or object.");
        }
    }

    @Override
    public void addAtPointedValue(Object objectTreeNode, Object newValue) throws PointerEvaluationException {
        Object containerNode = getParent().getPointedValue(objectTreeNode);
        ReferenceToken token = this.tokens.get(this.tokens.size() - 1);

        if (containerNode instanceof List) {
            @SuppressWarnings("unchecked")
            List<Object> list = (List<Object>) containerNode;

            if (token.index == ReferenceToken.NON_NUMERIC) {
                throw new PointerEvaluationException(token.content + " is not a valid array index");
            } else if (token.index == ReferenceToken.LAST) {
                list.add(newValue);
            } else if (token.index < list.size()) {
                list.add(token.index, newValue);
            }
        } else if (containerNode instanceof Map) {
            @SuppressWarnings("unchecked")
            Map<String, Object> map = (Map<String, Object>) containerNode;
            map.put(token.content, newValue);
        } else if (containerNode == null) {
            throw new PointerEvaluationException(getParent() + " is null. Expected array or object.");
        } else {
            throw new PointerEvaluationException(getParent() + " points to a scalar value. Expected array or object.");
        }
    }

    @Override
    public Object removePointedValue(Object objectTreeNode) throws PointerEvaluationException {
        Object containerNode = getParent().getPointedValue(objectTreeNode);

        ReferenceToken token = this.tokens.get(this.tokens.size() - 1);

        if (containerNode instanceof List) {
            @SuppressWarnings("unchecked")
            List<Object> list = (List<Object>) containerNode;

            if (token.index == ReferenceToken.NON_NUMERIC) {
                return null;
            } else if (token.index == ReferenceToken.LAST) {
                return null;
            } else if (token.index < list.size()) {
                return list.remove(token.index);
            } else {
                return null;
            }
        } else if (containerNode instanceof Map) {
            @SuppressWarnings("unchecked")
            Map<String, Object> map = (Map<String, Object>) containerNode;
            return map.remove(token.content);
        } else if (containerNode == null) {
            throw new PointerEvaluationException(getParent() + " is null. Expected array or object.");
        } else {
            throw new PointerEvaluationException(getParent() + " points to a scalar value. Expected array or object.");
        }
    }

    public JsonPointer with(String referenceToken) {
        String escaped = ReferenceToken.escape(referenceToken);
        return new JsonPointer(tokens.with(new ReferenceToken(escaped)), this.source + "/" + escaped);
    }

    public JsonPointer with(int referenceToken) {
        return new JsonPointer(tokens.with(new ReferenceToken(referenceToken)), this.source + "/" + referenceToken);
    }

    public boolean isRoot() {
        return tokens.isEmpty();
    }

    static class ReferenceToken {
        static final int LAST = -1;
        static final int NON_NUMERIC = -2;

        private final String source;
        private final String content;
        private final int index;

        ReferenceToken(String tokenString) {
            this.source = tokenString;
            this.content = unescapeTokenString(tokenString);
            this.index = parseToIndex(tokenString);
        }

        ReferenceToken(int index) {
            this.source = this.content = index == LAST ? "-" : String.valueOf(index);
            this.index = index;
        }

        static String escape(String tokenString) {
            if (!needsEscaping(tokenString)) {
                return tokenString;
            }

            StringBuilder result = new StringBuilder(tokenString.length() + 10);

            for (int i = 0; i < tokenString.length(); i++) {
                char c = tokenString.charAt(i);
                switch (c) {
                case '~':
                    result.append("~0");
                    break;
                case '/':
                    result.append("~1");
                    break;
                default:
                    result.append(c);
                }
            }

            return result.toString();
        }

        static boolean needsEscaping(String tokenString) {
            int length = tokenString.length();

            for (int i = 0; i < length; i++) {
                switch (tokenString.charAt(i)) {
                case '~':
                case '/':
                    return true;
                }
            }

            return false;
        }

        private static String unescapeTokenString(String tokenString) {
            if (tokenString.indexOf('~') == -1) {
                return tokenString;
            } else {
                return tokenString.replace("~1", "/").replace("~0", "~");
            }
        }

        @Override
        public String toString() {
            return source;
        }

        private static int parseToIndex(String tokenString) {
            if (tokenString.equals("-")) {
                return LAST;
            } else if (isNumeric(tokenString)) {
                return Integer.parseInt(tokenString);
            } else {
                return NON_NUMERIC;
            }
        }

        private static boolean isNumeric(String tokenString) {
            int length = tokenString.length();

            if (length == 0) {
                return false;
            }

            for (int i = 0; i < length; i++) {
                char c = tokenString.charAt(i);
                if (!(c >= '0' && c <= '9')) {
                    return false;
                }
            }

            return true;
        }

        @Override
        public int hashCode() {
            return content.hashCode();
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ReferenceToken)) {
                return false;
            }
            ReferenceToken other = (ReferenceToken) obj;

            return this.content.equals(other.content);
        }

    }

    @Override
    public String toString() {
        return source;
    }

    @Override
    public DocPointer getParent() {
        JsonPointer parent = this.parent;

        if (parent == null) {
            if (tokens.size() == 0) {
                return null;
            } else if (tokens.size() == 1) {
                return ROOT;
            } else {
                this.parent = parent = new JsonPointer((ImmutableList<ReferenceToken>) tokens.subList(0, tokens.size() - 1),
                        source.substring(0, this.source.lastIndexOf('/')));
                return parent;
            }
        } else {
            return parent;
        }
    }

    public DocPointer getAncestor(int generation) {
        if (generation >= tokens.size() && generation < 0) {
            throw new IllegalArgumentException("Invalid generation parameter: " + generation);
        }

        if (generation == tokens.size()) {
            return this;
        } else if (generation == tokens.size() - 1) {
            return getParent();
        }

        DocPointer ancestor = getParent();

        for (int i = tokens.size() - 1; i > generation; i--) {
            ancestor = ancestor.getParent();
        }

        return ancestor;
    }

    @Override
    public int hashCode() {
        return tokens.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof JsonPointer)) {
            return false;
        }
        JsonPointer other = (JsonPointer) obj;

        return this.tokens.equals(other.tokens);
    }

}
