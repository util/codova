/*
 * Copyright 2022 floragunn GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.floragunn.codova.documents.pointer;

import com.floragunn.codova.documents.DocNode;

public interface DocPointer {
    default DocNode getPointedValueAsNode(DocNode docNode) throws PointerEvaluationException {
        return DocNode.wrap(getPointedValue(docNode.toBasicObject()));
    }

    default Object getPointedValue(DocNode docNode) throws PointerEvaluationException {
        return getPointedValue(docNode.toBasicObject());
    }

    Object getPointedValue(Object objectTreeNode) throws PointerEvaluationException;

    void setPointedValue(Object objectTreeNode, Object newValue) throws PointerEvaluationException;

    void addAtPointedValue(Object objectTreeNode, Object newValue) throws PointerEvaluationException;

    Object removePointedValue(Object objectTreeNode) throws PointerEvaluationException;

    DocPointer getParent();
}
