/*
 * Copyright 2023 floragunn GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.floragunn.codova.documents;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

class DocNodeFlattener {
    private final DocNode rootDocNode;
    private final Map<String, Object> flattenedMap = new LinkedHashMap<>();

    DocNodeFlattener(DocNode docNode) {
        this.rootDocNode = docNode;
    }

    DocNode flatten() {
        for (Map.Entry<String, Object> entry : rootDocNode.entrySet()) {
            flattenValue(entry.getKey(), entry.getValue(), 0);
        }

        return DocNode.wrap(flattenedMap);
    }

    private void flatten(String flattenedTopPrefix, Map<?, ?> topValue, int depth) {
        if (depth > 200) {
            throw new RuntimeException("Object is too deply nested: " + flattenedTopPrefix);
        }

        for (Map.Entry<?, ?> entry : topValue.entrySet()) {
            String keyString = String.valueOf(entry.getKey());

            if (needsEscaping(keyString)) {
                // Remove the . which as added by flattenValue                
                flattenValue(flattenedTopPrefix.substring(0, flattenedTopPrefix.length() - 1) + "[\"" + keyString + "\"]", entry.getValue(), depth);
            } else {
                flattenValue(flattenedTopPrefix + keyString, entry.getValue(), depth);
            }
        }
    }

    private void flatten(String flattenedTopPrefix, Collection<?> topValue, int depth) {
        if (depth > 200) {
            throw new RuntimeException("Object is too deply nested: " + flattenedTopPrefix);
        }

        int i = 0;

        for (Object entryValue : topValue) {
            flattenValue(flattenedTopPrefix + '[' + i + ']', entryValue, depth);
            i++;
        }
    }

    private void flattenValue(String flattenedKey, Object entryValue, int depth) {
        if (entryValue instanceof DocNode) {
            DocNode docNode = (DocNode) entryValue;

            if (docNode.isMap()) {
                flatten(flattenedKey + '.', docNode, depth + 1);
            } else if (docNode.isList()) {
                flatten(flattenedKey, docNode.toList(), depth + 1);
            } else {
                flattenedMap.put(flattenedKey, entryValue);
            }

        } else if (entryValue instanceof Map) {
            flatten(flattenedKey + '.', (Map<?, ?>) entryValue, depth + 1);
        } else if (entryValue instanceof Collection) {
            flatten(flattenedKey, (Collection<?>) entryValue, depth + 1);
        } else {
            flattenedMap.put(flattenedKey, entryValue);
        }
    }

    private boolean needsEscaping(String key) {
        return key.indexOf('.') != -1 || key.indexOf('[') != -1;
    }

}
