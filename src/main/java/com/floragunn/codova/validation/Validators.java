/*
 * Copyright 2021 floragunn GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.floragunn.codova.validation;

import java.util.function.Predicate;

import jakarta.mail.internet.AddressException;
import jakarta.mail.internet.InternetAddress;
import org.apache.commons.validator.routines.EmailValidator;


public class Validators {
    public static final Predicate<String> EMAIL = (value) -> EmailValidator.getInstance(true, true).isValid(value);
    public static final Predicate<String> EMAIL_WITH_DISPLAY_NAME = (value) -> {
        try {
            new InternetAddress(value, true);
            return true;
        } catch (AddressException e) {
            return false;
        }
    };
}
