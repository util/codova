/*
 * Copyright 2022 floragunn GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.floragunn.codova.documents.patch;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

import com.floragunn.codova.documents.DocNode;

public class JsonPatchTest {
    @Test
    public void add() throws Exception {
        DocNode base = DocNode.of("a.a1", 10, "a.a2", 20);
        DocNode subject = new JsonPatch(DocNode.array(DocNode.of("op", "add", "path", "/a/a3", "value", 30))).apply(base);
        DocNode reference = DocNode.of("a.a1", 10, "a.a2", 20, "a.a3", 30);
        Assert.assertEquals(reference.toDeepBasicObject(), subject.toDeepBasicObject());
    }

    @Test
    public void add_array() throws Exception {
        DocNode base = DocNode.of("a", Arrays.asList(10, 20, 30));
        DocNode subject = new JsonPatch(DocNode.array(DocNode.of("op", "add", "path", "/a/2", "value", 25))).apply(base);
        DocNode reference = DocNode.of("a", Arrays.asList(10, 20, 25, 30));
        Assert.assertEquals(reference.toDeepBasicObject(), subject.toDeepBasicObject());
    }

    @Test
    public void remove() throws Exception {
        DocNode base = DocNode.of("a.a1", 10, "a.a2", 20);
        DocNode subject = new JsonPatch(DocNode.array(DocNode.of("op", "remove", "path", "/a/a2"))).apply(base);
        DocNode reference = DocNode.of("a.a1", 10);
        Assert.assertEquals(reference.toDeepBasicObject(), subject.toDeepBasicObject());
    }

    @Test
    public void remove_array() throws Exception {
        DocNode base = DocNode.of("a", Arrays.asList(10, 20, 30));
        DocNode subject = new JsonPatch(DocNode.array(DocNode.of("op", "remove", "path", "/a/1"))).apply(base);
        DocNode reference = DocNode.of("a", Arrays.asList(10, 30));
        Assert.assertEquals(reference.toDeepBasicObject(), subject.toDeepBasicObject());
    }

    @Test
    public void replace() throws Exception {
        DocNode base = DocNode.of("a.a1", 10, "a.a2", 20);
        DocNode subject = new JsonPatch(DocNode.array(DocNode.of("op", "replace", "path", "/a/a2", "value", 25))).apply(base);
        DocNode reference = DocNode.of("a.a1", 10, "a.a2", 25);
        Assert.assertEquals(reference.toDeepBasicObject(), subject.toDeepBasicObject());
    }

    @Test
    public void replace_array() throws Exception {
        DocNode base = DocNode.of("a", Arrays.asList(10, 20, 30));
        DocNode subject = new JsonPatch(DocNode.array(DocNode.of("op", "replace", "path", "/a/1", "value", 25))).apply(base);
        DocNode reference = DocNode.of("a", Arrays.asList(10, 25, 30));
        Assert.assertEquals(reference.toDeepBasicObject(), subject.toDeepBasicObject());
    }

    @Test
    public void replace_root() throws Exception {
        DocNode base = DocNode.of("a.a1", 10, "a.a2", 20);
        DocNode subject = new JsonPatch(DocNode.array(DocNode.of("op", "replace", "path", "", "value", 25))).apply(base);
        DocNode reference = DocNode.wrap(25);
        Assert.assertEquals(reference.toDeepBasicObject(), subject.toDeepBasicObject());
    }

    @Test
    public void copy() throws Exception {
        DocNode base = DocNode.of("a.a1", 10, "a.a2", 20);
        DocNode subject = new JsonPatch(DocNode.array(DocNode.of("op", "copy", "path", "/a/b", "from", "/a"))).apply(base);
        DocNode reference = DocNode.of("a.a1", 10, "a.a2", 20, "a.b.a1", 10, "a.b.a2", 20);
        Assert.assertEquals(reference.toDeepBasicObject(), subject.toDeepBasicObject());
    }

    @Test
    public void move() throws Exception {
        DocNode base = DocNode.of("a.a1", 10, "a.a2", 20);
        DocNode subject = new JsonPatch(DocNode.array(DocNode.of("op", "move", "path", "/a/a3", "from", "/a/a2"))).apply(base);
        DocNode reference = DocNode.of("a.a1", 10, "a.a3", 20);
        Assert.assertEquals(reference.toDeepBasicObject(), subject.toDeepBasicObject());
    }

    @Test
    public void test_success() throws Exception {
        DocNode base = DocNode.of("a.a1", 10, "a.a2", 20);
        DocNode subject = new JsonPatch(
                DocNode.array(DocNode.of("op", "test", "path", "/a/a2", "value", 20), DocNode.of("op", "add", "path", "/a/a3", "value", 30)))
                        .apply(base);
        DocNode reference = DocNode.of("a.a1", 10, "a.a2", 20, "a.a3", 30);
        Assert.assertEquals(reference.toDeepBasicObject(), subject.toDeepBasicObject());
    }

    @Test
    public void test_fail() throws Exception {
        DocNode base = DocNode.of("a.a1", 10, "a.a2", 20);
        DocNode subject = new JsonPatch(
                DocNode.array(DocNode.of("op", "test", "path", "/a/a2", "value", 21), DocNode.of("op", "add", "path", "/a/a3", "value", 30)))
                        .apply(base);
        Assert.assertEquals(base.toDeepBasicObject(), subject.toDeepBasicObject());
    }

    @Test
    public void test_fail_atomic() throws Exception {
        DocNode base = DocNode.of("a.a1", 10, "a.a2", 20);
        DocNode subject = new JsonPatch(DocNode.array(DocNode.of("op", "add", "path", "/a/a5", "value", 50),
                DocNode.of("op", "test", "path", "/a/a2", "value", 21), DocNode.of("op", "add", "path", "/a/a3", "value", 30))).apply(base);
        Assert.assertEquals(base.toDeepBasicObject(), subject.toDeepBasicObject());
    }

    @Test
    public void diff_equal_simple() throws Exception {
        DocNode d1 = DocNode.of("a", 1);
        DocNode d2 = DocNode.of("a", 1);

        JsonPatch diff = JsonPatch.fromDiff(d1, d2);

        Assert.assertTrue(diff.toJsonString(), diff.isEmpty());
    }

    @Test
    public void diff_equal_moreComplex() throws Exception {
        DocNode d1 = DocNode.of("a", 1, "b", Arrays.asList(1, 2, 3), "c.c1", "C1", "c.c2", "C2");
        DocNode d2 = DocNode.of("a", 1, "c.c1", "C1", "c.c2", "C2", "b", Arrays.asList(1, 2, 3));

        JsonPatch diff = JsonPatch.fromDiff(d1, d2);

        Assert.assertTrue(diff.toJsonString(), diff.isEmpty());
    }

    @Test
    public void diff_replace_simple() throws Exception {
        DocNode d1 = DocNode.of("a", 1);
        DocNode d2 = DocNode.of("a", 2);

        JsonPatch diff = JsonPatch.fromDiff(d1, d2);

        Assert.assertFalse(diff.toJsonString(), diff.isEmpty());
        Assert.assertEquals(d2.toBasicObject(), diff.apply(d1).toBasicObject());
    }

    @Test
    public void diff_replace_moreComplex() throws Exception {
        DocNode d1 = DocNode.of("a", 1, "b", Arrays.asList(1, 2, 3), "c.c1", "C1", "c.c2", "C2");
        DocNode d2 = DocNode.of("a", 1, "c.c1", "C1", "c.c2", "CX", "b", Arrays.asList(1, 2, 99, 3));

        JsonPatch diff = JsonPatch.fromDiff(d1, d2);

        Assert.assertFalse(diff.toJsonString(), diff.isEmpty());
        Assert.assertEquals(d2.toDeepBasicObject(), diff.apply(d1).toDeepBasicObject());
    }

    @Test
    public void diff_replace_depth_3() throws Exception {
        DocNode d1 = DocNode.of("a", 1, "b", 2, "c.c.c1", "C1", "c.c.c2", "C2");
        DocNode d2 = DocNode.of("a", 1, "c.c.c1", "C1", "c.c.c2", "CX", "b", 2);

        JsonPatch diff = JsonPatch.fromDiff(d1, d2);

        System.out.println(diff.toJsonString());
        Assert.assertFalse(diff.toJsonString(), diff.isEmpty());
        Assert.assertEquals(d2.toDeepBasicObject(), diff.apply(d1).toDeepBasicObject());
    }

    @Test
    public void diff_delete_simple() throws Exception {
        DocNode d1 = DocNode.of("a", 1);
        DocNode d2 = DocNode.EMPTY;

        JsonPatch diff = JsonPatch.fromDiff(d1, d2);

        Assert.assertFalse(diff.toJsonString(), diff.isEmpty());
        Assert.assertEquals(d2.toBasicObject(), diff.apply(d1).toBasicObject());
    }

    @Test
    public void diff_delete_moreComplex() throws Exception {
        DocNode d1 = DocNode.of("a", 1, "b", Arrays.asList(1, 2, 3), "c.c1", "C1", "c.c2", "C2");
        DocNode d2 = DocNode.of("a", 1, "c.c1", "C1", "b", Arrays.asList(1, 3));

        JsonPatch diff = JsonPatch.fromDiff(d1, d2);

        Assert.assertFalse(diff.toJsonString(), diff.isEmpty());
        Assert.assertEquals(d2.toDeepBasicObject(), diff.apply(d1).toDeepBasicObject());
    }

    @Test
    public void diff_add_simple() throws Exception {
        DocNode d1 = DocNode.of("a", 1);
        DocNode d2 = DocNode.of("a", 1, "b", 2);

        JsonPatch diff = JsonPatch.fromDiff(d1, d2);

        Assert.assertFalse(diff.toJsonString(), diff.isEmpty());
        Assert.assertEquals(d2.toBasicObject(), diff.apply(d1).toBasicObject());
    }

    @Test
    public void diff_add_moreComplex() throws Exception {
        DocNode d1 = DocNode.of("a", 1);
        DocNode d2 = DocNode.of("a", 1, "c.c1", "C1", "c.c2", "C2");

        JsonPatch diff = JsonPatch.fromDiff(d1, d2);

        Assert.assertFalse(diff.toJsonString(), diff.isEmpty());
        Assert.assertEquals(d2.toDeepBasicObject(), diff.apply(d1).toDeepBasicObject());
    }
}
