/*
 * Copyright 2021 floragunn GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.floragunn.codova.documents;

import static org.junit.Assert.assertEquals;

import java.util.AbstractMap;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

import com.floragunn.codova.validation.ConfigValidationException;
import com.floragunn.fluent.collections.ImmutableMap;
import com.floragunn.fluent.collections.OrderedImmutableMap;

public class DocNodeTest {
    @Test
    public void basicAccess() throws Exception {
        String doc = "" //
                + "a: 42\n"//
                + "b: x\n"//
                + "c:\n"//
                + "  - 1\n"//
                + "  - 2\n"//
                + "  - 3\n"//
                + "d: [6,7,8]\n"//
                + "e:\n"//
                + "  ea: 43\n"//
                + "  eb: y\n"//
                + "  ec:\n"//
                + "    - 10\n"//
                + "    - 20\n"//
                + "    - 30\n"//
                + "  ed: [60,70,80]\n"//
                + "  ee:\n"//
                + "    eea: 44\n"//
                + "    eeb: z\n"//
                + "    eec:\n"//
                + "      - 100\n"//
                + "      - 200\n"//
                + "      - 300\n"//
                + "e.ff.fff: 99";

        DocNode docNode = DocNode.parse(Format.YAML).from(doc);

        Assert.assertNull(docNode.get("x"));
        Assert.assertTrue(docNode.getAsNode("x").isNull());
        Assert.assertEquals(0, docNode.getAsNode("x").size());
        Assert.assertEquals(Collections.emptyList(), docNode.getAsNode("x").toList());

        Assert.assertEquals(42, docNode.get("a"));
        Assert.assertEquals(42, docNode.getAsNode("a").toBasicObject());
        Assert.assertFalse(docNode.getAsNode("a").isNull());
        Assert.assertNull(docNode.getAsNode("a").get("b"));
        Assert.assertTrue(docNode.getAsNode("a").getAsNode("b").isNull());

        Assert.assertEquals("x", docNode.get("b"));
        Assert.assertEquals(Arrays.asList("x"), docNode.getAsNode("b").toList());

        Assert.assertEquals(Arrays.asList(1, 2, 3), docNode.get("c"));
        Assert.assertEquals(Arrays.asList(1, 2, 3), docNode.getAsNode("c").toList());
        Assert.assertEquals(Arrays.asList("1", "2", "3"), docNode.getAsNode("c").toListOfStrings());
        Assert.assertEquals(3, docNode.getAsNode("c").size());

        Assert.assertEquals(Arrays.asList(6, 7, 8), docNode.get("d"));
        Assert.assertEquals(3, docNode.getAsNode("d").size());

        Assert.assertEquals(ImmutableMap.of("eea", 44, "eeb", "z", "eec", Arrays.asList(100, 200, 300)), docNode.get("e", "ee"));
        Assert.assertEquals(ImmutableMap.of("eea", 44, "eeb", "z", "eec", Arrays.asList(100, 200, 300)),
                docNode.getAsNode("e", "ee").toBasicObject());

        Assert.assertEquals(44, docNode.get("e", "ee", "eea"));

        Assert.assertEquals(Arrays.asList(100, 200, 300), docNode.getAsNode("e", "ee", "eec").toList());

    }

    @Test
    public void nullTest() throws DocumentParseException {
        String doc = "a: null";
        DocNode docNode = DocNode.parse(Format.YAML).from(doc);

        Assert.assertNull(docNode.get("a"));
        Assert.assertTrue(docNode.getAsNode("a").isNull());
        Assert.assertEquals(0, docNode.getAsListOfNodes("a").size());
        Assert.assertEquals(Collections.emptyList(), docNode.getAsListOfNodes("a"));
        Assert.assertEquals(Collections.emptyList(), docNode.getAsNode("a").toList());

        Assert.assertNull(docNode.get("b"));
        Assert.assertTrue(docNode.getAsNode("b").isNull());
        Assert.assertEquals(0, docNode.getAsListOfNodes("b").size());
        Assert.assertEquals(Collections.emptyList(), docNode.getAsListOfNodes("b"));
        Assert.assertEquals(Collections.emptyList(), docNode.getAsNode("b").toList());

    }

    @Test
    public void without() throws DocumentParseException {
        String doc = "" //
                + "a: 42\n"//
                + "b: x\n"//
                + "c:\n"//
                + "  - 1\n"//
                + "  - 2\n"//
                + "  - 3\n";

        DocNode docNode = DocNode.parse(Format.YAML).from(doc);

        Assert.assertEquals(ImmutableMap.of("a", 42, "c", Arrays.asList(1, 2, 3)), docNode.without("b").toMap());
    }

    @Test
    public void parseEmptyDocument() throws Exception {
        try {
            DocNode docNode = DocNode.parse(Format.JSON).from("");
            Assert.fail(docNode.toString());
        } catch (ConfigValidationException e) {
            Assert.assertEquals("The document is empty", e.getMessage());
        }
    }

    @Test
    public void parseWhitespaceDocument() throws Exception {
        try {
            DocNode docNode = DocNode.parse(Format.JSON).from("   ");
            Assert.fail(docNode.toString());
        } catch (ConfigValidationException e) {
            Assert.assertEquals("The document is empty", e.getMessage());
        }
    }

    @Test
    public void splitDottedAttributeNamesToTree() throws Exception {
        String yaml = "a.b.c: 1\n" //
                + "a.b.d: 2\n" //
                + "a.c: 3\n" //
                + "b.x: 4\n" //                
                + "a.b.e: 5\n" //
                + "a:\n  d: 6";

        DocNode docNode = DocNode.parse(Format.YAML).from(yaml);
        assertEquals(OrderedImmutableMap.<String, Object>of("a.b.c", 1, "a.b.d", 2, "a.c", 3, "b.x", 4, "a.b.e", 5)
                .with(ImmutableMap.of("a", ImmutableMap.of("d", 6))), docNode.toMap());

        DocNode splitAttrsDocNode = docNode.splitDottedAttributeNamesToTree();
        assertEquals(ImmutableMap.of("a", //
                ImmutableMap.of(//
                        "b", ImmutableMap.of("c", 1, "d", 2, "e", 5), //
                        "c", 3, //
                        "d", 6), //
                "b", ImmutableMap.of("x", 4)//
        ), splitAttrsDocNode);

    }

    @Test
    public void isFlat() {
        Assert.assertTrue(DocNode.of("a", 1, "b", 2).isFlat());
        Assert.assertTrue(DocNode.EMPTY.isFlat());
        Assert.assertFalse(DocNode.of("a", 1, "b", DocNode.array(1, 2, 3)).isFlat());
        Assert.assertFalse(DocNode.of("a", 1, "b", DocNode.of("x", 1, "y", 2)).isFlat());
    }

    @Test
    public void of_flat() {
        for (int paramCount = 1; paramCount <= 7; paramCount++) {
            char c = 'a';

            OrderedImmutableMap<String, Object> base = OrderedImmutableMap.empty();

            for (int k = 0; k < paramCount; k++, c++) {
                base = base.with(String.valueOf(c), k);
            }

            Assert.assertEquals(base, createDocNodeWithOf(base).toMap());
        }
    }

    @Test
    public void of1_nested() {
        OrderedImmutableMap<String, Object> base = OrderedImmutableMap.of("a.aa", 1);
        Assert.assertEquals(OrderedImmutableMap.of("a", OrderedImmutableMap.of("aa", 1)), createDocNodeWithOf(base).toMap());
    }

    @Test
    public void of2_nested() {
        Assert.assertEquals(OrderedImmutableMap.of("a", OrderedImmutableMap.of("aa", 1), "b", 2), DocNode.of("a.aa", 1, "b", 2).toDeepBasicObject());
        Assert.assertEquals(OrderedImmutableMap.of("a", OrderedImmutableMap.of("aa", 1, "bb", 2)),
                DocNode.of("a.aa", 1, "a.bb", 2).toDeepBasicObject());
        Assert.assertEquals(OrderedImmutableMap.of("a", 1, "b", OrderedImmutableMap.of("bb", 2)), DocNode.of("a", 1, "b.bb", 2).toDeepBasicObject());
    }

    @Test
    public void ofX_nested() {
        for (int paramCount = 0; paramCount <= 7; paramCount++) {
            OrderedImmutableMap<String, Object> base = OrderedImmutableMap.empty();

            for (int k = 0; k < paramCount; k++) {
                base = base.with(String.valueOf(k), k);
            }

            @SuppressWarnings("unchecked")
            Map.Entry<String, Object>[] baseArray = base.entrySet().toArray(new Map.Entry[base.size()]);

            Assert.assertEquals(base, createDocNodeWithOf(base).toDeepBasicObject());

            for (int k = 1; k <= paramCount; k++) {
                of_nested(k, base, baseArray);
            }
        }
    }

    private void of_nested(int remainingNestedParams, OrderedImmutableMap<String, Object> reference, Map.Entry<String, Object>[] referenceArray) {

        if (remainingNestedParams == 0) {
            OrderedImmutableMap.Builder<String, Object> builder = new OrderedImmutableMap.Builder<>();

            for (Map.Entry<String, Object> entry : referenceArray) {
                builder.with(entry.getKey(), entry.getValue());
            }

            OrderedImmutableMap<String, Object> source = builder.build();

            Assert.assertEquals(reference, createDocNodeWithOf(source).toDeepBasicObject());

        }

        for (int pos = 0; pos < referenceArray.length; pos++) {
            String key = referenceArray[pos].getKey();
            int value = ((Integer) referenceArray[pos].getValue()) + 10;

            if (referenceArray[pos].getKey().contains(".")) {
                continue;
            }

            OrderedImmutableMap<String, Object> subReference = reference.with(key, ImmutableMap.of(key + "x", value));
            Map.Entry<String, Object>[] subArray = referenceArray.clone();
            subArray[pos] = new AbstractMap.SimpleEntry<>(key + "." + key + "x", value);

            of_nested(remainingNestedParams - 1, subReference, subArray);

            for (int pos2 = pos + 1; pos2 < referenceArray.length; pos2++) {
                String key2 = subArray[pos2].getKey();

                if (key2.contains(".")) {
                    continue;
                }

                OrderedImmutableMap<String, Object> subSubReference = subReference.without(key2).with(key,
                        ImmutableMap.of(key + "x", value, key2 + "x", value + 100));
                Map.Entry<String, Object>[] subSubArray = subArray.clone();
                subSubArray[pos2] = new AbstractMap.SimpleEntry<>(key + "." + key2 + "x", value + 100);

                of_nested(remainingNestedParams - 1, subSubReference, subSubArray);

                for (int pos3 = pos + 1; pos3 < referenceArray.length; pos3++) {
                    String key3 = subSubArray[pos3].getKey();

                    if (key3.contains(".")) {
                        continue;
                    }

                    OrderedImmutableMap<String, Object> subSubSubReference = subSubReference.without(key3).with(key,
                            ImmutableMap.of(key + "x", value, key2 + "x", value + 100, key3 + "x", value + 1000));
                    Map.Entry<String, Object>[] subSubSubArray = subSubArray.clone();
                    subSubSubArray[pos3] = new AbstractMap.SimpleEntry<>(key + "." + key3 + "x", value + 1000);

                    of_nested(remainingNestedParams - 1, subSubSubReference, subSubSubArray);
                }
            }
        }
    }

    private static DocNode createDocNodeWithOf(OrderedImmutableMap<String, Object> map) {

        if (map.size() == 0) {
            return DocNode.EMPTY;
        }

        Iterator<Map.Entry<String, Object>> iter = map.entrySet().iterator();

        if (map.size() == 1) {
            Map.Entry<String, Object> e1 = iter.next();
            return DocNode.of(e1.getKey(), e1.getValue());
        } else if (map.size() == 2) {
            Map.Entry<String, Object> e1 = iter.next();
            Map.Entry<String, Object> e2 = iter.next();

            return DocNode.of(e1.getKey(), e1.getValue(), e2.getKey(), e2.getValue());
        } else if (map.size() == 3) {
            Map.Entry<String, Object> e1 = iter.next();
            Map.Entry<String, Object> e2 = iter.next();
            Map.Entry<String, Object> e3 = iter.next();

            return DocNode.of(e1.getKey(), e1.getValue(), e2.getKey(), e2.getValue(), e3.getKey(), e3.getValue());
        } else if (map.size() == 4) {
            Map.Entry<String, Object> e1 = iter.next();
            Map.Entry<String, Object> e2 = iter.next();
            Map.Entry<String, Object> e3 = iter.next();
            Map.Entry<String, Object> e4 = iter.next();

            return DocNode.of(e1.getKey(), e1.getValue(), e2.getKey(), e2.getValue(), e3.getKey(), e3.getValue(), e4.getKey(), e4.getValue());
        } else if (map.size() == 5) {
            Map.Entry<String, Object> e1 = iter.next();
            Map.Entry<String, Object> e2 = iter.next();
            Map.Entry<String, Object> e3 = iter.next();
            Map.Entry<String, Object> e4 = iter.next();
            Map.Entry<String, Object> e5 = iter.next();

            return DocNode.of(e1.getKey(), e1.getValue(), e2.getKey(), e2.getValue(), e3.getKey(), e3.getValue(), e4.getKey(), e4.getValue(),
                    e5.getKey(), e5.getValue());
        } else {
            Map.Entry<String, Object> e1 = iter.next();
            Map.Entry<String, Object> e2 = iter.next();
            Map.Entry<String, Object> e3 = iter.next();
            Map.Entry<String, Object> e4 = iter.next();
            Map.Entry<String, Object> e5 = iter.next();

            Object[] array = new Object[(map.size() - 5) * 2];
            int i = 0;

            while (iter.hasNext()) {
                Map.Entry<String, Object> e = iter.next();
                array[i] = e.getKey();
                array[i + 1] = e.getValue();
                i += 2;
            }

            return DocNode.of(e1.getKey(), e1.getValue(), e2.getKey(), e2.getValue(), e3.getKey(), e3.getValue(), e4.getKey(), e4.getValue(),
                    e5.getKey(), e5.getValue(), array);

        }
    }
}
