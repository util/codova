/*
 * Copyright 2023 floragunn GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.floragunn.codova.documents;

import org.junit.Assert;
import org.junit.Test;

import com.floragunn.fluent.collections.ImmutableMap;

public class DocNodeFlattenerTest {
    @Test
    public void flat() {
        DocNode flat = DocNode.of("a", 1, "b", 2, "c", 3);
        Assert.assertEquals(flat, flat.flatten());
    }

    @Test
    public void simple() {
        DocNode docNode = DocNode.of("a", 1, "b", 2, "c", DocNode.of("x", 8, "y", 9));
        Assert.assertEquals(DocNode.wrap(ImmutableMap.of("a", 1, "b", 2, "c.x", 8, "c.y", 9)), new DocNodeFlattener(docNode).flatten());
    }

    @Test
    public void nested() {
        DocNode docNode = DocNode.of("a", 1, "b", 2, "c", DocNode.of("x", 8, "y", DocNode.of("z", 9)));
        Assert.assertEquals(DocNode.wrap(ImmutableMap.of("a", 1, "b", 2, "c.x", 8, "c.y.z", 9)), new DocNodeFlattener(docNode).flatten());
    }

    @Test
    public void array() {
        DocNode docNode = DocNode.of("a", 1, "b", 2, "c", DocNode.array(8, 9));
        Assert.assertEquals(DocNode.wrap(ImmutableMap.of("a", 1, "b", 2, "c[0]", 8, "c[1]", 9)), new DocNodeFlattener(docNode).flatten());
    }

    @Test
    public void nested_array() {
        DocNode docNode = DocNode.of("a", 1, "b", 2, "c", DocNode.of("x", 8, "y", DocNode.of("z", DocNode.array(10, 11))));
        Assert.assertEquals(DocNode.wrap(ImmutableMap.of("a", 1, "b", 2, "c.x", 8, "c.y.z[0]", 10, "c.y.z[1]", 11)),
                new DocNodeFlattener(docNode).flatten());
    }

    @Test
    public void array_nested() {
        DocNode docNode = DocNode.of("a", 1, "b", 2, "c", DocNode.array(8, DocNode.of("x", 9, "y", 10)));
        Assert.assertEquals(DocNode.wrap(ImmutableMap.of("a", 1, "b", 2, "c[0]", 8, "c[1].x", 9, "c[1].y", 10)),
                new DocNodeFlattener(docNode).flatten());
    }

    @Test
    public void specialChars() {
        DocNode docNode = DocNode.wrap(ImmutableMap.of("a", 1, "b", 2, "c", DocNode.wrap(ImmutableMap.of("x.z", 8, "y.z", 9))));
        Assert.assertEquals(DocNode.wrap(ImmutableMap.of("a", 1, "b", 2, "c[\"x.z\"]", 8, "c[\"y.z\"]", 9)), new DocNodeFlattener(docNode).flatten());
    }

}
