/*
 * Copyright 2022 floragunn GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.floragunn.codova.documents;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

import com.floragunn.codova.documents.pointer.JsonPointer;
import com.floragunn.codova.documents.pointer.PointerEvaluationException;
import com.floragunn.fluent.collections.ImmutableList;
import com.floragunn.fluent.collections.OrderedImmutableMap;

public class JsonPointerTest {
    @Test
    public void getPointedValue_root() throws Exception {
        Object o = DocNode.of("a.a1", 1, "a.a2", 2).toDeepBasicObject();
        Assert.assertEquals(o, JsonPointer.parse("").getPointedValue(o));
    }

    @Test
    public void getPointedValue_topLevel() throws Exception {
        Map<String, Object> o = OrderedImmutableMap.of("a", OrderedImmutableMap.of("a1", 1, "a2", 2), "b", 100);
        Assert.assertEquals(o.get("a"), JsonPointer.parse("/a").getPointedValue(o));
        Assert.assertEquals(o.get("b"), JsonPointer.parse("/b").getPointedValue(o));
    }

    @Test
    public void getPointedValue_undefined() throws Exception {
        Map<String, Object> o = OrderedImmutableMap.of("a", OrderedImmutableMap.of("a1", 1, "a2", 2), "b", 100);
        Assert.assertNull(JsonPointer.parse("/c").getPointedValue(o));
    }

    @Test
    public void getPointedValue_undefined_intermediateElement() throws Exception {
        Map<String, Object> o = OrderedImmutableMap.of("a", OrderedImmutableMap.of("a1", 1, "a2", 2), "b", 100);
        try {
            JsonPointer.parse("/c/x").getPointedValue(o);
            Assert.fail();
        } catch (PointerEvaluationException e) {
            Assert.assertEquals("/c is null. Expected array or object.", e.getMessage());
        }
    }

    @Test
    public void getPointedValue_escape_code0() throws Exception {
        Map<String, Object> o = OrderedImmutableMap.of("a", OrderedImmutableMap.of("~", 1, "a2", 2), "b", 100);
        Assert.assertEquals(o.get("~"), JsonPointer.parse("/~0").getPointedValue(o));
    }

    @Test
    public void getPointedValue_escape_code1() throws Exception {
        Map<String, Object> o = OrderedImmutableMap.of("a", OrderedImmutableMap.of("/", 1, "a2", 2), "b", 100);
        Assert.assertEquals(o.get("/"), JsonPointer.parse("/~1").getPointedValue(o));
    }

    @Test
    public void getPointedValue_nested() throws Exception {
        Map<String, Object> o = OrderedImmutableMap.of("a", OrderedImmutableMap.of("a1", 1, "a2", 2));
        Assert.assertEquals(((Map<?, ?>) o.get("a")).get("a1"), JsonPointer.parse("/a/a1").getPointedValue(o));
    }

    @Test
    public void getPointedValue_numericMapKey() throws Exception {
        Map<String, Object> o = OrderedImmutableMap.of("a", OrderedImmutableMap.of("1", 1, "2", 2));
        Assert.assertEquals(((Map<?, ?>) o.get("a")).get("1"), JsonPointer.parse("/a/1").getPointedValue(o));
    }

    @Test
    public void getPointedValue_array() throws Exception {
        Map<String, Object> o = OrderedImmutableMap.of("a", ImmutableList.of(1, 5, 9, 42));
        Assert.assertEquals(((List<?>) o.get("a")).get(3), JsonPointer.parse("/a/3").getPointedValue(o));
    }

    @Test
    public void getPointedValue_array_root() throws Exception {
        List<Object> o = ImmutableList.of(1, 5, 9, 42);
        Assert.assertEquals(o.get(3), JsonPointer.parse("/3").getPointedValue(o));
    }

    @Test
    public void setPointedValue_topLevel() throws Exception {
        OrderedImmutableMap<String, Object> base = OrderedImmutableMap.of("a", OrderedImmutableMap.of("a1", 1, "a2", 2));
        Map<String, Object> subject = new HashMap<>(base);
        OrderedImmutableMap<String, Object> reference = base.with("a", 5);
        JsonPointer.parse("/a").setPointedValue(subject, 5);

        Assert.assertEquals(reference, subject);
    }

    @Test
    public void setPointedValue_nested() throws Exception {
        Map<String, Object> subject = new LinkedHashMap<>(OrderedImmutableMap.of("a", new LinkedHashMap<>(OrderedImmutableMap.of("a1", 1, "a2", 2))));
        Map<String, Object> reference = OrderedImmutableMap.of("a", OrderedImmutableMap.of("a1", 1, "a2", 5));
        JsonPointer.parse("/a/a2").setPointedValue(subject, 5);

        Assert.assertEquals(reference, subject);
    }

    @Test
    public void addAtPointedValue_nested_array() throws Exception {
        Map<String, Object> subject = new LinkedHashMap<>(OrderedImmutableMap.of("a", new ArrayList<>(Arrays.asList(10, 20, 30))));
        Map<String, Object> reference = OrderedImmutableMap.of("a", new ArrayList<>(Arrays.asList(10, 20, 25, 30)));
        JsonPointer.parse("/a/2").addAtPointedValue(subject, 25);

        Assert.assertEquals(reference, subject);
    }

    @Test
    public void addAtPointedValue_array_end() throws Exception {
        Map<String, Object> subject = new LinkedHashMap<>(OrderedImmutableMap.of("a", new ArrayList<>(Arrays.asList(10, 20, 30))));
        Map<String, Object> reference = OrderedImmutableMap.of("a", new ArrayList<>(Arrays.asList(10, 20, 30, 40)));
        JsonPointer.parse("/a/-").addAtPointedValue(subject, 40);

        Assert.assertEquals(reference, subject);
    }

    @Test
    public void addPointedValue_object_existingProperty() throws Exception {
        Map<String, Object> subject = new LinkedHashMap<>(OrderedImmutableMap.of("a", new LinkedHashMap<>(OrderedImmutableMap.of("a1", 1, "a2", 2))));
        Map<String, Object> reference = OrderedImmutableMap.of("a", OrderedImmutableMap.of("a1", 1, "a2", 5));
        JsonPointer.parse("/a/a2").addAtPointedValue(subject, 5);

        Assert.assertEquals(reference, subject);
    }

    @Test
    public void addPointedValue_object_newProperty() throws Exception {
        Map<String, Object> subject = new LinkedHashMap<>(OrderedImmutableMap.of("a", new LinkedHashMap<>(OrderedImmutableMap.of("a1", 1, "a2", 2))));
        Map<String, Object> reference = OrderedImmutableMap.of("a", OrderedImmutableMap.of("a1", 1, "a2", 2, "a3", 5));
        JsonPointer.parse("/a/a3").addAtPointedValue(subject, 5);

        Assert.assertEquals(reference, subject);
    }

    @Test
    public void addPointedValue_object_missingParent() throws Exception {
        try {
            Map<String, Object> subject = new LinkedHashMap<>(
                    OrderedImmutableMap.of("a", new LinkedHashMap<>(OrderedImmutableMap.of("a1", 1, "a2", 2))));
            JsonPointer.parse("/a/x/y").addAtPointedValue(subject, 5);

            Assert.fail();
        } catch (PointerEvaluationException e) {
            Assert.assertEquals("/a/x is null. Expected array or object.", e.getMessage());
        }
    }

    @Test
    public void removePointedValue_nested_object() throws Exception {
        Map<String, Object> subject = new LinkedHashMap<>(
                OrderedImmutableMap.of("a", new LinkedHashMap<>(OrderedImmutableMap.of("a1", 10, "a2", 20))));
        Map<String, Object> reference = OrderedImmutableMap.of("a", OrderedImmutableMap.of("a1", 10));
        JsonPointer.parse("/a/a2").removePointedValue(subject);

        Assert.assertEquals(reference, subject);
    }

    @Test
    public void removePointedValue_nested_array() throws Exception {
        Map<String, Object> subject = new LinkedHashMap<>(OrderedImmutableMap.of("a", new ArrayList<>(Arrays.asList(10, 20, 30))));
        Map<String, Object> reference = OrderedImmutableMap.of("a", new ArrayList<>(Arrays.asList(10, 30)));
        JsonPointer.parse("/a/1").removePointedValue(subject);

        Assert.assertEquals(reference, subject);
    }

    @Test
    public void getParent() throws Exception {
        JsonPointer pointer = JsonPointer.parse("/a/b/c");
        Assert.assertEquals(JsonPointer.parse("/a/b"), pointer.getParent());
        Assert.assertEquals(JsonPointer.parse("/a"), pointer.getParent().getParent());
        Assert.assertEquals(JsonPointer.parse(""), pointer.getParent().getParent().getParent());
    }

    @Test
    public void getAncestor() throws Exception {
        JsonPointer pointer = JsonPointer.parse("/a/b/c");
        Assert.assertEquals(JsonPointer.parse("/a/b/c"), pointer.getAncestor(3));
        Assert.assertEquals(JsonPointer.parse("/a/b"), pointer.getAncestor(2));
        Assert.assertEquals(JsonPointer.parse("/a"), pointer.getAncestor(1));
        Assert.assertEquals(JsonPointer.ROOT, pointer.getAncestor(0));
    }

}
