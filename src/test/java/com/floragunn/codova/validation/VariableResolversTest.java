package com.floragunn.codova.validation;

import java.io.File;

import org.junit.Assert;
import org.junit.Test;

import com.google.common.base.Charsets;
import com.google.common.collect.ImmutableMap;
import com.google.common.io.Files;

public class VariableResolversTest {

    @Test
    public void file() throws Exception {
        File tempFile = File.createTempFile("VariableResolversTest-", ".tmp");
        tempFile.deleteOnExit();
        Files.asCharSink(tempFile, Charsets.UTF_8).write("Hello\nWorld");

        Assert.assertEquals("Hello\nWorld", VariableResolvers.FILE.apply(tempFile.getAbsolutePath()));
    }

    @Test
    public void jsonFile() throws Exception {
        File tempFile = File.createTempFile("VariableResolversTest-", ".tmp");
        tempFile.deleteOnExit();
        Files.asCharSink(tempFile, Charsets.UTF_8).write("{\"foo\": 42}");

        Assert.assertEquals(ImmutableMap.of("foo", 42), VariableResolvers.JSON_FILE.apply(tempFile.getAbsolutePath()));
    }
}
