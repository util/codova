/*
 * Copyright 2021-2022 floragunn GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.floragunn.codova.validation;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.time.Instant;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.function.Predicate;

import com.floragunn.codova.config.templates.PipeExpression.PipeFunction;
import com.floragunn.codova.validation.errors.ValidationError;
import com.floragunn.fluent.collections.ImmutableMap;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import com.floragunn.codova.config.templates.Template;
import com.floragunn.codova.documents.DocNode;
import com.floragunn.codova.documents.DocumentParseException;
import com.floragunn.codova.documents.Format;
import com.floragunn.codova.documents.Parser;
import com.floragunn.fluent.collections.ImmutableList;
import com.google.common.base.Charsets;
import com.google.common.io.Files;

public class ValidatingDocumentNodeTest {
    @Test
    public void basicTest() {
        ValidationErrors validationErrors = new ValidationErrors();
        ValidatingDocNode vNode = new ValidatingDocNode(ImmutableMap.of("a", "A", "b", 2), validationErrors);

        Assert.assertEquals("A", vNode.get("a").asString());
        Assert.assertEquals("2", vNode.get("b").asString());
        Assert.assertNull(vNode.get("c").asString());
        Assert.assertEquals("X", vNode.get("c").withDefault("X").asString());
        Assert.assertNull(vNode.get("c").required().asString());

        Assert.assertEquals("c", validationErrors.getOnlyValidationError().getAttribute());
        Assert.assertEquals("Required attribute is missing", validationErrors.getOnlyValidationError().getMessage());

        validationErrors = new ValidationErrors();
        vNode = new ValidatingDocNode(ImmutableMap.of("a", "A", "b", 2), validationErrors);

        // Even with default value, accessing a required attribute yields a validation error
        Assert.assertEquals("X", vNode.get("c").required().withDefault("X").asString());
        Assert.assertEquals("c", validationErrors.getOnlyValidationError().getAttribute());
        Assert.assertEquals("Required attribute is missing", validationErrors.getOnlyValidationError().getMessage());
    }

    @Test
    public void baseUrlTest() throws Exception {

        ValidationErrors validationErrors = new ValidationErrors();
        ValidatingDocNode vNode = new ValidatingDocNode(ImmutableMap.of("x", "http://www.example.com"), validationErrors);

        Assert.assertEquals("http://www.example.com/", vNode.get("x").asBaseURL().toString());

        vNode = new ValidatingDocNode(ImmutableMap.of("x", "http://www.example.com/"), validationErrors);
        Assert.assertEquals("http://www.example.com/", vNode.get("x").asBaseURL().toString());

        vNode = new ValidatingDocNode(ImmutableMap.of("x", "http://www.example.com/a"), validationErrors);
        Assert.assertEquals("http://www.example.com/a/", vNode.get("x").asBaseURL().toString());

        vNode = new ValidatingDocNode(ImmutableMap.of("x", "http://www.example.com/a/"), validationErrors);
        Assert.assertEquals("http://www.example.com/a/", vNode.get("x").asBaseURL().toString());

    }

    @Test
    public void bigDecimalTest() {
        ValidationErrors validationErrors = new ValidationErrors();
        ValidatingDocNode vNode = new ValidatingDocNode(ImmutableMap.of("a", "1.234", "b", "x"), validationErrors);

        Assert.assertEquals("1.234", vNode.get("a").asBigDecimal().toString());
        Assert.assertNull(vNode.get("b").asBigDecimal());
        Assert.assertTrue(validationErrors.toDebugString(), validationErrors.toString().contains("Invalid value; expected: number"));
    }

    @Test
    public void listAttributeTest() {
        ValidationErrors validationErrors = new ValidationErrors();
        ValidatingDocNode vNode = new ValidatingDocNode(ImmutableMap.of("a", "1", "b", Arrays.asList("1", "2")), validationErrors);

        Assert.assertEquals(Arrays.asList("1"), vNode.get("a").asListOfStrings());
        Assert.assertEquals(Arrays.asList("1", "2"), vNode.get("b").asListOfStrings());
        Assert.assertNull(vNode.get("c").asListOfStrings());
    }

    @Test
    public void enumTest() {
        ValidationErrors validationErrors = new ValidationErrors();
        ValidatingDocNode vNode = new ValidatingDocNode(ImmutableMap.of("a", "e1", "b", "E2", "c", "ex"), validationErrors);

        Assert.assertEquals(TestEnum.E1, vNode.get("a").asEnum(TestEnum.class));
        Assert.assertEquals(TestEnum.E2, vNode.get("b").asEnum(TestEnum.class));
        Assert.assertNull(vNode.get("c").asEnum(TestEnum.class));

        Assert.assertEquals("c", validationErrors.getOnlyValidationError().getAttribute());
        Assert.assertEquals("E1|E2", validationErrors.getOnlyValidationError().getExpectedAsString());

        Assert.assertNull(vNode.get("d").asEnum(TestEnum.class));
    }

    @Test
    public void asString() {
        ValidationErrors validationErrors = new ValidationErrors();
        ValidatingDocNode vNode = new ValidatingDocNode(ImmutableMap.of("a", "A", "b", 2, "c", true, "d", Arrays.asList(1, 2, 3)), validationErrors);

        Assert.assertEquals("A", vNode.get("a").asString());
        Assert.assertEquals("2", vNode.get("b").asString());
        Assert.assertEquals("true", vNode.get("c").asString());
        Assert.assertNull(vNode.get("d").asString());
        Assert.assertEquals("d", validationErrors.getOnlyValidationError().getAttribute());
        Assert.assertEquals("A scalar string value", validationErrors.getOnlyValidationError().getExpectedAsString());
    }

    @Test
    public void asString_validatedBy() {
        ValidationErrors validationErrors = new ValidationErrors();
        ValidatingDocNode vNode = new ValidatingDocNode(ImmutableMap.of("a", "foo_A", "b", "B"), validationErrors);

        Assert.assertEquals("foo_A", vNode.get("a").validatedBy((Predicate<String>) (s) -> s.startsWith("foo")).asString());
        Assert.assertFalse(validationErrors.toDebugString(), validationErrors.hasErrors());
        Assert.assertEquals("B", vNode.get("b").validatedBy((Predicate<String>) (s) -> s.startsWith("foo")).asString());
        Assert.assertTrue(validationErrors.toDebugString(), validationErrors.hasErrors());
        Assert.assertEquals("Invalid value", validationErrors.getOnlyValidationError().getMessage());

        validationErrors = new ValidationErrors();
        vNode = new ValidatingDocNode(ImmutableMap.of("a", "foo_A", "b", "B"), validationErrors, Parser.Context.get().lenient());

        Assert.assertEquals("B", vNode.get("b").validatedBy((Predicate<String>) (s) -> s.startsWith("foo")).asString());
        Assert.assertFalse(validationErrors.toDebugString(), validationErrors.hasErrors());
    }

    @Test
    public void asListOfURIs() {
        ValidationErrors validationErrors = new ValidationErrors();
        ValidatingDocNode vNode = new ValidatingDocNode(ImmutableMap.of("a", Arrays.asList("https://a", "https://b")), validationErrors);

        Assert.assertEquals(ImmutableList.of(URI.create("https://a"), URI.create("https://b")), vNode.get("a").asList().ofURIs());
        Assert.assertEquals(ImmutableList.empty(), vNode.get("b").asList().withEmptyListAsDefault().ofURIs());
    }

    @Test
    public void usedAttrTest() throws DocumentParseException {
        String doc = "" //
                + "a: 42\n"//
                + "b: x\n"//
                + "d:\n"//
                + "  da: 43\n"//
                + "  db: y\n"//
                + "e:\n"//
                + "  ea: 43\n"//
                + "  eb: y\n"//
                + "  ee:\n"//
                + "    eea: 44\n"//
                + "    eeb: z\n"//
                + "e.ff.fff: 99";

        ValidationErrors validationErrors = new ValidationErrors();
        ValidatingDocNode vNode = new ValidatingDocNode(DocNode.parse(Format.YAML).from(doc), validationErrors);

        vNode.get("a").asString();
        vNode.get("e").asAnything();
        vNode.getAsDocNode("d");

        vNode.checkForUnusedAttributes();

        Assert.assertEquals(validationErrors.toString(), 1, validationErrors.size());
        Assert.assertEquals("Unsupported attribute", validationErrors.getOnlyValidationError().getMessage());
        Assert.assertEquals("b", validationErrors.getOnlyValidationError().getAttribute());
    }

    @Test
    public void ofTemplates() throws Exception {
        ValidationErrors validationErrors = new ValidationErrors();
        ValidatingDocNode vNode = new ValidatingDocNode(ImmutableMap.of("a", Arrays.asList("foo", "bar${x}"), "b", Arrays.asList("foo", "bar${x")),
                validationErrors);

        Assert.assertEquals(ImmutableList.of(Template.string("foo"), Template.string("bar${x}")), vNode.get("a").asList().ofTemplates());
        Assert.assertTrue(validationErrors.toDebugString(), validationErrors.getErrors().isEmpty());

        Assert.assertEquals(ImmutableList.of(Template.string("foo")), vNode.get("b").asList().ofTemplates());
        Assert.assertEquals("Unterminated expression", validationErrors.getErrors().get("b.1").iterator().next().getMessage());
    }

    @Test
    public void ofIntegers() {
        ValidationErrors validationErrors = new ValidationErrors();
        ValidatingDocNode vNode = new ValidatingDocNode(ImmutableMap.of("a", Arrays.asList(3, 4), "b", Arrays.asList(1, "a")), validationErrors);

        Assert.assertEquals(ImmutableList.of(3, 4), vNode.get("a").asList().ofIntegers());
        Assert.assertTrue(validationErrors.toDebugString(), validationErrors.getErrors().isEmpty());

        Assert.assertEquals(ImmutableList.of(1), vNode.get("b").asList().ofIntegers());
        Assert.assertEquals("Invalid value", validationErrors.getErrors().get("b.1").iterator().next().getMessage());
    }

    @Test
    public void ofIntegers_range() {
        ValidationErrors validationErrors = new ValidationErrors();
        ValidatingDocNode vNode = new ValidatingDocNode(ImmutableMap.of("a", Arrays.asList(13, 4), "b", Arrays.asList(1, "a")), validationErrors);

        Assert.assertEquals(ImmutableList.of(13, 4), vNode.get("a").asList().inRange(10, 20).ofIntegers());
        Assert.assertEquals("Invalid value", validationErrors.getErrors().get("a.1").iterator().next().getMessage());

        validationErrors = new ValidationErrors();
        vNode = new ValidatingDocNode(ImmutableMap.of("a", Arrays.asList(13, 4), "b", Arrays.asList(1, "a")), validationErrors,
                Parser.Context.lenient());

        Assert.assertEquals(ImmutableList.of(13, 4), vNode.get("a").asList().inRange(10, 20).ofIntegers());
        Assert.assertTrue(validationErrors.toDebugString(), validationErrors.getErrors().isEmpty());
    }

    @Test
    public void byString() {
        ValidationErrors validationErrors = new ValidationErrors();
        ValidatingDocNode vNode = new ValidatingDocNode(ImmutableMap.of("a", "2007-12-03T10:15:30.00Z", "b", "XY"), validationErrors);
        Assert.assertEquals(Instant.parse("2007-12-03T10:15:30.00Z"), vNode.get("a").byString(Instant::parse));
        Assert.assertTrue(validationErrors.toDebugString(), validationErrors.getErrors().isEmpty());
        Assert.assertNull(vNode.get("b").byString(Instant::parse));
        Assert.assertFalse(validationErrors.toDebugString(), validationErrors.getErrors().isEmpty());
        Assert.assertEquals("Invalid value", validationErrors.getOnlyValidationError().getMessage());
    }

    @Test
    public void resolvedVariables() throws Exception {
        File tempFile = File.createTempFile("VariableResolversTest-", ".tmp");
        tempFile.deleteOnExit();
        Files.asCharSink(tempFile, Charsets.UTF_8).write("Hello\nWorld");
        ValidationErrors validationErrors = new ValidationErrors();
        ValidatingDocNode vNode = new ValidatingDocNode(ImmutableMap.of("a", "#{file:" + tempFile.getAbsolutePath() + "}"),
                validationErrors).expandVariables(VariableResolvers.ALL);

        Assert.assertEquals("Hello\nWorld", vNode.get("a").asString());
    }

    @Test
    public void shouldReportValidationErrorWhenPipeFunctionIsMissing() throws IOException {
        File tempFile = File.createTempFile("VariableResolversTest-", ".tmp");
        tempFile.deleteOnExit();
        Files.asCharSink(tempFile, Charsets.UTF_8).write("Hello\nWorld");
        ValidationErrors validationErrors = new ValidationErrors();
        ValidatingDocNode vNode = new ValidatingDocNode(
                ImmutableMap.of("incorrect_pipe_function", "#{file:" + tempFile.getAbsolutePath() + "|unknownFunction}"),
                validationErrors).expandVariables(VariableResolvers.ALL);

        vNode.get("incorrect_pipe_function").asString();

        Assert.assertEquals(validationErrors.size(), 1);
        Map<String, Collection<ValidationError>> errors = validationErrors.getErrors();
        Assert.assertTrue(errors.containsKey("incorrect_pipe_function"));
        Collection<ValidationError> incorrectPipeFunction = errors.get("incorrect_pipe_function");
        Assert.assertEquals(1, incorrectPipeFunction.size());
    }

    @Test
    public void shouldReportErrorWhenVariableNameStartFromPipeFunctionSeparator() throws IOException, ConfigValidationException {
        File tempFile = File.createTempFile("VariableResolversTest-", ".tmp");
        tempFile.deleteOnExit();
        Files.asCharSink(tempFile, Charsets.UTF_8).write("Hello\nWorld");
        ValidationErrors validationErrors = new ValidationErrors();
        String expression = "#{|file:" + tempFile.getAbsolutePath() + "}";
        ImmutableMap<String, Object> source = ImmutableMap.of("pipe_function", expression);
        ValidatingDocNode vNode = new ValidatingDocNode(source, validationErrors) //
                .expandVariables(VariableResolvers.ALL);

        vNode.get("pipe_function").asString();

        Assert.assertEquals(validationErrors.size(), 1);
    }

    @Test
    public void shouldUsePipeFunction() throws IOException, ConfigValidationException {
        File tempFile = File.createTempFile("VariableResolversTest-", ".tmp");
        tempFile.deleteOnExit();
        Files.asCharSink(tempFile, Charsets.UTF_8).write("Hello\nWorld");
        ValidationErrors validationErrors = new ValidationErrors();
        String expression = "#{file:" + tempFile.getAbsolutePath() + "|toLowerCase}";
        ImmutableMap<String, Object> source = ImmutableMap.of("pipe_function", expression);
        ValidatingDocNode vNode = new ValidatingDocNode(source, validationErrors) //
                .expandVariables(VariableResolvers.ALL);

        String result = vNode.get("pipe_function").asString();

        Assert.assertEquals(validationErrors.size(), 0);
        Assert.assertEquals("hello\nworld", result);
    }

    @Test
    public void shouldUseTwoPipeFunctions() throws IOException, ConfigValidationException {
        File tempFile = File.createTempFile("VariableResolversTest-", ".tmp");
        tempFile.deleteOnExit();
        Files.asCharSink(tempFile, Charsets.UTF_8).write("Hello\nWorld");
        ValidationErrors validationErrors = new ValidationErrors();
        String expression = "#{file:" + tempFile.getAbsolutePath() + "|toLowerCase|base64}";
        ImmutableMap<String, Object> source = ImmutableMap.of("pipe_function", expression);
        ValidatingDocNode vNode = new ValidatingDocNode(source, validationErrors) //
                .expandVariables(VariableResolvers.ALL);

        String result = vNode.get("pipe_function").asString();

        Assert.assertEquals(validationErrors.size(), 0);
        Assert.assertEquals("aGVsbG8Kd29ybGQ=", result);
    }

    @Test
    public void shouldUseThreePipeFunctions() throws IOException {
        File tempFile = File.createTempFile("VariableResolversTest-", ".tmp");
        tempFile.deleteOnExit();
        Files.asCharSink(tempFile, Charsets.UTF_8).write("Hello\nMoto");
        ValidationErrors validationErrors = new ValidationErrors();
        String expression = "#{file:" + tempFile.getAbsolutePath() + "|toRegexFragment|toUpperCase|toJson}";
        ImmutableMap<String, Object> source = ImmutableMap.of("pipe_function", expression);
        ValidatingDocNode vNode = new ValidatingDocNode(source, validationErrors) //
                .expandVariables(VariableResolvers.ALL);

        String result = vNode.get("pipe_function").asString();

        Assert.assertEquals(validationErrors.size(), 0);
        Assert.assertEquals("\"(\\\\QHELLO\\nMOTO\\\\E)\"", result);
    }

    @Test
    public void shouldUseCustomPipeFunction() throws IOException {
        File tempFile = File.createTempFile("VariableResolversTest-", ".tmp");
        tempFile.deleteOnExit();
        Files.asCharSink(tempFile, Charsets.UTF_8).write("Hello\nMoto");
        ValidationErrors validationErrors = new ValidationErrors();
        String expression = "#{file:" + tempFile.getAbsolutePath() + "|prefix}";
        ImmutableMap<String, Object> source = ImmutableMap.of("pipe_function", expression);
        ImmutableMap<String, PipeFunction> pipeFunctionMap = ImmutableMap.of("prefix", v -> "prefix_" + v);
        Parser.Context context = Parser.Context.DEFAULT.withPipeFunctions(pipeFunctionMap);
        ValidatingDocNode vNode = new ValidatingDocNode(source, validationErrors, context) //
            .expandVariables(VariableResolvers.ALL);

        String result = vNode.get("pipe_function").asString();

        Assert.assertEquals(validationErrors.size(), 0);
        Assert.assertEquals("prefix_Hello\nMoto", result);
    }

    @Test
    public void shouldOverrideStandardPipeFunctions() throws IOException, ConfigValidationException {
        File tempFile = File.createTempFile("VariableResolversTest-", ".tmp");
        tempFile.deleteOnExit();
        Files.asCharSink(tempFile, Charsets.UTF_8).write("Hello\nWorld");
        ValidationErrors validationErrors = new ValidationErrors();
        String expression = "#{file:" + tempFile.getAbsolutePath() + "|toLowerCase}";
        ImmutableMap<String, Object> source = ImmutableMap.of("pipe_function", expression);
        ImmutableMap<String, PipeFunction> pipeFunctionMap = ImmutableMap.of("prefix", v -> "prefix_" + v);
        Parser.Context context = Parser.Context.DEFAULT.withPipeFunctions(pipeFunctionMap);
        ValidatingDocNode vNode = new ValidatingDocNode(source, validationErrors, context) //
            .expandVariables(VariableResolvers.ALL);

        vNode.get("pipe_function").asString();

        Assert.assertEquals(validationErrors.size(), 1);
    }

    @Ignore // Unused attributes right now only work well on attributes on the top level of ValidatingDocNode
    @Test
    public void usedAttrTest2() throws DocumentParseException {
        String doc = "" //
                + "a: 42\n"//
                + "b: x\n"//
                + "e:\n"//
                + "  ea: 43\n"//
                + "  eb: y\n"//
                + "  ee:\n"//
                + "    eea: 44\n"//
                + "    eeb: z\n"//
                + "e.ff.fff: 99";

        ValidationErrors validationErrors = new ValidationErrors();
        ValidatingDocNode vNode = new ValidatingDocNode(DocNode.parse(Format.YAML).from(doc), validationErrors);

        vNode.get("a").asString();
        vNode.get("b").asString();
        vNode.get("e", "ff").asString();
        vNode.get("e", "ea").asString();
        vNode.get("e", "ee").asString();

        vNode.checkForUnusedAttributes();

        Assert.assertEquals(1, validationErrors.size());
        Assert.assertEquals("Unsupported attribute", validationErrors.getOnlyValidationError().getMessage());
        Assert.assertEquals("b", validationErrors.getOnlyValidationError().getAttribute());
    }

    public static enum TestEnum {
        E1, E2;
    }
}
