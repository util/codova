/*
 * Copyright 2022 floragunn GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.floragunn.codova.config.text;

import java.util.Arrays;
import java.util.Iterator;

import org.junit.Assert;
import org.junit.Test;

import com.floragunn.codova.config.text.PatternImpl.PrefixPattern;
import com.floragunn.codova.documents.DocNode;
import com.floragunn.codova.documents.Format;
import com.floragunn.codova.validation.ConfigValidationException;
import com.floragunn.fluent.collections.ImmutableList;
import com.floragunn.fluent.collections.ImmutableSet;

public class PatternTest {

    @Test
    public void constant() throws ConfigValidationException {
        Pattern pattern = Pattern.create("abc");
        Assert.assertTrue(pattern.matches("abc"));
        Assert.assertFalse(pattern.matches("abcd"));
        Assert.assertFalse(pattern.matches("x"));
        Assert.assertTrue(pattern.matches(Arrays.asList("x", "abc")));
        Assert.assertFalse(pattern.matches(Arrays.asList("x", "y")));
    }

    @Test
    public void constant_compound() throws ConfigValidationException {
        Pattern pattern = Pattern.create("abc", "xyz");

        Assert.assertTrue(pattern.matches("abc"));
        Assert.assertTrue(pattern.matches("xyz"));
        Assert.assertFalse(pattern.matches("abcd"));
        Assert.assertFalse(pattern.matches("x"));
        Assert.assertTrue(pattern.matches(Arrays.asList("x", "abc")));
        Assert.assertFalse(pattern.matches(Arrays.asList("x", "y")));
        Assert.assertTrue(pattern.matches(Arrays.asList("xyz", "y")));
    }

    @Test
    public void constant_compound_duplicate() throws ConfigValidationException {
        Pattern pattern = Pattern.create("abc", "abc");

        Assert.assertTrue(pattern.matches("abc"));
        Assert.assertFalse(pattern.matches("xyz"));
        Assert.assertFalse(pattern.matches("abcd"));
        Assert.assertFalse(pattern.matches("x"));
    }

    @Test
    public void prefix() throws ConfigValidationException {
        Pattern pattern = Pattern.create("ab*");
        Assert.assertTrue(pattern.matches("abc"));
        Assert.assertTrue(pattern.matches("abcd"));
        Assert.assertFalse(pattern.matches("a"));
        Assert.assertFalse(pattern.matches("x"));
        Assert.assertTrue(pattern.matches(Arrays.asList("x", "abc")));
        Assert.assertFalse(pattern.matches(Arrays.asList("x", "y")));
    }

    @Test
    public void prefix_compound() throws ConfigValidationException {
        Pattern pattern = Pattern.create("ab*", "xy*");
        Assert.assertTrue(pattern.matches("abc"));
        Assert.assertTrue(pattern.matches("abcd"));
        Assert.assertFalse(pattern.matches("a"));
        Assert.assertFalse(pattern.matches("z"));
        Assert.assertFalse(pattern.matches("x"));
        Assert.assertTrue(pattern.matches("xy"));
        Assert.assertTrue(pattern.matches("xyz"));
        Assert.assertTrue(pattern.matches(Arrays.asList("x", "abc")));
        Assert.assertTrue(pattern.matches(Arrays.asList("xyz", "a")));
        Assert.assertFalse(pattern.matches(Arrays.asList("x", "y")));
    }

    @Test
    public void prefix_compound_duplicates() throws ConfigValidationException {
        Pattern pattern = Pattern.create("ab*", "ab*");
        Assert.assertTrue(pattern.matches("abc"));
        Assert.assertTrue(pattern.matches("abcd"));
        Assert.assertFalse(pattern.matches("a"));
    }

    @Test
    public void simple_basic() throws ConfigValidationException {
        Pattern pattern = Pattern.create("ab*yz");
        Assert.assertTrue(pattern.matches("abyz"));
        Assert.assertTrue(pattern.matches("abcxyz"));
        Assert.assertTrue(pattern.matches("abcxyzblablayz"));
        Assert.assertTrue(pattern.matches("abcdxyz"));
        Assert.assertFalse(pattern.matches("a"));
        Assert.assertFalse(pattern.matches("ab"));
        Assert.assertFalse(pattern.matches("abc"));
        Assert.assertFalse(pattern.matches("abcxy"));
        Assert.assertFalse(pattern.matches("x"));
        Assert.assertTrue(pattern.matches(Arrays.asList("x", "abcxyz")));
        Assert.assertFalse(pattern.matches(Arrays.asList("x", "y")));
    }

    @Test
    public void simple_questionmark() throws ConfigValidationException {
        Pattern pattern = Pattern.create("ab?yz");
        Assert.assertFalse(pattern.matches("abyz"));
        Assert.assertTrue(pattern.matches("abcyz"));
        Assert.assertFalse(pattern.matches("abcxyz"));
        Assert.assertFalse(pattern.matches("a"));
        Assert.assertFalse(pattern.matches("ab"));
        Assert.assertFalse(pattern.matches("abc"));
        Assert.assertFalse(pattern.matches("abcxy"));
        Assert.assertFalse(pattern.matches("x"));
        Assert.assertTrue(pattern.matches(Arrays.asList("x", "abxyz")));
        Assert.assertFalse(pattern.matches(Arrays.asList("x", "y")));
    }

    @Test
    public void simple_basic_compound() throws ConfigValidationException {
        Pattern pattern = Pattern.create("ab*yz", "ab*yz", "ab*MN", "ab*PQ", "yz*ab", "*POST", "abc*xxx");
        Assert.assertTrue(pattern.matches("abyz"));
        Assert.assertTrue(pattern.matches("abcyz"));
        Assert.assertTrue(pattern.matches("abcxyz"));
        Assert.assertTrue(pattern.matches("abcdxyz"));
        Assert.assertTrue(pattern.matches("abcxxx"));
        Assert.assertFalse(pattern.matches("abxxx"));
        Assert.assertFalse(pattern.matches("a"));
        Assert.assertFalse(pattern.matches("ab"));
        Assert.assertFalse(pattern.matches("abc"));
        Assert.assertFalse(pattern.matches("abcy"));
        Assert.assertFalse(pattern.matches("x"));
        Assert.assertTrue(pattern.matches("abMN"));
        Assert.assertTrue(pattern.matches("abyMN"));
        Assert.assertFalse(pattern.matches("abM"));
        Assert.assertTrue(pattern.matches("abPQ"));
        Assert.assertTrue(pattern.matches("abyPQ"));
        Assert.assertFalse(pattern.matches("abP"));
        Assert.assertTrue(pattern.matches("1_POST"));
        Assert.assertFalse(pattern.matches("1_POSTX"));
        Assert.assertTrue(pattern.matches(Arrays.asList("x", "abcxyz")));
        Assert.assertFalse(pattern.matches(Arrays.asList("x", "y")));
    }

    @Test
    public void simple_suffix() throws ConfigValidationException {
        Pattern pattern = Pattern.create("*yz");
        Assert.assertTrue(pattern.matches("abyz"));
        Assert.assertTrue(pattern.matches("abcxyz"));
        Assert.assertTrue(pattern.matches("abcdxyz"));
        Assert.assertFalse(pattern.matches("a"));
        Assert.assertFalse(pattern.matches("ab"));
        Assert.assertFalse(pattern.matches("abc"));
        Assert.assertFalse(pattern.matches("abcxy"));
        Assert.assertFalse(pattern.matches("x"));
        Assert.assertTrue(pattern.matches(Arrays.asList("x", "abcxyz")));
        Assert.assertFalse(pattern.matches(Arrays.asList("x", "y")));
    }

    @Test
    public void simple_3part() throws ConfigValidationException {
        Pattern pattern = Pattern.create("ab*mn*yz");
        Assert.assertTrue(pattern.matches("abmnyz"));
        Assert.assertTrue(pattern.matches("abclmnoxyz"));
        Assert.assertTrue(pattern.matches("abclmn123lmnxyz"));
        Assert.assertFalse(pattern.matches("a"));
        Assert.assertFalse(pattern.matches("ab"));
        Assert.assertFalse(pattern.matches("abc"));
        Assert.assertFalse(pattern.matches("abcmnxy"));
        Assert.assertFalse(pattern.matches("x"));
        Assert.assertTrue(pattern.matches(Arrays.asList("x", "abclmnoxyz")));
        Assert.assertFalse(pattern.matches(Arrays.asList("x", "y")));
    }

    @Test
    public void mixed_compound() throws ConfigValidationException {
        Pattern pattern = Pattern.create("ab*", "xy*", "C", "a*xyz");
        Assert.assertTrue(pattern.matches("abc"));
        Assert.assertTrue(pattern.matches("abcd"));
        Assert.assertTrue(pattern.matches("axyz"));
        Assert.assertTrue(pattern.matches("acdxyz"));
        Assert.assertTrue(pattern.matches("C"));
        Assert.assertFalse(pattern.matches("a"));
        Assert.assertFalse(pattern.matches("axxy"));
        Assert.assertFalse(pattern.matches("z"));
        Assert.assertFalse(pattern.matches("x"));
        Assert.assertTrue(pattern.matches("xy"));
        Assert.assertTrue(pattern.matches("xyz"));
        Assert.assertTrue(pattern.matches(Arrays.asList("x", "abc")));
        Assert.assertTrue(pattern.matches(Arrays.asList("xyz", "a")));
        Assert.assertFalse(pattern.matches(Arrays.asList("x", "y")));
    }

    @Test
    public void basic_exclusions() throws ConfigValidationException {
        Pattern pattern = Pattern.create("ab*", "-abcd");
        Assert.assertTrue(pattern.matches("abc"));
        Assert.assertFalse(pattern.matches("abcd"));
        Assert.assertTrue(pattern.matches("abcde"));
    }

    @Test
    public void wildcard_exclusions() throws ConfigValidationException {
        Pattern pattern = Pattern.create("ab*", "-abcd*");
        Assert.assertTrue(pattern.matches("abc"));
        Assert.assertFalse(pattern.matches("abcd"));
        Assert.assertFalse(pattern.matches("abcde"));
    }

    @Test
    public void mixed_compound_with_exclusions() throws ConfigValidationException {
        Pattern pattern = Pattern.create("ab*", "xy*", "-abcd", "C", "a*xyz");
        Assert.assertTrue(pattern.matches("abc"));
        Assert.assertFalse(pattern.matches("abcd"));
        Assert.assertTrue(pattern.matches("abcde"));
        Assert.assertTrue(pattern.matches("axyz"));
        Assert.assertTrue(pattern.matches("acdxyz"));
        Assert.assertTrue(pattern.matches("C"));
        Assert.assertFalse(pattern.matches("a"));
        Assert.assertFalse(pattern.matches("axxy"));
        Assert.assertFalse(pattern.matches("z"));
        Assert.assertFalse(pattern.matches("x"));
        Assert.assertTrue(pattern.matches("xy"));
        Assert.assertTrue(pattern.matches("xyz"));
        Assert.assertTrue(pattern.matches(Arrays.asList("x", "abc")));
        Assert.assertTrue(pattern.matches(Arrays.asList("xyz", "a")));
        Assert.assertFalse(pattern.matches(Arrays.asList("x", "y")));
    }

    @Test
    public void multi_exclusions() throws ConfigValidationException {
        Pattern pattern = Pattern.create("ab*", "-abcd*", "-abx*", "abcdz*", "-abcdzz");
        Assert.assertTrue(pattern.matches("abc"));
        Assert.assertFalse(pattern.matches("abcd"));
        Assert.assertFalse(pattern.matches("abcde"));
        Assert.assertTrue(pattern.matches("abcdz"));
        Assert.assertTrue(pattern.matches("abcdzx"));
        Assert.assertFalse(pattern.matches("abcdzz"));
        Assert.assertFalse(pattern.matches("abx"));
    }

    @Test
    public void wildcard() throws ConfigValidationException {
        Pattern pattern = Pattern.create("ab*", "xy*", "C", "a*xyz", "*");
        Assert.assertTrue(pattern.matches("a"));
        Assert.assertTrue(pattern.matches("abcd"));
        Assert.assertTrue(pattern.matches("axyz"));
        Assert.assertTrue(pattern.matches("acdxyz"));
        Assert.assertTrue(pattern.matches("C"));
        Assert.assertTrue(pattern.isWildcard());
    }

    @Test
    public void constant_iterateMatching() throws ConfigValidationException {
        Pattern pattern = Pattern.create("C");
        Assert.assertEquals(ImmutableSet.of("C"), ImmutableSet.of(pattern.iterateMatching(ImmutableSet.of("a", "abcd", "aecd", "axyz", "C"))));
        Assert.assertEquals(ImmutableSet.of("C"), ImmutableSet.of(pattern.iterateMatching(ImmutableSet.of("C"))));
        Assert.assertEquals(ImmutableSet.empty(), ImmutableSet.of(pattern.iterateMatching(ImmutableSet.of("a", "aecd"))));
    }

    @Test
    public void mixed_compound_iterateMatching() throws ConfigValidationException {
        Pattern pattern = Pattern.create("ab*", "xy*", "C", "a*xyz");
        Assert.assertEquals(ImmutableSet.of("abcd", "axyz", "C"),
                ImmutableSet.of(pattern.iterateMatching(ImmutableList.of("a", "abcd", "aecd", "axyz", "C"))));
        Assert.assertEquals(ImmutableSet.empty(), ImmutableSet.of(pattern.iterateMatching(ImmutableList.of("a", "aecd"))));
        Assert.assertEquals(ImmutableSet.empty(), ImmutableSet.of(pattern.iterateMatching(ImmutableList.empty())));
    }

    @Test
    public void mixed_compound_iterateMatching_iteratorProtocol() throws ConfigValidationException {
        Pattern pattern = Pattern.create("ab*", "xy*", "a*xyz");
        Iterator<String> iter = pattern.iterateMatching(ImmutableList.of("a", "abcd", "aecd", "axyz", "C")).iterator();

        Assert.assertEquals("abcd", iter.next());
        Assert.assertEquals("axyz", iter.next());
        Assert.assertFalse(iter.hasNext());
    }

    @Test
    public void wildcard_iterateMatching() throws ConfigValidationException {
        Pattern pattern = Pattern.create("*");
        Assert.assertEquals(ImmutableSet.of("a", "abcd", "aecd", "axyz", "C"),
                ImmutableSet.of(pattern.iterateMatching(ImmutableList.of("a", "abcd", "aecd", "axyz", "C"))));
        Assert.assertEquals(ImmutableSet.empty(), ImmutableSet.of(pattern.iterateMatching(ImmutableList.empty())));
    }

    @Test
    public void constant_iterateMatchingObjects() throws ConfigValidationException {
        Pattern pattern = Pattern.create("C");
        Assert.assertEquals(ImmutableSet.of(obj("C")),
                ImmutableSet.of(pattern.iterateMatching(ImmutableSet.of(obj("a"), obj("abcd"), obj("C")), obj -> obj.value)));
        Assert.assertEquals(ImmutableSet.of(obj("C")), ImmutableSet.of(pattern.iterateMatching(ImmutableSet.of(obj("C")), obj -> obj.value)));
        Assert.assertEquals(ImmutableSet.empty(), ImmutableSet.of(pattern.iterateMatching(ImmutableSet.of(obj("a"), obj("aecd")), obj -> obj.value)));
    }

    @Test
    public void mixed_compound_iterateMatchingObjects() throws ConfigValidationException {
        Pattern pattern = Pattern.create("ab*", "xy*", "C", "a*xyz");
        Assert.assertEquals(ImmutableSet.of(obj("abcd"), obj("axyz"), obj("C")), ImmutableSet
                .of(pattern.iterateMatching(ImmutableList.of(obj("a"), obj("abcd"), obj("aecd"), obj("axyz"), obj("C")), obj -> obj.value)));
        Assert.assertEquals(ImmutableSet.empty(),
                ImmutableSet.of(pattern.iterateMatching(ImmutableList.of(obj("a"), obj("aecd")), obj -> obj.value)));
    }

    @Test
    public void wildcard_iterateMatchingObjects() throws ConfigValidationException {
        Pattern pattern = Pattern.create("*");
        Assert.assertEquals(ImmutableSet.of(obj("a"), obj("abcd"), obj("aecd")),
                ImmutableSet.of(pattern.iterateMatching(ImmutableList.of(obj("a"), obj("abcd"), obj("aecd")), obj -> obj.value)));
        Assert.assertEquals(ImmutableSet.empty(), ImmutableSet.of(pattern.iterateMatching(ImmutableList.empty())));
    }

    @Test
    public void mixed_compound_getMatching() throws ConfigValidationException {
        Pattern pattern = Pattern.create("ab*", "xy*", "C", "a*xyz");
        Assert.assertEquals(ImmutableSet.of("abcd", "axyz", "C"), pattern.getMatching(ImmutableSet.of("a", "abcd", "aecd", "axyz", "C")));
        Assert.assertEquals(ImmutableSet.empty(), pattern.getMatching(ImmutableSet.of("a", "aecd")));
        Assert.assertEquals(ImmutableSet.empty(), pattern.getMatching(ImmutableSet.empty()));
    }

    @Test
    public void mixed_compound_getMatching_mapped() throws ConfigValidationException {
        Pattern pattern = Pattern.create("ab*", "xy*", "C", "a*xyz");
        Assert.assertEquals(ImmutableSet.of("ABCD", "AXYZ"),
                pattern.getMatching(ImmutableSet.of("A", "ABCD", "AECD", "AXYZ", "C"), (s) -> s.toLowerCase()));
        Assert.assertEquals(ImmutableSet.empty(), pattern.getMatching(ImmutableSet.empty()));
    }

    @Test
    public void javaPattern() throws ConfigValidationException {
        Pattern pattern = Pattern.create("/ab.*cd/");
        Assert.assertTrue(pattern.matches("abcd"));
        Assert.assertTrue(pattern.matches("abxcd"));
        Assert.assertFalse(pattern.matches("a"));
        Assert.assertFalse(pattern.matches("x"));
        Assert.assertTrue(pattern.matches(Arrays.asList("x", "abcd")));
        Assert.assertFalse(pattern.matches(Arrays.asList("x", "y")));
    }

    @Test
    public void javaPattern_compound_duplicate() throws ConfigValidationException {
        Pattern pattern = Pattern.create("/ab.*cd/", "/ab.*cd/");
        Assert.assertTrue(pattern.matches("abcd"));
        Assert.assertTrue(pattern.matches("abxcd"));
        Assert.assertFalse(pattern.matches("a"));
        Assert.assertFalse(pattern.matches("x"));
        Assert.assertTrue(pattern.matches(Arrays.asList("x", "abcd")));
        Assert.assertFalse(pattern.matches(Arrays.asList("x", "y")));
        Assert.assertTrue(pattern instanceof PatternImpl.JavaPattern);
    }

    @Test
    public void join() throws Exception {
        Pattern constant = Pattern.create("a");
        Pattern prefix1 = Pattern.create("ab*");
        Pattern prefix2 = Pattern.create("ac*");
        Pattern java = Pattern.create("/ja.*va/");
        Pattern simple1 = Pattern.create("ad*mn");
        Pattern simple2 = Pattern.create("ad*op");
        Pattern compound = Pattern.create("co*nd", "ko*nd");

        Pattern joined = Pattern.join(ImmutableList.of(constant, prefix1, prefix2, java, simple1, simple2, compound));
        Assert.assertTrue(joined.matches("a"));
        Assert.assertFalse(joined.matches("aa"));
        Assert.assertTrue(joined.matches("abc"));
        Assert.assertTrue(joined.matches("abcd"));
        Assert.assertTrue(joined.matches("acc"));
        Assert.assertTrue(joined.matches("java"));
        Assert.assertTrue(joined.matches("javava"));
        Assert.assertFalse(joined.matches("ja"));
        Assert.assertTrue(joined.matches("admn"));
        Assert.assertTrue(joined.matches("adxmn"));
        Assert.assertTrue(joined.matches("admomn"));
        Assert.assertFalse(joined.matches("admomno"));
        Assert.assertFalse(joined.matches("ad"));
        Assert.assertFalse(joined.matches("ado"));
        Assert.assertTrue(joined.matches("compound"));
        Assert.assertTrue(joined.matches("kond"));

    }

    @Test
    public void join_simple_noprefix() throws Exception {
        Pattern simple1 = Pattern.create("*b");
        Pattern simple2 = Pattern.create("*d");

        Pattern joined = Pattern.join(ImmutableList.of(simple1, simple2));
        Assert.assertTrue(joined.matches("aab"));
        Assert.assertTrue(joined.matches("b"));
        Assert.assertTrue(joined.matches("aabd"));
        Assert.assertFalse(joined.matches("aabc"));
    }

    @Test
    public void join_single_constant() throws Exception {
        Pattern constant1 = Pattern.create("a");
        Pattern constant2 = Pattern.create("a");

        Pattern joined = Pattern.join(ImmutableList.of(constant1, constant2));
        Assert.assertTrue(joined.matches("a"));
        Assert.assertFalse(joined.matches("aa"));

        Assert.assertTrue(joined instanceof PatternImpl.Constant);
    }

    @Test
    public void join_single_simple() throws Exception {
        Pattern simple1 = Pattern.create("a*b");
        Pattern simple2 = Pattern.create("a*b");

        Pattern joined = Pattern.join(ImmutableList.of(simple1, simple2));
        Assert.assertTrue(joined.matches("aab"));
        Assert.assertFalse(joined.matches("b"));

        Assert.assertTrue(joined instanceof SimplePatternImpl);
    }

    @Test
    public void join_single_simple_noprefix() throws Exception {
        Pattern simple1 = Pattern.create("*b");
        Pattern simple2 = Pattern.create("*b");

        Pattern joined = Pattern.join(ImmutableList.of(simple1, simple2));
        Assert.assertTrue(joined.matches("aab"));
        Assert.assertTrue(joined.matches("b"));
        Assert.assertFalse(joined.matches("aabc"));

        Assert.assertTrue(joined instanceof SimplePatternImpl);
    }

    @Test
    public void join_single_prefix() throws Exception {
        Pattern simple1 = Pattern.create("a*");
        Pattern simple2 = Pattern.create("a*");

        Pattern joined = Pattern.join(ImmutableList.of(simple1, simple2));
        Assert.assertTrue(joined.matches("aa"));
        Assert.assertFalse(joined.matches("b"));

        Assert.assertTrue(joined instanceof PrefixPattern);
    }

    @Test
    public void join_blank() throws Exception {
        Pattern joined = Pattern.join(ImmutableList.empty());
        Assert.assertTrue(joined.equals(PatternImpl.BLANK));
    }

    @Test
    public void join_wildcard() throws Exception {
        Pattern joined = Pattern.join(ImmutableList.of(Pattern.create("a*"), Pattern.create("*")));
        Assert.assertTrue(joined.equals(PatternImpl.WILDCARD));
    }

    @Test
    public void create_fromDocNode() throws Exception {
        String yaml = "- a*\n- b*";
        DocNode docNode = DocNode.parse(Format.YAML).from(yaml);
        Pattern pattern = Pattern.parse(docNode, null);
        Assert.assertTrue(pattern.matches("aaa"));
        Assert.assertTrue(pattern.matches("bbb"));
        Assert.assertFalse(pattern.matches("caa"));
    }

    @Test
    public void create_fromDocNodeScalar() throws Exception {
        DocNode docNode = DocNode.wrap("a*");
        Pattern pattern = Pattern.parse(docNode, null);
        Assert.assertTrue(pattern.matches("aaa"));
        Assert.assertFalse(pattern.matches("caa"));
    }

    @Test
    public void isConstant() {
        Assert.assertTrue(Pattern.isConstant("a"));
        Assert.assertFalse(Pattern.isConstant("a*"));
        Assert.assertFalse(Pattern.isConstant("a?"));
        Assert.assertFalse(Pattern.isConstant("*a"));
        Assert.assertFalse(Pattern.isConstant("/a.*/"));
        Assert.assertFalse(Pattern.isConstant("*"));
    }

    @Test
    public void createPrefixPattern() {
        Pattern pattern = Pattern.createPrefixPattern("aa", "ab");
        Assert.assertTrue(pattern.matches("aaa"));
        Assert.assertTrue(pattern.matches("aa"));
        Assert.assertFalse(pattern.matches("a"));
        Assert.assertTrue(pattern.matches("abb"));
        Assert.assertTrue(pattern.matches("ab"));
        Assert.assertFalse(pattern.matches("b"));
    }

    static class TestObject {
        final String value;

        TestObject(String value) {
            this.value = value;
        }

        @Override
        public int hashCode() {
            return value.hashCode();
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof TestObject)) {
                return false;
            }

            return this.value.equals(((TestObject) obj).value);
        }
    }

    static TestObject obj(String value) {
        return new TestObject(value);
    }

}
