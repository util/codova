/*
 * Copyright 2022 floragunn GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.floragunn.codova.config.text;

import org.junit.Assert;
import org.junit.Test;

import com.floragunn.fluent.collections.ImmutableList;
import com.floragunn.fluent.collections.ImmutableSet;

public class PatternMapTest {
    @Test
    public void basic() throws Exception {
        PatternMap.Builder<String> builder = new PatternMap.Builder<>();
        builder.add(Pattern.create("abc*"), "ABC_");
        builder.add(Pattern.create("abc*xyz"), "ABC_XYZ");
        builder.add(Pattern.create("ab*"), "AB_");
        builder.add(Pattern.create("aa*"), "AA_");
        builder.add(Pattern.create("a*"), "A_");
        builder.add(Pattern.create("a"), "A");
        builder.add(Pattern.create("z"), "Z");
        builder.add(Pattern.create("*yy"), "_YY");
        builder.add(Pattern.create("x"), "X1");
        builder.add(Pattern.create("x"), "X2");
        builder.add(Pattern.create("cc", "cs*"), "CC/CS_");
        builder.add(Pattern.create("xxx*"), "XXX1");
        builder.add(Pattern.create("xxx*"), "XXX2");

        PatternMap<String> subject = builder.build();

        Assert.assertEquals(ImmutableSet.empty(), subject.get("AAAA"));
        Assert.assertEquals(ImmutableSet.empty(), subject.get(""));
        Assert.assertEquals(ImmutableSet.of("A", "A_"), subject.get("a"));
        Assert.assertEquals(ImmutableSet.of("A_", "AA_"), subject.get("aa"));
        Assert.assertEquals(ImmutableSet.of("A", "A_", "AA_"), subject.get(ImmutableList.of("a", "aa")));
        Assert.assertEquals(ImmutableSet.of("A_", "AB_"), subject.get("ab"));
        Assert.assertEquals(ImmutableSet.of("A_", "AB_", "ABC_"), subject.get("abc"));
        Assert.assertEquals(ImmutableSet.of("A_", "AB_", "ABC_"), subject.get("abcd"));
        Assert.assertEquals(ImmutableSet.of("A_", "AB_", "ABC_"), subject.get("abcdx"));
        Assert.assertEquals(ImmutableSet.of("A_", "AB_", "ABC_", "ABC_XYZ"), subject.get("abcdbxyz"));
        Assert.assertEquals(ImmutableSet.of("A_", "AB_", "ABC_"), subject.get("abcdxyz0"));
        Assert.assertEquals(ImmutableSet.of("X1", "X2"), subject.get("x"));
        Assert.assertEquals(ImmutableSet.of("XXX1", "XXX2"), subject.get("xxx"));
        Assert.assertEquals(ImmutableSet.of("XXX1", "XXX2"), subject.get("xxxx"));
        Assert.assertEquals(ImmutableSet.of("_YY"), subject.get("xyy"));
        Assert.assertEquals(ImmutableSet.of("_YY"), subject.get(ImmutableList.of("xyy", "xyyy")));
        Assert.assertEquals(ImmutableSet.of("CC/CS_"), subject.get("csi"));
        Assert.assertEquals(ImmutableSet.of("CC/CS_"), subject.get("cc"));
    }

    @Test
    public void constants() throws Exception {
        PatternMap.Builder<String> builder = new PatternMap.Builder<>();
        builder.add(Pattern.create("a"), "A");
        builder.add(Pattern.create("b"), "B");
        builder.add(Pattern.create("c"), "C");
        PatternMap<String> subject = builder.build();

        Assert.assertEquals(ImmutableSet.empty(), subject.get("AAAA"));
        Assert.assertEquals(ImmutableSet.empty(), subject.get(""));
        Assert.assertEquals(ImmutableSet.of("A"), subject.get("a"));
    }

    @Test
    public void without_prefix() throws Exception {
        PatternMap.Builder<String> builder = new PatternMap.Builder<>();
        builder.add(Pattern.create("*a"), "A");
        builder.add(Pattern.create("*b"), "B");
        builder.add(Pattern.create("*c"), "C");
        PatternMap<String> subject = builder.build();

        Assert.assertEquals(ImmutableSet.empty(), subject.get("AAAA"));
        Assert.assertEquals(ImmutableSet.empty(), subject.get(""));
        Assert.assertEquals(ImmutableSet.of("A"), subject.get("a"));
    }

    @Test
    public void with_wildcard() throws Exception {
        PatternMap.Builder<String> builder = new PatternMap.Builder<>();
        builder.add(Pattern.create("*"), "WC");

        builder.add(Pattern.create("abc*"), "ABC_");
        builder.add(Pattern.create("abc*xyz"), "ABC_XYZ");
        builder.add(Pattern.create("ab*"), "AB_");
        builder.add(Pattern.create("aa*"), "AA_");
        builder.add(Pattern.create("a*"), "A_");
        builder.add(Pattern.create("a"), "A");
        builder.add(Pattern.create("z"), "Z");
        builder.add(Pattern.create("*yy"), "_YY");
        builder.add(Pattern.create("x"), "X1");
        builder.add(Pattern.create("x"), "X2");
        builder.add(Pattern.create("xxx*"), "XXX1");
        builder.add(Pattern.create("xxx*"), "XXX2");

        PatternMap<String> subject = builder.build();

        Assert.assertEquals(ImmutableSet.of("WC"), subject.get("AAAA"));
        Assert.assertEquals(ImmutableSet.of("WC"), subject.get(""));
        Assert.assertEquals(ImmutableSet.of("WC", "A", "A_"), subject.get("a"));
        Assert.assertEquals(ImmutableSet.of("WC", "A_", "AA_"), subject.get("aa"));
        Assert.assertEquals(ImmutableSet.of("WC", "A_", "AB_"), subject.get("ab"));
        Assert.assertEquals(ImmutableSet.of("WC", "A_", "AB_", "ABC_"), subject.get("abc"));
        Assert.assertEquals(ImmutableSet.of("WC", "A_", "AB_", "ABC_"), subject.get("abcd"));
        Assert.assertEquals(ImmutableSet.of("WC", "A_", "AB_", "ABC_"), subject.get("abcdx"));
        Assert.assertEquals(ImmutableSet.of("WC", "A_", "AB_", "ABC_", "ABC_XYZ"), subject.get("abcdbxyz"));
        Assert.assertEquals(ImmutableSet.of("WC", "A_", "AB_", "ABC_"), subject.get("abcdxyz0"));
        Assert.assertEquals(ImmutableSet.of("WC", "X1", "X2"), subject.get("x"));
        Assert.assertEquals(ImmutableSet.of("WC", "XXX1", "XXX2"), subject.get("xxx"));
        Assert.assertEquals(ImmutableSet.of("WC", "XXX1", "XXX2"), subject.get("xxxx"));
        Assert.assertEquals(ImmutableSet.of("WC", "_YY"), subject.get("xyy"));
    }

}
