/*
 * Copyright 2024 floragunn GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.floragunn.codova.config.net;

import java.time.Duration;

import org.junit.Assert;
import org.junit.Test;

import com.floragunn.codova.documents.DocNode;
import com.floragunn.codova.validation.ValidatingDocNode;
import com.floragunn.codova.validation.ValidationErrors;

public class CacheConfigTest {
    @Test
    public void customDefault() {
        CacheConfig defaultValue = new CacheConfig(true, Duration.ofMinutes(60), null, null);
        ValidationErrors validationErrors = new ValidationErrors();
        ValidatingDocNode vNode = new ValidatingDocNode(DocNode.of("cache.max_size", 1234), validationErrors);
        
        CacheConfig parsedValue = vNode.get("cache").withDefault(defaultValue).by(CacheConfig::new);
        
        Assert.assertEquals(parsedValue.getExpireAfterWrite(), defaultValue.getExpireAfterWrite());
        Assert.assertEquals(parsedValue.getMaxSize().intValue(), 1234);
    }
}
