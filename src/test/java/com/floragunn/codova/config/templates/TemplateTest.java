/*
 * Copyright 2022 floragunn GmbH
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package com.floragunn.codova.config.templates;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.DiagnosingMatcher;
import org.junit.Assert;
import org.junit.Test;

import com.floragunn.codova.documents.DocReader;
import com.floragunn.codova.documents.DocWriter;
import com.floragunn.codova.validation.ConfigValidationException;
import com.floragunn.fluent.collections.ImmutableMap;
import com.google.common.base.Joiner;
import com.google.common.collect.Iterables;

public class TemplateTest {
    @Test
    public void testReplaceAttributesBasic() throws Exception {
        ImmutableMap<String, Object> attributes = ImmutableMap.of("user.name", "horst", "user.attrs.map1",
                ImmutableMap.of("one", 1, "two", 2, "three", 3), "user.roles", Arrays.asList("role1", "role2", "role3"));
        AttributeSource attributeSource = AttributeSource.from(attributes);

        Template<String> template = new Template<>("User: ${user.name}\nRoles: ${user.roles}\nMap1: ${user.attrs.map1}\n", String::valueOf);
        String result = template.render(attributeSource);

        Assert.assertEquals(result, attributes.get("user.name"), getStringSegment("User: (.*)", result));
        Assert.assertEquals(result, toQuotedCommaSeparatedString((Collection<?>) attributes.get("user.roles")),
                getStringSegment("Roles: (.*)", result));
        Assert.assertEquals(result, attributes.get("user.attrs.map1").toString(), getStringSegment("Map1: (.*)", result));
    }

    @Test
    public void testReplaceAttributesJson() throws Exception {
        ImmutableMap<String, Object> attributes = ImmutableMap.of("user.name", "horst", "user.attrs.map1",
                ImmutableMap.of("one", 1, "two", 2, "three", 3), "user.roles", Arrays.asList("role1", "role2", "role3"));
        AttributeSource attributeSource = AttributeSource.from(attributes);

        Template<String> template = new Template<>("User: ${user.name|toJson}\nRoles: ${user.roles|toJson}\nMap1: ${user.attrs.map1|toJson}\n",
                String::valueOf);
        String result = template.render(attributeSource);

        Assert.assertThat(result, segment("User: (.*)", equalsAsJson(attributes.get("user.name"))));
        Assert.assertThat(result, segment("Roles: (.*)", equalsAsJson(attributes.get("user.roles"))));
        Assert.assertThat(result, segment("Map1: (.*)", equalsAsJson(attributes.get("user.attrs.map1"))));
    }

    @Test
    public void testReplaceAttributesToList() throws Exception {

        ImmutableMap<String, Object> attributes = ImmutableMap.of("user.name", "horst", "user.attrs.map1",
                ImmutableMap.of("one", 1, "two", 2, "three", 3), "user.roles", Arrays.asList("role1", "role2", "role3"));
        AttributeSource attributeSource = AttributeSource.from(attributes);

        Template<String> template = new Template<>(
                "User: ${user.name|toList|toJson}\nRoles: ${user.roles|toList|toJson}\nAttributes: ${user.attrs|toList|toJson}\nMap1: ${user.attrs.map1|toList|toJson}\n",
                String::valueOf);
        String result = template.render(attributeSource);

        Assert.assertThat(result, segment("User: (.*)", equalsAsJson(Arrays.asList(attributes.get("user.name")))));
        Assert.assertThat(result, segment("Roles: (.*)", equalsAsJson(attributes.get("user.roles"))));
        Assert.assertThat(result, segment("Map1: (.*)", equalsAsJson(Arrays.asList(attributes.get("user.attrs.map1")))));
    }

    @Test
    public void testReplaceAttributesHeadOfList() throws Exception {
        ImmutableMap<String, Object> attributes = ImmutableMap.of("user.name", "horst", "user.attrs.map1",
                ImmutableMap.of("one", 1, "two", 2, "three", 3), "user.roles", Arrays.asList("role1", "role2", "role3"));
        AttributeSource attributeSource = AttributeSource.from(attributes);

        Template<String> template = new Template<>(
                "User: ${user.name|head|toJson}\nRoles: ${user.roles|head|toJson}\nMap1: ${user.attrs.map1|head|toJson}\n",
                String::valueOf);
        String result = template.render(attributeSource);

        Assert.assertThat(result, segment("User: (.*)", equalsAsJson(attributes.get("user.name"))));
        Assert.assertThat(result, segment("Roles: (.*)", equalsAsJson(((Collection<?>) attributes.get("user.roles")).iterator().next())));
        Assert.assertThat(result, segment("Map1: (.*)", equalsAsJson(attributes.get("user.attrs.map1"))));
    }

    @Test
    public void testReplaceAttributesTailOfList() throws Exception {
        ImmutableMap<String, Object> attributes = ImmutableMap.of("user.name", "horst", "user.attrs.map1",
                ImmutableMap.of("one", 1, "two", 2, "three", 3), "user.roles", Arrays.asList("role1", "role2", "role3"));
        AttributeSource attributeSource = AttributeSource.from(attributes);

        Template<String> template = new Template<>(
                "User: ${user.name|tail|toJson}\nRoles: ${user.roles|tail|toJson}\nAttributes: ${user.attrs|tail|toJson}\nMap1: ${user.attrs.map1|tail|toJson}\n",
                String::valueOf);
        String result = template.render(attributeSource);

        Set<?> rolesTail = new HashSet<Object>(((Collection<?>) attributes.get("user.roles")));
        rolesTail.remove(((Collection<?>) attributes.get("user.roles")).iterator().next());

        Assert.assertThat(result, segment("User: (.*)", equalsAsJson(Collections.emptyList())));
        Assert.assertThat(result, segment("Roles: (.*)", equalsAsJson(rolesTail)));
        Assert.assertThat(result, segment("Map1: (.*)", equalsAsJson(Collections.emptyList())));
    }

    @Test
    public void testReplaceAttributesDefault() throws Exception {
        ImmutableMap<String, Object> attributes = ImmutableMap.of("user.name", "horst", "user.attrs.map1",
                ImmutableMap.of("one", 1, "two", 2, "three", 3), "user.roles", Arrays.asList("role1", "role2", "role3"));
        AttributeSource attributeSource = AttributeSource.from(attributes);

        Template<String> template = new Template<>("A1: ${user.attrs.foo|toJson:-1}\nA2: ${user.attrs.bar|toJson:-\"blub\"}\n", String::valueOf);
        String result = template.render(attributeSource);

        Assert.assertThat(result, segment("A1: (.*)", equalsAsJson(1)));
        Assert.assertThat(result, segment("A2: (.*)", equalsAsJson("blub")));
    }

    @Test
    public void testReplaceAttributesJsonDefault() throws Exception {
        ImmutableMap<String, Object> attributes = ImmutableMap.of("user.name", "horst", "user.attrs.map1",
                ImmutableMap.of("one", 1, "two", 2, "three", 3), "user.roles", Arrays.asList("role1", "role2", "role3"));
        AttributeSource attributeSource = AttributeSource.from(attributes);

        Template<String> template = new Template<>(
                "A1: ${user.attrs.foo?:{\"a\": 1}|toJson}\nA2: ${user.attrs.bar?:true|toJson}\nA3: ${user.attrs.bar?:[1,2,3]|toJson}\n",
                String::valueOf);
        String result = template.render(attributeSource);

        Assert.assertThat(result, segment("A1: (.*)", equalsAsJson(ImmutableMap.of("a", 1))));
        Assert.assertThat(result, segment("A2: (.*)", equalsAsJson(true)));
        Assert.assertThat(result, segment("A3: (.*)", equalsAsJson(Arrays.asList(1, 2, 3))));

    }

    @Test
    public void constantTemplate_string() throws Exception {
        Template<String> template = Template.string("foo");
        Assert.assertEquals("foo", template.getConstantValue());
        Assert.assertTrue(template.isConstant());
    }

    @Test
    public void constantTemplate_parsed() throws Exception {
        Template<com.floragunn.codova.config.text.Pattern> template = new Template<com.floragunn.codova.config.text.Pattern>("*",
                com.floragunn.codova.config.text.Pattern::create);
        Assert.assertEquals(com.floragunn.codova.config.text.Pattern.wildcard(), template.getConstantValue());
        Assert.assertTrue(template.isConstant());
    }
    
    @Test
    public void constantTemplate_parsedLate() throws Exception {
        Template<String> template = Template.string("*");
        Assert.assertEquals(com.floragunn.codova.config.text.Pattern.wildcard(),
                template.parser(com.floragunn.codova.config.text.Pattern::create).getConstantValue()); 
        
        Template<String> invalidPattern = Template.string("/a(/");
        
        try {
            invalidPattern.parser(com.floragunn.codova.config.text.Pattern::create);
            Assert.fail();
        } catch (ConfigValidationException e) {
            Assert.assertTrue(e.getMessage().startsWith("Invalid value"));
        }
    }
    
    private static String getStringSegment(String patternString, String input) {
        Pattern pattern = Pattern.compile("^" + patternString + "$", Pattern.MULTILINE);
        Matcher matcher = pattern.matcher(input);

        if (matcher.find()) {
            return matcher.group(1);
        } else {
            return null;
        }
    }

    private static String toQuotedCommaSeparatedString(Collection<?> roles) {
        return Joiner.on(',').join(Iterables.transform(roles, s -> {
            return new StringBuilder(String.valueOf(s).length() + 2).append('"').append(s).append('"').toString();
        }));
    }

    private static DiagnosingMatcher<String> equalsAsJson(Object expectedObjectStructure) {
        Object rewrittenExpectedObjectStructure;

        try {
            rewrittenExpectedObjectStructure = DocReader.json().read((DocWriter.json().writeAsString(expectedObjectStructure)));
        } catch (Exception e1) {
            throw new RuntimeException(e1);
        }

        return new DiagnosingMatcher<String>() {

            @Override
            public void describeTo(Description description) {
                description.appendText("equals as JSON to ").appendValue(expectedObjectStructure);
            }

            @Override
            protected boolean matches(Object item, Description mismatchDescription) {
                if (!(item instanceof String)) {
                    mismatchDescription.appendValue(item).appendText(" is not a string");
                    return false;
                }

                Object parsedItem;

                try {
                    parsedItem = DocReader.json().read(((String) item));
                } catch (Exception e) {
                    mismatchDescription.appendValue(item).appendText(" is not valid JSON: " + e);
                    return false;
                }

                if (parsedItem.equals(rewrittenExpectedObjectStructure)) {
                    return true;
                } else {
                    mismatchDescription.appendValue(parsedItem).appendText(" does not equal ").appendValue(rewrittenExpectedObjectStructure);
                    return false;
                }

            }

        };
    }

    private static DiagnosingMatcher<String> segment(String patternString, BaseMatcher<String> subMatcher) {
        return new DiagnosingMatcher<String>() {

            @Override
            public void describeTo(Description description) {
                description.appendText("segment " + patternString + " ").appendDescriptionOf(subMatcher);
            }

            @Override
            protected boolean matches(Object item, Description mismatchDescription) {
                if (!(item instanceof String)) {
                    mismatchDescription.appendValue(item).appendText(" is not a string");
                    return false;
                }

                String segment = getStringSegment(patternString, (String) item);

                if (segment != null) {
                    if (subMatcher.matches(segment)) {
                        return true;
                    } else {
                        subMatcher.describeMismatch(segment, mismatchDescription);
                        return false;
                    }
                } else {
                    mismatchDescription.appendText("Could not find pattern " + patternString + " in").appendValue(item);
                    return false;
                }

            }

        };
    }
}
