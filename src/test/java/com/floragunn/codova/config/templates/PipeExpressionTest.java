/*
 * Copyright 2022 floragunn GmbH
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package com.floragunn.codova.config.templates;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

import com.floragunn.fluent.collections.ImmutableMap;

public class PipeExpressionTest {
    @Test
    public void basic() throws Exception {
        PipeExpression expression = PipeExpression.parse("a");
        Assert.assertEquals("X", expression.evaluate(AttributeSource.of("a", "X")));
        Assert.assertEquals(null, expression.evaluate(AttributeSource.of("b", "X")));
    }

    @Test
    public void basic_dotted() throws Exception {
        PipeExpression expression = PipeExpression.parse("a.b");
        Assert.assertEquals("X", expression.evaluate(AttributeSource.from(ImmutableMap.of("a.b", "X"))));
        Assert.assertEquals("X", expression.evaluate(AttributeSource.from(ImmutableMap.of("a", ImmutableMap.of("b", "X")))));
    }

    @Test
    public void toLowerCase() throws Exception {
        PipeExpression expression = PipeExpression.parse("a|toLowerCase");
        Assert.assertEquals("x", expression.evaluate(AttributeSource.of("a", "X")));
    }

    @Test
    public void toLowerCase_collection() throws Exception {
        PipeExpression expression = PipeExpression.parse("a|toLowerCase");
        Assert.assertEquals(Arrays.asList("x", "y"), expression.evaluate(AttributeSource.of("a", Arrays.asList("X", "Y"))));
    }

    @Test
    public void toUpperCase() throws Exception {
        PipeExpression expression = PipeExpression.parse("a|toUpperCase");
        Assert.assertEquals("X", expression.evaluate(AttributeSource.of("a", "x")));
    }

    @Test
    public void toUpperCase_collection() throws Exception {
        PipeExpression expression = PipeExpression.parse("a|toUpperCase");
        Assert.assertEquals(Arrays.asList("X", "Y"), expression.evaluate(AttributeSource.of("a", Arrays.asList("x", "Y"))));
    }
}
