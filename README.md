# codova

`codova` is a framework for *co*nfiguration *do*cuments *va*lidation.

## Background

The framework was incrementally built together with the Search Guard plugin for Elasticsearch. This special ecosystem has led to a number of specific design choices for codova.

- Does not use any reflection. Reason: The usage of reflection is protected by a Security Manager in Elasticsearch and is thus difficult to use and error-prone (due to forgotten privileged blocks).
- Does not use jackson-databind. First of all, jackson-databind also uses reflection. Secondly, the classpath in an Elasticsearch environment makes it hard or even impossible to keep jackson-databind on a recent version.

Additional goals, regardless of the environment:

- User centric configuration validation: The framework shall offer very useful validation error messages.  
- Make often reoccuring configuration types reusable; examples: TLS configuration, proxy configuration, string patterns, etc.
- Provide a fluent syntax for configuration parsing

## Overview

`codova` is partitioned into three main packes:

- `com.floragunn.codova.documents`: This is a lightweight framework for representing abstract syntax trees of JSON/YAML documents and parsing/unparsing these from/to files. This forms the basis for `config` and `validation`. If you do not need validation or reusable config types, you can also just use this on its own.
- `com.floragunn.codova.validation`: This is the validation framework. 
- `com.floragunn.codova.config`: These are reusable config types such as TLS configuration, etc. It builds upon the `documents` and `validation` framework. 

## Package `documents`

`codova` is designed for document types like JSON and JSON-compatible documents, like YAML, SMILE, etc.

`codova` supports two main approaches to representing these documents:

- As an object tree of the standard Java types `String`, `Number`, `Boolean`, `Map` and `List`. These Java types are often referred to as the "basic object".
- Using the `DocNode` class. Compared to the standard Java types, `DocNode` facilitates operations on JSON/YAML documents like tree traversion or accessing members. It is more null-safe and requires less type casting.

Central classes of this package are:

- `DocNode`: A standardized interface to nodes of JSON/YAML/SMILE documents. If you have such a document as an object tree of standard Java types, you can just wrap `DocNode` around it to get the enhanced interface.
- `DocReader` and `DocWriter`: Allow reading and writing of JSON/YAML/SMILE documents from/to basic objects or `DocNode`. See the class `Format` for the available file formats.
- `Document`: An interface that can be implemented by classes which are supposed to have a document (i.e. JSON/YAML/SMILE) representation. Classes implementing `Document` must have a `toBasicObject` method which provides a representation of the class contexts using the the standard Java types `String`, `Number`, `Boolean`, `Map` and `List` or using a `DocNode`. Such classes will then automatically
serializable by `DocWriter`. Additionally, they get the methods `toJsonString()` and `toYamlString()` which give quick access to such serialized documents.
- `UnparsedDocument`: Represents documents that have not been parsed yet and are preserved in their serialized form. With `UnparsedDocument`, you can re-write documents from one format to another using `DocWriter` in streaming manner. It is not necessary to build a complete object tree in this case. `UnparsedDocument` objects are accompanied by a media type (like `application/json`) to give a hint about the format of its content.

### Dotted attribute names

The `DocNode` class offers the function `splitDottedAttributeNamesToTree()` to split attribute names containing dots into an object tree. 

Consider for example this document:

```
foo:
  a.b: "bla"
  a.c: "blub"
```

After applying `splitDottedAttributeNamesToTree()`, the document looks like this:

```
foo:
  a:
    b: "bla"
    c: "blub"
```

This is supposed to make deeply nested attributes within YAML less a problem. It is heavily used for the Search Guard `authc` configuration.

**Note:** This is a non-standard functionality.

**Note:** Additionally, some utility methods like `DocNode.of("a", 1, "b.c", 2)` support the dotted notation to specify deep nesting. This is independent of the usage of the `splitDottedAttributeNamesToTree()` function.

### Examples

Create a constant doc node representing the document `{"a": 1, "b": {"c": 2}}`:

```
DocNode docNode = DocNode.of("a", 1, "b.c", 2);
```

Alternatively:

```
DocNode docNode = DocNode.of("a", 1, "b", DocNode.of("c", 2));
```


Use DocNode to read a JSON file into DocNode:

```
DocNode docNode = DocNode.parse(Format.JSON).from(new File("/my/file.json"));
```

Use DocReader to read a JSON file into a basic object tree:

```
Object object = DocReader.json().read(new File("/my/file.json"));
```


See also:

- [DocNodeTest.java](https://git.floragunn.com/util/codova/-/blob/main/src/test/java/com/floragunn/codova/documents/DocNodeTest.java)
- [BasicDocReaderTest.java](https://git.floragunn.com/util/codova/-/blob/main/src/test/java/com/floragunn/codova/documents/BasicDocReaderTest.java)



## Package `documents.patch`

This package provides support for modifying documents using the standards JSON merge patch and JSON patch. Additionally, there is a non-standard patch implementation which utilizes JSON path expressions instead of JSON pointer expressions which are normally used by JSON patch.

The patch implementations are able to patch documents represented by `DocNode` trees or any document implementing `PatchableDocument`. 

### Examples

```
String jsonPatchAsString = getJsonPatchAsStringFromSomewhere();
DocPath docPatch = DocPatch.parse("application/json-patch+json", jsonPatchAsString);
DocNode originalDocNode = ...;
DocNode patchedDocNode = docPatch.apply(originalDocNode);
```

See also:

- [JsonPatchTest.java](https://git.floragunn.com/util/codova/-/blob/main/src/test/java/com/floragunn/codova/documents/patch/JsonPatchTest.java)


## Package `validation`

The purpose of the validation package is to parse and validate configuration in one pass. It is supposed to be very versatile to provide validation error messages that are as user-centric and helpful as possible. Parsing and validation shall not abort with the first error. Instead, parsing shall progress as far as possible and aggregate as many validation error messages as possible.

Central classes of this package are:

- `ValidatingDocNode`: A wrapper around `DocNode` (or basic objects) which offers fluent methods that retrieve parsed and validated data. In case of validation errors, these are automatically recorded and aggregated. Additionally, the `ValidatingDocNode` class provides basic infrastructure for interpolating configuration variables.
- `ValidationError`: A base class for all possible validation errors. Validation errors usually consist of a path to the attribute causing the error, a validation error message and potentially further attributes.
- `ValidationErrors`: A collection of all validation errors that have been encountered so far for a document.
- `ConfigValidationException`: Methods that parse and validate configuration shall throw this exception in case the configuration cannot be parsed. This exception encapsulates `ValidationErrors`.
- `ValidationResult`: This is an alternative to `ConfigValidationException`. Methods using `ConfigValidationException` either return the 100% validated parsed object, or no parsed object at all, but the `ConfigValidationException`. If you want to have access to partially parsed objects, you can use the return type `ValidationResult`, which encapsulates both the parsed object (potentially incomplete) and the encountered validation errors.


### Typical usage pattern

A class which allows parsing configuration using `codova` often looks similar to this:


```
public class TLSConfig {
   // [...]

    public static TLSConfig parse(DocNode config, Parser.Context parserContext) throws ConfigValidationException {
        ValidationErrors validationErrors = new ValidationErrors();
        ValidatingDocNode vNode = new ValidatingDocNode(config, validationErrors, parserContext);
        
        TLSConfig tlsConfig = new TLSConfig();
        tlsConfig.supportedProtocols = vNode.get("enabled_protocols").asList().withDefault("TLSv1.2", "TLSv1.1").ofStrings();
        tlsConfig.supportedCipherSuites = vNode.get("enabled_ciphers").asList().ofStrings();
        tlsConfig.hostnameVerificationEnabled = vNode.get("verify_hostnames").withDefault(true).asBoolean();
        tlsConfig.trustAll = vNode.get("trust_all").withDefault(false).asBoolean();
        tlsConfig.truststore = vNode.get("trusted_cas").by(TLSConfig::toTruststore);        
        tlsConfig.clientCertAuthConfig = vNode.get("client_auth").by(ClientCertAuthConfig::parse);

        vNode.checkForUnusedAttributes();
        validationErrors.throwExceptionForPresentErrors();

        return tlsConfig;
    }
}
```



## Package `config`

TODO


## License

This code is licensed under the Apache 2.0 License.

## Copyright

Copyright 2022 by floragunn GmbH

